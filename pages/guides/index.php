
<!----------------------------------------
	START OF PAGE
----------------------------------------->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>


	<!----------------------------------------
		PHP VARIABLES
	----------------------------------------->

	<?php $includes = $_SERVER['DOCUMENT_ROOT'].getEnv('ROOT_PATH')."/pages/page-appendages/"; ?>
	
	
	<!----------------------------------------
		HEADER
	----------------------------------------->
	
	<?php include( $includes."header.php" ); ?>

</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		NAVIGATION
	----------------------------------------->
	
	<?php include( $includes."/mobile-navigation.php" ); ?>
	
	
	<!-- Site Wrapper -->
	
	<div class="site-wrapper">
	
	
	<!-- Standard Navigation -->
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!----------------------------------------
		BANNER
		
		Preferably use absolute paths.
		The final parameter should be either
		"fullscreen" or ""
	----------------------------------------->	
	
	<?php heroImage( "Guides", getEnv('ROOT_PATH')."/images/featured2.jpg", "" ); ?>
	
	
	<!----------------------------------------
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none">
	
		<div class="gallery large horizontal">
		
			<div>
			<a href="<?php echo getEnv('ROOT_PATH') ?>/pages/guides/halo-5/h5-guide.php">
				<div class="img-container fit-height">
				
					<div class="guide-preview">
						<div class="article-title">Halo 5</div>
						<div class="article-info">Warzone !!</div>
					</div>
					
					<img class="article-image" src="<?php echo getEnv('ROOT_PATH') ?>/images/index/homepage2.jpg" alt>
				
				</div>
			</a>
			</div>

			<div>
			<a href="<?php echo getEnv('ROOT_PATH') ?>/pages/guides/halo-5/h5-guide.php">
				<div class="img-container fit-height">
				
					<div class="guide-preview">
						<div class="article-title">Halo Reach</div>
						<div class="article-info">Invasion !!!</div>
					</div>
					
					<img class="article-image" src="<?php echo getEnv('ROOT_PATH') ?>/images/index/homepage1.jpg" alt>
				
				</div>
			</a>
			</div>
		
			<div>
			<a href="<?php echo getEnv('ROOT_PATH') ?>/pages/guides/halo-5/h5-guide.php">
				<div class="img-container fit-height">
				
					<div class="guide-preview">
						<div class="article-title">Halo 3</div>
						<div class="article-info">Ye olden days</div>
					</div>
					
					<img class="article-image" src="<?php echo getEnv('ROOT_PATH') ?>/images/index/homepage3.jpg" alt>
				
				</div>
			</a>
			</div>
		
		</div>
	
	</div>
	
	
	<!---------------------------------------- 
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."footer.php" ); ?>
	
	
	<!-- End Site Wrapper -->
	
	</div>
	
</body>
</html>
