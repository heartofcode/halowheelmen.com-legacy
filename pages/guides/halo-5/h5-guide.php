
<!----------------------------------------
	START OF PAGE
----------------------------------------->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>


	<!----------------------------------------
		PHP VARIABLES
	----------------------------------------->

	<?php $includes = $_SERVER['DOCUMENT_ROOT'].getEnv('ROOT_PATH')."/pages/page-appendages/"; ?>
	<?php $pages = $_SERVER['DOCUMENT_ROOT'].getEnv('ROOT_PATH')."/pages/"; ?>
	
	
	<!----------------------------------------
		HEADER
	----------------------------------------->
	
	<?php include( $includes."header.php" ); ?>
	
	<?php include( $includes."user-session.php" ); ?>
	<?php include( $includes."forum-ids.php" ); ?>

</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		NAVIGATION
	----------------------------------------->
	
	<?php include( $includes."/mobile-navigation.php" ); ?>
	
	
	<!-- Site Wrapper -->
	
	<div class="site-wrapper">
	
	
	<!-- Standard Navigation -->
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!----------------------------------------
		BANNER
		
		Preferably use absolute paths.
		The final parameter should be either
		"fullscreen" or ""
	----------------------------------------->	
	
	<?php heroImage( "Halo 5 Guides", getEnv('ROOT_PATH')."/images/featured2.jpg", "" ); ?>
	
	
	<!----------------------------------------
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none">
	
		<div>
			<h1>Maps and Gametypes</h1>
			
			<div class="gallery medium horizontal-wrap">
				<?php include( $pages."guides/halo-5/h5-map-guides.php" ); ?>
			</div>
		</div>
		
		<div>
			<h1>Vehicles and Weapons</h1>
			
			<div class="gallery medium horizontal-wrap">
				<?php include( $pages."guides/halo-5/h5-vehicle-guides.php" ); ?>
			</div>
		</div>

	
	</div>
	
	
	<!---------------------------------------- 
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."footer.php" ); ?>
	
	
	<!-- End Site Wrapper -->
	
	</div>
	
</body>
</html>
