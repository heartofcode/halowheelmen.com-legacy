<?php

define( 'HTML_GUIDE_TITLE', '#(<div[^<>]*class="article-title[^<>]*>(?:.*?)<\/div>)#' );
define( 'HTML_GUIDE_DESCRIPTION', '#(<div[^<>]*class="article-info[^<>]*>(?:.*?)<\/div>)#' );
define( 'HTML_GUIDE_IMAGE', '#(<img[^<>]*class="article-image[^<>]*>(?:.*?)<\/img>)#' );

define( 'HTML_RIP_STYLE', '#(<(?:div|img)[^<>]*)rip-style ?([^<>]*)(style=[^<>]*)(>(?:.*?)<\/(?:div|img)>)#' );

function guidePreview( $subject )
{
	$content = "<div class='profile'>";
	
	preg_match( HTML_GUIDE_TITLE, $subject, $matches1 );
	if ( count($matches1) >= 1 )
	{
		$content .= $matches1[1];
	}
	
	preg_match( HTML_GUIDE_DESCRIPTION, $subject, $matches1 );
	if ( count($matches1) >= 1 )
	{
		$content .= $matches1[1];
	}
	
	$content .= "</div>";
	
	preg_match( HTML_GUIDE_IMAGE, $subject, $matches1 );
	if ( count($matches1) >= 1 )
	{
		$content .= $matches1[1];
	}
	
	$content = preg_replace( HTML_RIP_STYLE, "$1$2$4", $content );
	
	return $content;
}

function article( $subject )
{
	$subject = preg_replace( HTML_GUIDE_TITLE, "", $subject );
	$subject = preg_replace( HTML_GUIDE_DESCRIPTION, "", $subject );
	$subject = preg_replace( HTML_GUIDE_IMAGE, "", $subject );
	$subject = preg_replace( HTML_RIP_STYLE, "$1$2$4", $subject );
	
	return $subject;
}

?>