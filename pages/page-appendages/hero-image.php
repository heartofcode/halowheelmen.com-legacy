
<?php

function heroImage( $overlayText, $imagePath, $fullscreen )
{
	echo
		"<!----------------------------------------
			FEATURED MEDIA
		----------------------------------------->

		<div id='hero-image' class='img-container hero-image {$fullscreen}'>";
			
	if ( $fullscreen == "fullscreen" )
	{
		echo "<a href='#content' class='arrow-down smoothScroll'></a>";
	}
	
	echo
			"<div class='overlay black'><h1>{$overlayText}</h1></div>
			
			<img src='{$imagePath}'></img>
			
		</div>";
}

function heroVideo( $overlayText, $videoPath, $videoFormat, $fullscreen )
{
	echo
		"<!----------------------------------------
			FEATURED MEDIA
		----------------------------------------->
		
		<div id='hero-video' class='img-container hero-video {$fullscreen}'>";
		
	if ( $fullscreen == "fullscreen" )
	{
		echo "<a href='#content' class='arrow-down smoothScroll'></a>";
	}
	
	echo
		"<div class='overlay black'><h1>{$overlayText}</h1></div>
		
		<video width='auto' height='auto'autoplay loop muted>
			<source id='hero-video-source' src='{$videoPath}' type='video/{$videoFormat}'>
		</video>
		
	</div>";
}

?>