<?php

/******************************************************************************
* POST SYNDICATION SCRIPT by chAos
*
* A very basic script that pulls threads with the first post from the database
* and puts them into an array form so you can use them as you like.
*
* For use with phpBB3, freely distributable
*
******************************************************************************/

/** Notes:
* - Attachments haven't been handled properly.
* - Starts a forum session as Guest user, taking all the default values for time, bbcode style (from theme), etc
* - While viewing this page, users will appear to be viewing the Forum Index on viewonline.php.  
*   This can't be helped without modifying other code which is beyond this
*/

//////////////////////////////////////

// limit the number of posts that we query from the forums
define('POST_LIMIT', 3);

// If set to true, it will print the posts out
// If set to false it will create an array $news[] with all the following info
define('PRINT_TO_SCREEN', true);

//   'topic_id'         eg. 119
//   
//   'topic_time'      eg. 06 June, 07 (uses board default)
//   
//   'username'         eg. chAos
//   'topic_title'      eg. "News Post"
//   
//   'post_text'         eg. just the text (formatted w/ smilies, bbcode, etc)

//////////////////////////////////////

// FORUM_NEWS and FORUM_MOTM are in the forum-ids file under page-appendages.

$query = 
"SELECT u.user_id, u.username, t.topic_title, t.topic_poster, t.forum_id, t.topic_id, t.topic_time, t.topic_first_post_id, p.poster_id, p.topic_id, p.post_id, p.post_text, p.bbcode_bitfield, p.bbcode_uid 
FROM ".USERS_TABLE." u, ".TOPICS_TABLE." t, ".POSTS_TABLE." p 
WHERE u.user_id = t.topic_poster 
AND u.user_id = p.poster_id 
AND t.topic_id = p.topic_id 
AND p.post_id = t.topic_first_post_id 
AND (t.forum_id = ".FORUM_NEWS." OR t.forum_id = ".FORUM_MOTM.")
ORDER BY t.topic_time DESC";

$result = $db->sql_query_limit($query, POST_LIMIT);
$posts = array();
$news = array();
$bbcode_bitfield = '';
$message = '';
$poster_id = 0;

while ($row = $db->sql_fetchrow($result))
{

   $posts[] = array(
      'forum_id' => $row['forum_id'],
      'user_id' => $row['user_id'],
      'topic_id' => $row['topic_id'],
      'topic_time' => $row['topic_time'], 
      'username' => $row['username'], 
      'topic_title' => $row['topic_title'], 
      'post_text' => $row['post_text'],
      'bbcode_uid' => $row['bbcode_uid'],
      'bbcode_bitfield' => $row['bbcode_bitfield'],
   );

   $bbcode_bitfield = $bbcode_bitfield | base64_decode($row['bbcode_bitfield']);

}

// Instantiate BBCode
if ($bbcode_bitfield !== '')
{
   $bbcode = new bbcode(base64_encode($bbcode_bitfield));
}

// Output the posts
foreach ($posts as $m)
{

   $poster_id = $m['user_id'];
   
   $message = $m['post_text'];

   if ($m['bbcode_bitfield'])
   {
      $bbcode->bbcode_second_pass($message, $m['bbcode_uid'], $m['bbcode_bitfield']);
   }

   $message = str_replace("\n", '<br />', $message);
   $message = smiley_text($message);
   
   if (PRINT_TO_SCREEN) {

      /* Output is in the following format
       * <div class="article-preview">
       * <h2>Thread Title</h2>
       ^ <div class="article-date">date // 5 comments // poster</div>
       * <p>First post test</p>
       * </div>
       */

      echo "<div class='article-preview'>";
      echo "\n<div class='article-timestamp'>".$user->format_date($m['topic_time'])." // {$m['username']}</div>";
      echo "\n\n<a href='/halowheelmen.com/pages/dynamic/article.php?id={$m['topic_id']}'><h1>{$m['topic_title']}</h1></a>";
      echo "\n<p class='article-content'>{$message}</p>";
      echo "</div>";

   } else {

      $news[] = array(
         'topic_id' => $m['topic_id'], // eg: 119
         
         'topic_time' => $user->format_date($m['topic_time']), // eg: 06 June, 07 (uses board default)
         
         'username' => $m['username'], // eg: chAos
         'topic_title' => $m['topic_title'], // eg: "News Post"
         
         'post_text' => $message, // just the text         
         );
   }

   unset($message,$poster_id);

}

?>