
<!----------------------------------------
	START OF PAGE
----------------------------------------->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>


	<!----------------------------------------
		PHP VARIABLES
	----------------------------------------->

	<?php $includes = $_SERVER['DOCUMENT_ROOT']."/site/pages/page-appendages/"; ?>
	
	
	<!----------------------------------------
		HEADER
	----------------------------------------->
	
	<?php include( $includes."header.php" ); ?>

</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		NAVIGATION
	----------------------------------------->
	
	<?php include( $includes."/mobile-navigation.php" ); ?>
	
	
	<!-- Site Wrapper -->
	
	<div class="site-wrapper">
	
	
	<!-- Standard Navigation -->
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!----------------------------------------
		BANNER
		
		Preferably use absolute paths.
		The final parameter should be either
		"fullscreen" or ""
	----------------------------------------->	
	
	<?php # heroImage( "Title", "Path to Image", "fullscreen" ); ?>
	
	
	<!----------------------------------------
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none">
	
	
		<!-- Content goes here -->
	
	</div>
	
	
	<!---------------------------------------- 
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."footer.php" ); ?>
	
	
	<!-- End Site Wrapper -->
	
	</div>
	
</body>
</html>
