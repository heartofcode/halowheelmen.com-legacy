<?php

if(file_exists($serverRoot."/globals.php"))
{
	require_once($serverRoot."/globals.php");
}
elseif(file_exists("../globals.php"))
{
	require_once("../globals.php");
}


if(file_exists($serverRoot."/statistics/statClass.php"))
{
	require_once($serverRoot."/statistics/statClass.php");
}
elseif(file_exists("statClass.php"))
{
	require_once("statClass.php");
}

ini_set("memory_limit","90M");

class h3GroupStats
{
	public $arrUserList = array(); //array of h3stat objects

	public function __construct()
	{
		$this->clearAndLoad();
	}

	public function loadList()
	{
		//$result = mysql_query("Select * from groupStats");
		//$row = mysql_fetch_array($result);
		//$this = unserialize($row["stats"]);
	}

	//clears whatever users are in the array and loads up fresh from the database.
	public function clearAndLoad()
	{
		$this->arrUserList = array();
		$sql = "Select * from stats";
		$result = mysql_query($sql);
		for($i=0;$i<mysql_num_rows($result);$i++)
		{
			$row = mysql_fetch_array($result);
			//$tempStats = new h3Stat($row["gamertag"]);
			//echo "<br>$i GT: ".$row["gamertag"];
			$tempStats = unserialize($row["stats"]);
			$this->arrUserList[$row["gamertag"]] = $tempStats;
			unset($tempStats);
		}
	}

	public function sortList($sortCriteria,$rsb)
	{

		foreach ($this->arrUserList as $gamertag => $userObject)
		{
			//$temp = new h3Stat();
			//$temp->rWarthogDriver;
			//echo "<br>Processing $gamertag";
			//$userObject = new h3Stat("");
			switch ($sortCriteria)
			{
				case "LAAG":
				switch ($rsb)
				{
					case "ranked":
					$amount = $userObject->rLAAG;
					break;
					case "social":
					$amount = $userObject->sLAAG;
					break;
					case "both":
					$amount = $userObject->sLAAG + $userObject->rLAAG;
					default:
					break;
				}
				break;

				case "aveLAAG":
				switch ($rsb)
				{
					case "ranked":
					$userObject->rWheelmanGames == 0 ? $amount = 0: $amount = $userObject->rLAAG/$userObject->rWheelmanGames;
					break;
					case "social":
					$userObject->sWheelmanGames == 0 ? $amount = 0: $amount = $userObject->sLAAG/$userObject->sWheelmanGames;
					break;
					case "both":
					($userObject->sWheelmanGames == 0 && $userObject->rWheelmanGames==0) ? $amount = 0: $amount = ($userObject->sLAAG + $userObject->rLAAG)/($userObject->rWheelmanGames + $userObject->sWheelmanGames);
					default:
					break;
				}
				break;

				case "Gauss":
				switch ($rsb)
				{
					case "ranked":
					$amount = $userObject->rGauss;
					break;
					case "social":
					$amount = $userObject->sGauss;
					break;
					case "both":
					$amount = $userObject->rGauss + $userObject->sGauss;
					default:
					break;
				}
				break;

				case "aveGauss":
				switch ($rsb)
				{
					case "ranked":
					$userObject->rGaussGames == 0 ? $amount = 0: $amount = $userObject->rGauss/$userObject->rGaussGames;
					break;
					case "social":
					$userObject->sGaussGames == 0 ? $amount = 0: $amount = $userObject->sGauss/$userObject->sGaussGames;
					break;
					case "both":
					($userObject->sGaussGames == 0 && $userObject->rGaussGames==0) ? $amount = 0: $amount = ($userObject->rGauss + $userObject->sGauss)/($userObject->rGaussGames + $userObject->sGaussGames);
					default:
					break;
				}
				break;

				case "Wheelman":
				switch ($rsb)
				{
					case "ranked":
					$amount = $userObject->rWheelman;
					break;
					case "social":
					$amount = $userObject->sWheelman;
					break;
					case "both":
					$amount = $userObject->rWheelman + $userObject->sWheelman;
					default:
					break;
				}
				break;

				case "aveWheelman":
				switch ($rsb)
				{
					case "ranked":
					$userObject->rWheelmanGames == 0 ? $amount = 0: $amount = $userObject->rWheelman/$userObject->rWheelmanGames;
					break;
					case "social":
					$userObject->sWheelmanGames == 0 ? $amount = 0: $amount = $userObject->sWheelman/$userObject->sWheelmanGames;
					break;
					case "both":
					($userObject->sWheelmanGames == 0 && $userObject->rWheelmanGames==0) ? $amount = 0: $amount = ($userObject->rWheelman + $userObject->sWheelman)/($userObject->rWheelmanGames + $userObject->sWheelmanGames);
					default:
					break;
				}
				break;

				case "WarthogDriver":
				switch ($rsb)
				{
					case "ranked":
					$amount = $userObject->rWarthogDriver;
					break;
					case "social":
					$amount = $userObject->sWarthogDriver;
					break;
					case "both":
					$amount = $userObject->rWarthogDriver + $userObject->sWarthogDriver;
					default:
					break;
				}
				break;
				case "aveWarthogDriver":
				switch ($rsb)
				{
					case "ranked":
					$userObject->rWheelmanGames == 0 ? $amount = 0: $amount = $userObject->rWarthogDriver/$userObject->rWheelmanGames;
					break;
					case "social":
					$userObject->sWheelmanGames == 0 ? $amount = 0: $amount = $userObject->sWarthogDriver/$userObject->sWheelmanGames;
					break;
					case "both":
					($userObject->sWheelmanGames == 0 && $userObject->rWheelmanGames==0) ? $amount = 0: $amount = ($userObject->rWarthogDriver + $userObject->sWarthogDriver)/($userObject->rWheelmanGames + $userObject->sWheelmanGames);
					default:
					break;
				}
				break;
				case "gp":
				if(is_a($userObject,"h3Stat"))
				{
					$userObject->setGPPoints();
					$amount = $userObject->groundPounderPoints;
				}				

				//echo "<br>Amount: $amount";
				break;
				default:
				break;
			}

			//echo "$gamertag | amount: $amount";
			if(substr($sortCriteria,0,3) == "ave")
			{
				switch ($rsb)
				{
					case "ranked":
					$userObject->rWheelmanGames >= 200 ? $tempUserArray[$gamertag] = $amount : $nothing = "nothing";
					break;
					case "social":
					$userObject->sWheelmanGames >= 500 ? $tempUserArray[$gamertag] = $amount : $nothing = "nothing";
					break;
					case "both":
					($userObject->rWheelmanGames >= 200 || $userObject->sWheelmanGames >= 500) ? $tempUserArray[$gamertag] = $amount : $nothing = "nothing";
					default:
					break;
				}
			}
			elseif($sortCriteria=="gp")
			{
				($userObject->rAllGames >= 300 || $userObject->sAllGames >= 500) ? $tempUserArray[$gamertag] = $amount : $nothing = "nothing";
			}
			else
			{
				$tempUserArray[$gamertag] = $amount;
			}

		}
		arsort($tempUserArray);
		return $tempUserArray;

	}

} //end class


?>