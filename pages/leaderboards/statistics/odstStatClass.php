<?php



ini_set("memory_limit","80M");

if(file_exists($serverRoot."/globals.php"))
{
	require_once($serverRoot."/globals.php");
}

if(file_exists("../globals.php"))
{
	require_once("../globals.php");
}



class odstStats
{
	public $arrffMedals;
	public $arrffVehicles;
	public $arrffWeapons;
	public $arrffEnemies;

	public $arrMedals;
	public $arrWeapons;
	public $arrVehicles;
	public $arrEnemies;
	
	
	//other vairables
	public $gamertag;
	public $safeGamertag;
	public $tempID;
	public $url;
	
	public $ff_points;
	public $ff_kills;
	public $ff_deaths;
	public $ff_games;
	public $ff_ppg;
	public $ff_kpg;
	public $ff_dpg;
	public $ff_ppd;
	public $ff_kpd;
	public $ff_ppk;
	public $ff_highscore;
	
	public $points;
	public $kills;
	public $deaths;
	public $games;
	public $ppg;
	public $kpg;
	public $dpg;
	public $ppd;
	public $kpd;
	public $ppk;
	public $highscore;	
	

	public function __construct($gamertag)
	{
		$this->gamertag = $gamertag;
		$this->safeGamertag = str_replace(" ","+",$gamertag);		
		$this->url = "http://www.bungie.net/stats/halo3/careerstatsodst.aspx?player=".$this->safeGamertag;
	}
		
	public function runStats()
	{
		$display = true;
		$url = $this->url."&mode=1";
		if(url_exists($url))
		{
			$page_contents = file_get_contents($url);	
			if(strpos($page_contents, 'Halo 3 Service Record Not Found') === false)
			{	
				$i = strpos($page_contents,"ctl00_mainContent_psPlayer_hypHighScore");
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->ff_highscore = substr($page_contents,$i,$j-$i);
				
				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->ff_points = substr($page_contents,$i,$j-$i);				

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"P",$i);
				$this->ff_ppg = trim(substr($page_contents,$i,$j-$i));	

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"P",$i);
				$this->ff_ppd = trim(substr($page_contents,$i,$j-$i));	

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"P",$i);
				$this->ff_ppk = trim(substr($page_contents,$i,$j-$i));	

				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->ff_kills = substr($page_contents,$i,$j-$i);					
	
				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"K",$i);
				$this->ff_kpg = trim(substr($page_contents,$i,$j-$i));	
				
				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"K",$i);
				$this->ff_kpd = trim(substr($page_contents,$i,$j-$i));					
				
				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->ff_deaths = substr($page_contents,$i,$j-$i);						

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"D",$i);
				$this->ff_dpg = trim(substr($page_contents,$i,$j-$i));		
				
				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->ff_games = substr($page_contents,$i,$j-$i);	
				
												
				$odstitem = new ODSTitem("Spartan Laser");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Spartan Laser - WAV M6 GGNR');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Chopper");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Chopper - (Type-25 RAV)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}			
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Pistol");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Auto Mag - M6C/SOCOM');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";					
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Hammer");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Gravity Hammer - (Type-2 EW/H)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Fuel Rod Cannon");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Fuel Rod Gun - (Type-33 LAAW)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Rocket Launcher");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Rocket Launcher - M41 MAV/AW');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;			
				
				$odstitem = new ODSTitem("Plasma Grenade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Grenade - (Type-1 AP-G)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
								
				$odstitem = new ODSTitem("Silenced SMG");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Silenced SMG - M7S/Caseless');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}								
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Spike Grenade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Spike Grenade - (Type-2 AP-FG)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Melee");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Somebody smashed somebody!');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Heavy Machine Gun");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Heavy Machine Gun - AIE-486H');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Frag Grenade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Frag Grenade - M9 HE-DP');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Explosion");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'WUH-BLAM');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Carbine");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Carbine - (Type-51 Carbine)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;				
																
				$odstitem = new ODSTitem("Ghost");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Ghost - (Type-32 RAV)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
																
				$odstitem = new ODSTitem("Warthog Driver");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Warthog - Driver');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("FireBomb");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Firebomb - (Type-3 AP/AM-IG)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
												
				
				$odstitem = new ODSTitem("Missle Pod");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Missile Pod - LAU-65D');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Warthog LAAG");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Turret - M41 LAAG');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Guardians");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Killed, once again, by the guardians.');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;

				$odstitem = new ODSTitem("Sniper");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sniper Rifle - SRS 99D AM');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;				
				
				$odstitem = new ODSTitem("Needler");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Needler - (Type-33 GML)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;				
				
				$odstitem = new ODSTitem("Shotgun");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Shotgun - M90A CAWS');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Mauler");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Mauler - (Type-52 Pistol)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Brute Shot");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Shot - (Type-25 GL)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Plasma Rifle");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Rifle - (Type-25 DER)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
							
				$odstitem = new ODSTitem("Classified - I'm calling my lawyer!");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, "I'm calling my lawyer!");
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Plasma Pistol");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Pistol - (Type-25 DEP)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Beam Rifle");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Beam Rifle - (Type-50 SR)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Plasma Cannon");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Cannon - (Type-42 DESW)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Wraith - Driver");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Wraith - Driver');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}													
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Tripmine");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Tripmine - TR/9 AP Mine');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Scorpion");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Scorpion - M808B MBT');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Banshee");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Banshee - (Type-26 GSA)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
				
								
				$odstitem = new ODSTitem("Gauss");
				$odstitemitemType = "weapon";
				$odstitemtype = "firefight";
				$i = strpos($page_contents, 'Warthog - M68 ALIM');
				if($i===false)
				{
					$odstitempoints = "-";
					$odstitemkills = "-";
					$odstitemppk = "-";
					$odstitemkd = "-";
					$odstitemdeaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitempoints = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitemkills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitemppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitemkd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
												
				$odstitem = new ODSTitem("Scorpion Turret");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Scorpion - M247T MMG');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Mongoose");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Mongoose - M274 ULATV');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("FlameThrower");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'FlameThrower - M7057 DP');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
												
				$odstitem = new ODSTitem("Spiker");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Spiker - (Type-25 Carbine)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Shade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Shade - (Type-26 ASG)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Wraith - Gunner");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Wraith - Gunner');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Classified - Iiiiiiiieeeeeeeee!!!");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Iiiiiiiieeeeeeeee!!!');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
								
				//medals aquire				
				
				$odstitem = new ODSTitem("Killing Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Killing Spree!');				
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					$i = strpos($page_contents,"(",$i);					
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
																
				$odstitem = new ODSTitem("Killing Frenzy");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Killing Frenzy!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Running Riot");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Running Riot!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Rampage");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Rampage!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				
								
				$odstitem = new ODSTitem("Untouchable");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Untouchable!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Invincible");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Invincible!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Shotgun Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Shotgun Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
							
																								
				$odstitem = new ODSTitem("Open Season");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Open Season!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
									
				
				$odstitem = new ODSTitem("Sniper Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sniper Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;					
				
				$odstitem = new ODSTitem("Sharpshooter");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sharpshooter!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Splatter Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Splatter Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Vehicular Manslaughter");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Vehicular Manslaughter!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Hammer Spree!");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Hammer Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Dream Crusher");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Dream Crusher!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Stick Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Stick Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Sticky Fingers");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sticky Fingers!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;				
				


				$odstitem = new ODSTitem("Double Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Double Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{
					
					$odstitem->kills = "-";
					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;				
				
				$odstitem = new ODSTitem("Triple Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Triple Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{
					
					$odstitem->kills = "-";
					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);		
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;					
				
				$odstitem = new ODSTitem("Overkill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Overkill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Killtacular");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killtacular!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
												
				$odstitem = new ODSTitem("Killtrocity");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killtrocity!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Killimanjaro");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killimanjaro!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Killtastrophe");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killtastrophe!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
								
				$odstitem = new ODSTitem("Killpocalypse");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killpocalypse!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Killionaire");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killionaire!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;					
								
				$odstitem = new ODSTitem("Beat Down");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Beat Down!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Assassin");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Assassin!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Grenade Stick");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Grenade Stick!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Headshot");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Headshot!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Laser Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Laser Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Splatter");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Splatter!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Needler Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Needler Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("EMP Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'EMP Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Incineration");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Incineration!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrffMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Death from the Grave");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Death from the Grave!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrffMedals[$odstitem->itemName] = $odstitem;								
											
				$odstitem = new ODSTitem("Vehicle Jack");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Vehicle Jack!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrffMedals[$odstitem->itemName] = $odstitem;						
																
				$odstitem = new ODSTitem("Assist");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Assist!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrffMedals[$odstitem->itemName] = $odstitem;					
											
				$this->arrffMedals[$odstitem->itemName] = $odstitem;						
																
				$odstitem = new ODSTitem("Hero");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Hero!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrffMedals[$odstitem->itemName] = $odstitem;	
				
				
//FF enemies
				$odstitem = new ODSTitem("Brute Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Infantry',$i);
					$i = strpos($page_contents,'Brute Infantry',$i);
					$i = strpos($page_contents,'Brute Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Grunt Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Grunt Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Grunt Infantry',$i);
					$i = strpos($page_contents,'Grunt Infantry',$i);
					$i = strpos($page_contents,'Grunt Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Brute Hero");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Hero');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Hero',$i);
					$i = strpos($page_contents,'Brute Hero',$i);
					$i = strpos($page_contents,'Brute Hero',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Brute Leader");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Leader');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Leader',$i);
					$i = strpos($page_contents,'Brute Leader',$i);
					$i = strpos($page_contents,'Brute Leader',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Brute Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Spec-Ops',$i);
					$i = strpos($page_contents,'Brute Spec-Ops',$i);
					$i = strpos($page_contents,'Brute Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Jackal Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Jackal Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Jackal Spec-Ops',$i);
					$i = strpos($page_contents,'Jackal Spec-Ops',$i);
					$i = strpos($page_contents,'Jackal Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Hunter Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Hunter Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Hunter Spec-Ops',$i);
					$i = strpos($page_contents,'Hunter Spec-Ops',$i);
					$i = strpos($page_contents,'Hunter Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Drone Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Drone Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Drone Infantry',$i);
					$i = strpos($page_contents,'Drone Infantry',$i);
					$i = strpos($page_contents,'Drone Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Grunt Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Grunt Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Grunt Spec-Ops',$i);
					$i = strpos($page_contents,'Grunt Spec-Ops',$i);
					$i = strpos($page_contents,'Grunt Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Jackal Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Jackal Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Jackal Infantry',$i);
					$i = strpos($page_contents,'Jackal Infantry',$i);
					$i = strpos($page_contents,'Jackal Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Drone Leader ");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Drone Leader ');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Drone Leader ',$i);
					$i = strpos($page_contents,'Drone Leader ',$i);
					$i = strpos($page_contents,'Drone Leader ',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Engineer");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Engineer');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Engineer',$i);
					$i = strpos($page_contents,'Engineer',$i);
					$i = strpos($page_contents,'Engineer',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffEnemies[$odstitem->itemName] = $odstitem;	
				
//FF Vehicles
				$odstitem = new ODSTitem("Wraith");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Wraith',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Chopper");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Chopper',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Banshee");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Banshee',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Mongoose");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Mongoose',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Warthog");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Warthog',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Scorpion");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Scorpion',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Shade");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Shade',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Ghost");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Ghost',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Phantom");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Phantom',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Scarab");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Scarab',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Recharge");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Recharge',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrffVehicles[$odstitem->itemName] = $odstitem;					
							
																			
			}
			else
			{
				if($display)
					echo "<br><b>".$this->gamertag." doesn't exist.  Deleting</b>";
				mysql_query("delete from odststats where LCASE(gamertag) = '".strtolower($this->gamertag)."'");
				//mysql_query("update phpbb3_profile_fields_data set pf_user_gamertag = 'test' WHERE LCASE(pf_user_gamertag) = '".strtolower($this->gamertag)."'");
				return false;
			}
		}	
		
		
		$url = $this->url."&mode=0";
		if(url_exists($url))
		{
			$page_contents = file_get_contents($url);	
			if(strpos($page_contents, 'Halo 3 Service Record Not Found') === false)
			{
	
				$i = strpos($page_contents,"ctl00_mainContent_psPlayer_hypHighScore");
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->highscore = substr($page_contents,$i,$j-$i);
				
				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->points = substr($page_contents,$i,$j-$i);				

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"P",$i);
				$this->ppg = trim(substr($page_contents,$i,$j-$i));	

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"P",$i);
				$this->ppd = trim(substr($page_contents,$i,$j-$i));	

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"P",$i);
				$this->ppk = trim(substr($page_contents,$i,$j-$i));	

				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->kills = substr($page_contents,$i,$j-$i);					
	
				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"K",$i);
				$this->kpg = trim(substr($page_contents,$i,$j-$i));	
				
				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"K",$i);
				$this->kpd = trim(substr($page_contents,$i,$j-$i));					
				
				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->deaths = substr($page_contents,$i,$j-$i);						

				$i = strpos($page_contents,'<li>',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"D",$i);
				$this->dpg = trim(substr($page_contents,$i,$j-$i));		
				
				$i = strpos($page_contents,'class="first"',$i);
				$i = strpos($page_contents,">",$i);
				$i++;
				$j = strpos($page_contents,"<",$i);
				$this->games = substr($page_contents,$i,$j-$i);				
				
																			
				$odstitem = new ODSTitem("Spartan Laser");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Spartan Laser - WAV M6 GGNR');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Chopper");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Chopper - (Type-25 RAV)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}			
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Pistol");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Auto Mag - M6C/SOCOM');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";					
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Hammer");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Gravity Hammer - (Type-2 EW/H)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Fuel Rod Cannon");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Fuel Rod Gun - (Type-33 LAAW)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Rocket Launcher");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Rocket Launcher - M41 MAV/AW');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;			
				
				$odstitem = new ODSTitem("Plasma Grenade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Grenade - (Type-1 AP-G)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
								
				$odstitem = new ODSTitem("Silenced SMG");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Silenced SMG - M7S/Caseless');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}								
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Spike Grenade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Spike Grenade - (Type-2 AP-FG)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Melee");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Somebody smashed somebody!');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Heavy Machine Gun");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Heavy Machine Gun - AIE-486H');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Frag Grenade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Frag Grenade - M9 HE-DP');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Explosion");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'WUH-BLAM');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Carbine");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Carbine - (Type-51 Carbine)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;				
																
				$odstitem = new ODSTitem("Ghost");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Ghost - (Type-32 RAV)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
																
				$odstitem = new ODSTitem("Warthog Driver");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Warthog - Driver');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("FireBomb");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Firebomb - (Type-3 AP/AM-IG)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
												
				
				$odstitem = new ODSTitem("Missle Pod");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Missile Pod - LAU-65D');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Warthog LAAG");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Turret - M41 LAAG');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Guardians");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Killed, once again, by the guardians.');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;

				$odstitem = new ODSTitem("Sniper");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sniper Rifle - SRS 99D AM');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;				
				
				$odstitem = new ODSTitem("Needler");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Needler - (Type-33 GML)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;				
				
				$odstitem = new ODSTitem("Shotgun");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Shotgun - M90A CAWS');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Mauler");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Mauler - (Type-52 Pistol)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Brute Shot");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Shot - (Type-25 GL)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Plasma Rifle");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Rifle - (Type-25 DER)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
							
				$odstitem = new ODSTitem("Classified - I'm calling my lawyer!");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, "I'm calling my lawyer!");
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Plasma Pistol");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Pistol - (Type-25 DEP)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Beam Rifle");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Beam Rifle - (Type-50 SR)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Plasma Cannon");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Plasma Cannon - (Type-42 DESW)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Wraith - Driver");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Wraith - Driver');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}													
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Tripmine");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Tripmine - TR/9 AP Mine');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Scorpion");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Scorpion - M808B MBT');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Banshee");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Banshee - (Type-26 GSA)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
				
								
				$odstitem = new ODSTitem("Gauss");
				$odstitemitemType = "weapon";
				$odstitemtype = "firefight";
				$i = strpos($page_contents, 'Warthog - M68 ALIM');
				if($i===false)
				{
					$odstitempoints = "-";
					$odstitemkills = "-";
					$odstitemppk = "-";
					$odstitemkd = "-";
					$odstitemdeaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitempoints = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitemkills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitemppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitemkd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
												
				$odstitem = new ODSTitem("Scorpion Turret");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Scorpion - M247T MMG');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Mongoose");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Mongoose - M274 ULATV');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("FlameThrower");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'FlameThrower - M7057 DP');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
												
				$odstitem = new ODSTitem("Spiker");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Spiker - (Type-25 Carbine)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Shade");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Shade - (Type-26 ASG)');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Wraith - Gunner");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Wraith - Gunner');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrWeapons[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Classified - Iiiiiiiieeeeeeeee!!!");
				$odstitem->itemType = "weapon";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Iiiiiiiieeeeeeeee!!!');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";
				}
				else
				{
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}					
								
				//medals aquire				
				
				$odstitem = new ODSTitem("Killing Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Killing Spree!');				
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					$i = strpos($page_contents,"(",$i);					
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}					
				$this->arrMedals[$odstitem->itemName] = $odstitem;
																
				$odstitem = new ODSTitem("Killing Frenzy");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Killing Frenzy!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Running Riot");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Running Riot!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Rampage");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Rampage!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				
								
				$odstitem = new ODSTitem("Untouchable");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Untouchable!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Invincible");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Invincible!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
								
				$odstitem = new ODSTitem("Shotgun Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Shotgun Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
							
																								
				$odstitem = new ODSTitem("Open Season");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Open Season!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;
									
				
				$odstitem = new ODSTitem("Sniper Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sniper Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;					
				
				$odstitem = new ODSTitem("Sharpshooter");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sharpshooter!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Splatter Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Splatter Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Vehicular Manslaughter");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Vehicular Manslaughter!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Hammer Spree!");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Hammer Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Dream Crusher");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Dream Crusher!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Stick Spree");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Stick Spree!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				
				$odstitem = new ODSTitem("Sticky Fingers");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Sticky Fingers!');
				$odstitem->deaths = "-";
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,'Points',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->points = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);												
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;				
				


				$odstitem = new ODSTitem("Double Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Double Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{
					
					$odstitem->kills = "-";
					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;				
				
				$odstitem = new ODSTitem("Triple Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Triple Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{
					
					$odstitem->kills = "-";
					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);		
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;					
				
				$odstitem = new ODSTitem("Overkill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Overkill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Killtacular");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killtacular!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
												
				$odstitem = new ODSTitem("Killtrocity");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killtrocity!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Killimanjaro");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killimanjaro!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Killtastrophe");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killtastrophe!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
								
				$odstitem = new ODSTitem("Killpocalypse");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killpocalypse!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Killionaire");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Killionaire!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;					
								
				$odstitem = new ODSTitem("Beat Down");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Beat Down!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Assassin");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Assassin!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Grenade Stick");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Grenade Stick!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Headshot");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Headshot!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Laser Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Laser Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Splatter");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Splatter!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Needler Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Needler Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("EMP Kill");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'EMP Kill!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Incineration");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Incineration!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				$this->arrMedals[$odstitem->itemName] = $odstitem;
				
				$odstitem = new ODSTitem("Death from the Grave");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Death from the Grave!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrMedals[$odstitem->itemName] = $odstitem;								
											
				$odstitem = new ODSTitem("Vehicle Jack");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Vehicle Jack!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrMedals[$odstitem->itemName] = $odstitem;						
																
				$odstitem = new ODSTitem("Assist");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Assist!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrMedals[$odstitem->itemName] = $odstitem;					
											
				$this->arrMedals[$odstitem->itemName] = $odstitem;						
																
				$odstitem = new ODSTitem("Hero");
				$odstitem->itemType = "medal";
				$odstitem->type = "firefight";
				
				$i = strpos($page_contents, 'Hero!');
				$odstitem->deaths = "-";
				$odstitem->points = "-";
				$odstitem->ppk = "-";
				
				if($i===false)
				{					
					$odstitem->kills = "-";					
					$odstitem->kd = "-";
					
				}
				else
				{
					$i = strpos($page_contents,'Earned',$i);
					$i = strpos($page_contents,'number',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"(",$i);
					$odstitem->kills = trim(substr($page_contents,$i,$j-$i));
					
					$i = $j;
					$i++;
					$j = strpos($page_contents,")",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);
				}	
				
				$this->arrMedals[$odstitem->itemName] = $odstitem;	
				
				
//FF enemies
				$odstitem = new ODSTitem("Brute Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Infantry',$i);
					$i = strpos($page_contents,'Brute Infantry',$i);
					$i = strpos($page_contents,'Brute Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Grunt Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Grunt Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Grunt Infantry',$i);
					$i = strpos($page_contents,'Grunt Infantry',$i);
					$i = strpos($page_contents,'Grunt Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Brute Hero");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Hero');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Hero',$i);
					$i = strpos($page_contents,'Brute Hero',$i);
					$i = strpos($page_contents,'Brute Hero',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Brute Leader");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Leader');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Leader',$i);
					$i = strpos($page_contents,'Brute Leader',$i);
					$i = strpos($page_contents,'Brute Leader',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Brute Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Brute Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Brute Spec-Ops',$i);
					$i = strpos($page_contents,'Brute Spec-Ops',$i);
					$i = strpos($page_contents,'Brute Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Jackal Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Jackal Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Jackal Spec-Ops',$i);
					$i = strpos($page_contents,'Jackal Spec-Ops',$i);
					$i = strpos($page_contents,'Jackal Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Hunter Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Hunter Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Hunter Spec-Ops',$i);
					$i = strpos($page_contents,'Hunter Spec-Ops',$i);
					$i = strpos($page_contents,'Hunter Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Drone Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Drone Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Drone Infantry',$i);
					$i = strpos($page_contents,'Drone Infantry',$i);
					$i = strpos($page_contents,'Drone Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Grunt Spec-Ops");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Grunt Spec-Ops');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Grunt Spec-Ops',$i);
					$i = strpos($page_contents,'Grunt Spec-Ops',$i);
					$i = strpos($page_contents,'Grunt Spec-Ops',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Jackal Infantry");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Jackal Infantry');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Jackal Infantry',$i);
					$i = strpos($page_contents,'Jackal Infantry',$i);
					$i = strpos($page_contents,'Jackal Infantry',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Drone Leader ");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Drone Leader ');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Drone Leader ',$i);
					$i = strpos($page_contents,'Drone Leader ',$i);
					$i = strpos($page_contents,'Drone Leader ',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
				$odstitem = new ODSTitem("Engineer");
				$odstitem->itemType = "enemy";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, 'Engineer');
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'Engineer',$i);
					$i = strpos($page_contents,'Engineer',$i);
					$i = strpos($page_contents,'Engineer',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrEnemies[$odstitem->itemName] = $odstitem;	
				
//FF Vehicles
				$odstitem = new ODSTitem("Wraith");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Wraith',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Chopper");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Chopper',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Banshee");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Banshee',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Mongoose");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Mongoose',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Warthog");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Warthog',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Scorpion");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Scorpion',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Shade");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Shade',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Ghost");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Ghost',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Phantom");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Phantom',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Scarab");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Scarab',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
				$odstitem = new ODSTitem("Recharge");
				$odstitem->itemType = "vehicle";
				$odstitem->type = "firefight";
				$i = strpos($page_contents, '<div class="vehicles">');
				$i = strpos($page_contents, 'Recharge',$i);
				if($i===false)
				{
					$odstitem->points = "-";
					$odstitem->kills = "-";
					$odstitem->ppk = "-";
					$odstitem->kd = "-";
					$odstitem->deaths = "-";							
				}
				else
				{
					$i = strpos($page_contents,'<ul>',$i);
					$i = strpos($page_contents,'class="points active"',$i);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->points = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"kills",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kills = substr($page_contents,$i,$j-$i);
					
					$i = strpos($page_contents,"pd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->ppk = substr($page_contents,$i,$j-$i);				
					
					$i = strpos($page_contents,"kd",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->kd = substr($page_contents,$i,$j-$i);	
					
					$i = strpos($page_contents,"deaths",$j);
					$i = strpos($page_contents,'>',$i);
					$i++;
					$j = strpos($page_contents,"<",$i);
					$odstitem->deaths = substr($page_contents,$i,$j-$i);												
				}
				$this->arrVehicles[$odstitem->itemName] = $odstitem;					
							
																			
						
			}
			else
			{
				if($display)
					echo "<br><b>".$this->gamertag." doesn't exist.  Deleting</b>";
				mysql_query("delete from odststats where LCASE(gamertag) = '".strtolower($this->gamertag)."'");
				//mysql_query("update phpbb3_profile_fields_data set pf_user_gamertag = 'test' WHERE LCASE(pf_user_gamertag) = '".strtolower($this->gamertag)."'");
				return false;
			}
		}
	
		$result = mysql_query("select * from odststats where gamertag = '".$this->gamertag."'");
		//echo "<br>GT:".$this->gamertag." before serializing";
		$serialized = addslashes(serialize($this));
		if(mysql_num_rows($result) > 0) //update
		{
			//echo "<br> ".$this->gamertag." being updated.  Sericalised = ".strlen($serialized);
			if(strlen($serialized) < 33554432  && mysql_query("update odststats set stats = '$serialized', updateDate = now() where LCASE(gamertag) = '".strtolower($this->gamertag)."'"))
			{
				if($display)
					echo "<br>".$this->gamertag." Updated!";
				mysql_query("update odststats set flagged = 0, updateDate = now() where LCASE(gamertag) = '".strtolower($this->gamertag)."'");
			}
			else
			{
				if($display)
					echo "<br>".$this->gamertag." update FAILED";// | update stats set stats = '$serialized' where gamertag = '".$this->gamertag."'";
				if($display)						
					echo "<br>".mysql_error();
				mysql_query("delete from odststats where LCASE(gamertag) = '".strtolower($this->gamertag)."'");
				mysql_query("update phpbb3_profile_fields_data set pf_user_gamertag = 'test' WHERE LCASE(pf_user_gamertag) = '".strtolower($this->gamertag)."'");
			}
		}
		else //insert
		{
			if(strlen($serialized) < 33554432  && mysql_query("insert into odststats (gamertag,stats,updateDate) values ('".$this->gamertag."','$serialized',now())"))
			{
				if($display)
					echo "<br>".$this->gamertag." INSERTED!";
			}
			else
			{
				if($display)
					echo "<br>".$this->gamertag." insert FAILED "; //| insert into stats (gamertag,stats) values ('".$this->gamertag."','$serialized')";
			}
		}	
	}
	

}
	
class ODSTitem
{
	public function __construct($name)
	{
		$this->itemName = $name;
	}
	public $points;
	public $kills; //count for medals
	public $ppk;   //points per game for meadls
	public $kd;    //count per game for meadls
	public $deaths;
	
	public $itemName;
	public $itemType; //weaspon, enemy, vehicle, medal
	public $type; //firefight or campaign

}

?>
