<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?


include_once("groupClass.php");
include_once("statClass.php");
include_once("../globals.php");


isset($_GET["type"]) ? $type = $_GET["type"] : $type = "ranked";
isset($_GET["sort"]) ? $sort = $_GET["sort"] : $sort = "aveLaag";
echo "<!-- type: $type | sort: $sort-->";
$group = new h3GroupStats();

if($type == "ranked" || $type == "rankedAll")
{
	switch ($sort)
	{
		case "laag":
			$list = $group->sortList("LAAG","ranked");
			break;

		case "gauss":
			$list = $group->sortList("Gauss","ranked");
			break;

		case "aveLaag":
			$list = $group->sortList("aveLAAG","ranked");
			break;
		case "aveGauss":
			$list = $group->sortList("aveGauss","ranked");
			break;
		default:
			break;
	}

}
elseif ($type=="social" || $type == "socialAll")
{
	switch ($sort)
	{
		case "laag":
			$list = $group->sortList("LAAG","social");
			break;

		case "gauss":
			$list = $group->sortList("Gauss","social");
			break;

		case "aveLaag":
			$list = $group->sortList("aveLAAG","social");
			break;
		case "aveGauss":
			$list = $group->sortList("aveGauss","social");
			break;
		default:
			break;
	}
}



if($type == "socialAll" || $type == "rankedAll")
{
	$count = sizeof($list);

}
else
{
	$count = 20;
}
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled 1</title>
<base target="_self" />

<style type="text/css">
A:link {
	COLOR: #ffffff; TEXT-DECORATION: underline
}
A:visited {
	COLOR: #ffffff; TEXT-DECORATION: underline
}
A:hover {
	COLOR: #b3b3b3; TEXT-DECORATION: none
}
A:active {
	COLOR: #ffffff; TEXT-DECORATION: underline
}

.style1 {
	text-align: center;
	background-color: #424345;
	font-family: Arial;
	color: #FFFFFF;
}
.style2 {
	color:#FFFFFF;
	background-color: #2e2f31;
}
.style3 {
	font-family: Arial;
	color:#FFFFFF;
	background-color: #2e2f31;
}
.style4 {
	font-family: Arial;
	text-align: center;
	color:#FFFFFF;
	background-color: #2e2f31;
}
.style5 {
	font-family: Arial;
	color:#FFFFFF;
	background-color: #424345;
}
.style6 {
	font-family: Arial;
	color:#FFFFFF;
	background-color: #424345;
}
</style>
</head>

<body style="background-color: #2e2f31; margin-top: 5px;">

<table class="style2" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td class="style1" colspan="6">Top <? echo ucfirst(substr($type,0,6))?> Gunners</td>
	</tr>
	<tr>
		<td style="width: 21px" class="style3">&nbsp;</td>
		<td style="width: 201px" class="style3">Gamertag</td>
		<td style="width: 107px" class="style4" colspan="2"><div align="center">LAAG</div></td>
		<td style="width: 133px" class="style4" colspan="2"><div align="center">Gauss</div></td>
	</tr>
	<tr>
		<td style="width: 21px" class="style3">&nbsp;</td>
		<td style="width: 201px" class="style3">&nbsp;
		</td>
		<td style="width: 107px" class="style3"><div align="right"><a href="gunner.php?type=<? echo $type?>&sort=laag">Total</a></div></td>
		<td style="width: 107px" class="style3"><div align="right"><a href="gunner.php?type=<? echo $type?>&sort=aveLaag">Ave</a></div></td>
		<td style="width: 133px" class="style3"><div align="right"><a href="gunner.php?type=<? echo $type?>&sort=gauss">Total</a></div></td>
		<td style="width: 133px" class="style3"><div align="right"><a href="gunner.php?type=<? echo $type?>&sort=aveGauss">Ave</a></div></td>
	</tr>
		<?
		$i=0;
		foreach ($list as $gamertag => $value)
		{
			if($i>$count-1)
			break;

			$result = mysql_query("Select * from stats where gamertag = '$gamertag'");
			if($result)
			{
				$row = mysql_fetch_array($result);
				$user = unserialize($row["stats"]);
	
				//$user = new h3Stat($gamertag);
				$rLAAG = $user->rLAAG;
				$rGauss = $user->rGauss;
				$sLAAG = $user->sLAAG;
				$sGauss = $user->sGauss;
				$rGames = $user->rWheelmanGames;
				$sGames = $user->sWheelmanGames;
				$rGaussG = $user->rGaussGames;
				$sGaussG = $user->sGaussGames;
	
				if(substr($type,0,6)=="ranked")
				{
					$rGaussG == 0 ? $spave = 0 : $spave = $rGauss/$rGaussG;
					$rGames == 0 ? $wave = 0 : $wave = $rLAAG/$rGames;
				}
				else
				{
					$sGaussG == 0 ? $spave = 0 : $spave = $sGauss/$sGaussG;
					$sGames == 0 ? $wave = 0 : $wave = $sLAAG/$sGames;
					
				}
				if($i%2==1)
				$class = 3;
				else
				$class = 6;

    ?>

	<tr>
		<td style="width: 21px" class="style<? echo $class?>"><? echo $i+1?></td>
		<td style="width: 201px" class="style<? echo $class?>">
		<?
		if(substr($type,0,6)=="ranked")
		{?>
		<a target="_blank" href="http://www.bungie.net/Stats/Halo3/CareerStats.aspx?player=<?echo $gamertag?>&social=False"><?echo $gamertag?></a>
		<?}
		else
		{
		?>
		<a target="_blank" href="http://www.bungie.net/Stats/Halo3/CareerStats.aspx?player=<?echo $gamertag?>&social=true"><?echo $gamertag?></a>

		<?}?>
		</td>
		<td style="width: 107px" class="style<? echo $class?>">
		    <div align="right">
		        <?
		if(substr($type,0,6)=="ranked")
		{
			echo $rLAAG;
		}
		else
		{
			echo $sLAAG;
		}
		?>
	        </div></td>
		<td style="width: 107px" class="style<? echo $class?>">
	        <div align="right"><?echo number_format(round($wave,2),2)?></div>
        </td>
		<td style="width: 133px" class="style<? echo $class?>">
		    <div align="right">
		        <?
		if(substr($type,0,6)=="ranked")
			echo $rGauss;
		else
			echo $sGauss;
		?>
            </div></td>
		<td style="width: 133px" class="style<? echo $class?>">
	    <div align="right"><?echo number_format(round($spave,2),2)?></div></td>
	</tr>
	<?
	$i++;
	}
}

		?>
	<tr>
		<td style="width: 21px" class="style3">&nbsp;</td>
		<td style="width: 201px" class="style3">&nbsp;</td>
		<td style="width: 107px" class="style3" colspan="2">&nbsp;</td>
		<td style="width: 133px" class="style3" colspan="2">
		<?
		if($type <> "rankedAll" and $type <> "socialAll")
		{
			?><a href="gunner.php?type=<? echo $type?>All&sort=<? echo $sort?>">See all --&gt;</a><?}?>&nbsp;</td>
	</tr>
</table>

<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3489741-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<?
mysql_free_result($result);
mysql_close();
?>
</body>

</html>
