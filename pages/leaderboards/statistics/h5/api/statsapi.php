<?php
/**
 * Created by PhpStorm.
 * User: Josh
 * Date: 2/27/2016
 * Time: 4:46 PM
 */
require('../statGlobals.php');
require('apiCommon.php');
ob_start();
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
print_r($request);
$stats = new statClass($mysqli);

switch ($request[0]) {
    case 'runmatch':
        $return = $stats->runMatchByID($request[1],$request[2],$request[3],$request[4],$request[5]);
        ob_end_clean();
        sendResponse( 200, $return,'text/html');
        break;
    case 'getMatches':
        echo "<br>Run get matches";
        //set_time_limit(5000);
        ini_set('memory_limit', '128M');
        ini_set('max_execution_time','5000');
        $gamertag = $request[1];
        $oHaloApi = new haloapi($stats->apiKey, array(urlencode($gamertag)));
        $returnArray = $stats->getAPIdata($gamertag);

        if($returnArray == ''){
            $returnArray = [];
        }

        $start = sizeof($returnArray);
        $count = 25;

        $resultCount = 1;
        $currentCount = 0;

        while($resultCount > 0 && $currentCount < 1000) {
            echo "<br>Launching with start: $start and count: $count";
            $params['start'] = $start;
            $params['count'] = $count;

            $statusCode = 429;
            while($statusCode == 429) {
                $matches = $oHaloApi->getPlayerMatches($params);

                if($matches["statusCode"] == 429) {

                    $startMsg = strpos($matches["message"],"Try again in")+13;
                    $sleepCount = substr($matches["message"],$startMsg,strpos($matches["message"]," ",$startMsg)-$startMsg);
                    echo "<br>over rate limit";
                    echo "<br>Sleeping for $sleepCount seconds";
                    //print_r($matches);

                    //sleep(intval(trim($sleepCount)));


                } else {
                    echo "<br>We're good, continuing";
                    $statusCode = 0;
                }
            }
            //print_r($matches);
            $start+= $matches["ResultCount"];
            $currentCount+=$matches["ResultCount"];
            $resultCount = $matches["ResultCount"];
            //echo "<br>Result Count: $resultCount";
            //print_r($matches);


/*
            for ($i = 0; $i < sizeof($matches["Results"]); $i++) {
                $match = $matches["Results"][$i];
                $matchID = $match["Id"]["MatchId"];
                $gameMode = $match["Id"]["GameMode"];
                $gameDate = $match['MatchCompletedDate']['ISO8601Date'];

                $returnArray[] = array('matchid' => $matchID, 'gamemode' => $gameMode, 'gamedate' => $gameDate );
            }
*/
            $returnArray = array_merge($returnArray,$matches["Results"]);

        }
        echo "<br>Array Size: ".sizeof($returnArray);
        if($start >= 1000 && $resultCount > 0) {
            $stats->setAPIdata($gamertag,$returnArray);
            ob_end_clean();
            sendResponse(200, json_encode('more'), 'text/json');
        } else {
            echo "<br>Array size to return: ".sizeof($returnArray);

            echo "<br>End result: " . sizeof($returnArray);
            ob_end_clean();
            sendResponse(200, json_encode($returnArray), 'text/json');
        }
        break;
    case 'getDbGames':
        $gamertag = $request[1];
        $recordedGames = $stats->getDatabaseGames($gamertag);
        echo "<br>Recorded games: ".sizeof($recordedGames);
        print_r($recordedGames);
        ob_end_clean();
        sendResponse( 200, json_encode($recordedGames),'text/json');
        break;
    case 'getPlayerID':
        $gamertag = $request[1];
        $playerID = $stats->getPlayerID($gamertag);
        ob_end_clean();
        sendResponse(200,$playerID);
        break;
    case 'getUntimedGames':
    	$games = $stats->getUntimedGames();
    	ob_end_clean();
    	sendResponse( 200, json_encode($games),'text/json');
    	break;
    case 'getGameTime':
    	$game = $request[1];
    	$stats->getGameTime($game);

    	sendResponse(200,json_encode('true'),'text/json');
    	break;
}