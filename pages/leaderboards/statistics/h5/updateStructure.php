<?php
/**
 * Created by PhpStorm.
 * User: Josh
 * Date: 1/1/2016
 * Time: 11:03 AM
 */

require('statGlobals.php');
ini_set('display_errors',1);
function update_vehicles($sApiKey,$mysqli){
    echo "<br>UPDATING VEHICLES";
    $oHaloApi = new haloapi($sApiKey, array('HWM Brickfungus'));
    $vehicles = $oHaloApi->getVehicles();
    for($i=0;$i<sizeof($vehicles);$i++){
        $vehicle = $vehicles[$i];
        echo "<br>Updating: ".$vehicle['name'];
        $vehicle['isUsableByPlayer'] ? $usable = 1 : $usable = 0;

        $sql = "Replace into vehicle values (".$vehicle['id'].",'".$vehicle['name']."','".$mysqli->real_escape_string($vehicle['description'])."','".$vehicle['largeIconImageUrl']."','".$vehicle['smallIconImageUrl']."',".$usable.",'".$vehicle['contentId']."')";
        $mysqli->query($sql);
    }
    echo "<br><br>";
}

function update_weapons($sApiKey,$mysqli){
    echo "<br>UPDATING WEAPONS";
    $oHaloApi = new haloapi($sApiKey, array('HWM Brickfungus'));
    $weapons = $oHaloApi->getWeapons();
    for($i=0;$i<sizeof($weapons);$i++){
        $weapon = $weapons[$i];

        echo "<br>Updating: ".$weapon['name'];
        $weapon['isUsableByPlayer'] ? $usable = 1 : $usable = 0;

        $sql = "Replace into weapon values (".$weapon['id'].",'".$weapon['name']."','".$mysqli->real_escape_string($weapon['description'])."','".$weapon['smallIconImageUrl']."','".$weapon['largeIconImageUrl']."','".$weapon['type']."',".$usable.",'".$weapon['contentId']."')";
        $mysqli->query($sql);

    }

    echo "<br><br>";
}


function update_gametype($sApiKey,$mysqli){
    $oHaloApi = new haloapi($sApiKey, array('HWM Brickfungus'));
    $varients = $oHaloApi->getGameBaseVariants();

    echo "<br>UPDATING GAME TYPES";
    for($i=0;$i<sizeof($varients);$i++){
        $variant = $varients[$i];
        //print_r($variant);
        echo "<br>Updating: ".$variant['name'];

        $strSQL = "Replace into variant values ('".$variant['id']."', '".$mysqli->real_escape_string($variant['name'])."','".$mysqli->real_escape_string($variant['iconUrl'])."')";
        $result = $mysqli->query($strSQL);
        $variantID = $variant['id'];
        //print_r($variant['supportedGameModes']);
        for($j=0;$j<sizeof($variant['supportedGameModes']);$j++)
        {
            $currGameMode = $variant['supportedGameModes'][$j];
            echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;Adding $currGameMode to this varient";
            $strGameModeSQL = "Select * from gamemode where name = '".$mysqli->real_escape_string($currGameMode)."'";
            $result = $mysqli->query($strGameModeSQL);
            $row = $result->fetch_assoc();
            $gameModeID = $row['id'];

            $strSQL = "insert into variant_gamemode values ('$variantID',$gameModeID)";
            $mysqli->query($strSQL);
        }
    }
    echo "<br><br>";
}

function update_maps($sApiKey,$mysqli){
    $oHaloApi = new haloapi($sApiKey, array('HWM Brickfungus'));
    echo "<br><br>UPDATING MAPS";

    $maps = $oHaloApi->getMaps();

    //print_r($maps);
    //return;

    for($i=0;$i<sizeof($maps);$i++){
        $map = $maps[$i];
        //print_r($map);
        echo "<br>Updating: ".$map['name'];

        $sql = "Replace into map values ('".$map['id']."','".$map['name']."','".$mysqli->real_escape_string($map['description'])."','".$map['imageUrl']."')";

        $mysqli->query($sql);
        $mapID = $map['id'];

        for($j=0;$j<sizeof($map['supportedGameModes']);$j++){
            $sql = "Select * from gamemode where name = '".$map['supportedGameModes'][$j]."'";
            $result = $mysqli->query($sql);
            if($result->num_rows > 0) {
                $row = $result->fetch_object();
                $sql = "Replace into map_gamemode values ('$mapID',".$row->id.")";
                $result = $mysqli->query($sql);
            } else { //insert
                $sql = "Insert into gamemode (name) values (".$map['supportedGameModes'][$j].")";
                $result = $mysqli->query($sql);
                $gameModeID = mysqli_insert_id($mysqli);
                $sql = "Replace into map_gamemode values ('$mapID',$gameModeID)";
                $result = $mysqli->query($sql);
            }
        }
    }

    echo "<br><br>";
}

function update_medals($sApiKey,$mysqli){
    $oHaloApi = new haloapi($sApiKey, array('HWM Brickfungus'));
    echo "<br><br>UPDATING MEDALS";

    $medals = $oHaloApi->getMedals();
    //print_r($medals);

    for($i=0;$i<sizeof($medals);$i++){
        $medal = $medals[$i];

        echo "<br>Updating: ".$medal['name'];
       
        $sql = "Replace into medal values (".$medal['id'].",'".$mysqli->real_escape_string($medal['description'])."','".$medal['classification']."','".$medal['spriteLocation']['spriteSheetUri']."',".$medal['spriteLocation']['left'].",".$medal['spriteLocation']['top'].",".$medal['spriteLocation']['height'].",".$medal['spriteLocation']['spriteWidth'].",".$medal['spriteLocation']['spriteHeight'].",".$medal['difficulty'].",'".$medal['name']."',".$medal['spriteLocation']['width'].")";
        $mysqli->query($sql);

    }
}

function update_enemies($sApiKey,$mysqli){
    $oHaloApi = new haloapi($sApiKey,array('HWM BrickFungus'));
    echo "<br><br>UPDATING ENEMIES";

    $enemies = $oHaloApi->getEnemies();
    //print_r($enemies);

    for($i=0;$i<sizeof($enemies);$i++){
        $enemy = $enemies[$i];

        echo "<br>Updating: ".$enemy['name'];

        $sql = "Replace into enemy values (".$enemy['id'].",'".$mysqli->real_escape_string($enemy['faction'])."','".$mysqli->real_escape_string($enemy['name'])."','".$enemy['largeIconImageUrl']."','".$enemy['smallIconImageUrl']."','".$enemy['contentId']."')";
        $mysqli->query($sql);

    }
}


update_vehicles($sApiKey,$mysqli);
update_weapons($sApiKey,$mysqli);
update_gametype($sApiKey,$mysqli);
update_maps($sApiKey,$mysqli);
update_medals($sApiKey,$mysqli);
update_enemies($sApiKey,$mysqli);
