<?php
require('../statGlobals.php');

$url = 'https://www.halowaypoint.com/en-us/games/halo-5-guardians/xbox-one/commendations/spartan-companies/halowheelmen';

echo "<br>$url";

$page_contents = file_get_contents($url);

$i = strpos($page_contents,"nav-links--l2");
$i = strpos($page_contents, 'Summary</a>',$i);
$i = strpos($page_contents, 'href',$i) +6;
$j = strpos($page_contents, '"',$i);
$kill = substr($page_contents, $i,$j-$i);

echo "<br>Kill URL: $kill";

$i = strpos($page_contents, 'href',$i) +6;
$j = strpos($page_contents, '"',$i);
$assist = substr($page_contents, $i,$j-$i);

echo "<br>Assist URL: $assist";

$i = strpos($page_contents, 'href',$i) +6;
$j = strpos($page_contents, '"',$i);
$gamemode = substr($page_contents, $i,$j-$i);

echo "<br>GameMode URL: $gamemode";


$killPage = file_get_contents($kill);


$i = strpos($killPage, '<h2 class="text--larger">Kill Commendations</h2>');
$i = strpos($killPage, '</h2>',$i);

$i = strpos($killPage, 'text--large',$i);

while($i !== false) {
    $i = strpos($killPage, '>',$i) + 1;
    $j = strpos($killPage, '</p>',$i);
    $title = substr($killPage, $i,$j-$i);
    echo "<br>title: $title";

    $i=strpos($killPage, 'text--smallest',$i);
    $i = strpos($killPage, '>',$i) + 1;
    $j = strpos($killPage, '</p>',$i);
    $level = substr($killPage, $i,$j-$i);
    echo "<br>Level: $level";

    $i=strpos($killPage, 'description text--smaller',$i);
    $i = strpos($killPage, '>',$i) + 1;
    $j = strpos($killPage, '</p>',$i);
    $desc = substr($killPage, $i,$j-$i);
    echo "<br>Description: $desc";

    $i=strpos($killPage, 'numeric--small xp',$i);
    $i = strpos($killPage, '>',$i) + 1;
    $j = strpos($killPage, '</p>',$i);
    $num = substr($killPage, $i,$j-$i);
    echo "<br>Number: $num";

    
    $arrNum = explode('/', $num);
    $percentage = $arrNum[0]/$arrNum[1];

    $mysqli->query("INSERT INTO  company (
        id ,
        name ,
        level ,
        description ,
        count ,
        total ,
        percentage ,
        updateDate,
        type
        )
        VALUES (NULL ,  '$title',  '$level',  '$desc',  '".$arrNum[0]."',  '".$arrNum[1]."',  '$percentage',  '".date("Y-m-d H:i:s")."','kill'
        )");
    $i = strpos($killPage, 'text--large',$i);
}
echo "<BR><BR>";
$assistPage = file_get_contents($assist);
$i = strpos($assistPage, '<h2 class="text--larger">Assist Commendations</h2>');
$i = strpos($assistPage, '</h2>',$i);

$i = strpos($assistPage, 'text--large',$i);

while($i !== false) {
    $i = strpos($assistPage, '>',$i) + 1;
    $j = strpos($assistPage, '</p>',$i);
    $title = substr($assistPage, $i,$j-$i);
    echo "<br>title: $title";

    $i=strpos($assistPage, 'text--smallest',$i);
    $i = strpos($assistPage, '>',$i) + 1;
    $j = strpos($assistPage, '</p>',$i);
    $level = substr($assistPage, $i,$j-$i);
    echo "<br>Level: $level";

    $i=strpos($assistPage, 'description text--smaller',$i);
    $i = strpos($assistPage, '>',$i) + 1;
    $j = strpos($assistPage, '</p>',$i);
    $desc = substr($assistPage, $i,$j-$i);
    echo "<br>Description: $desc";

    $i=strpos($assistPage, 'numeric--small xp',$i);
    $i = strpos($assistPage, '>',$i) + 1;
    $j = strpos($assistPage, '</p>',$i);
    $num = substr($assistPage, $i,$j-$i);
    echo "<br>Number: $num";
    $arrNum = explode('/', $num);
    $percentage = $arrNum[0]/$arrNum[1];

    $mysqli->query("INSERT INTO  company (
        id ,
        name ,
        level ,
        description ,
        count ,
        total ,
        percentage ,
        updateDate,
        type
        )
        VALUES (
        NULL ,  '$title',  '$level',  '$desc',  '".$arrNum[0]."',  '".$arrNum[1]."',  '$percentage',  '".date("Y-m-d H:i:s")."','assist'
        )");
    $i = strpos($assistPage, 'text--large',$i);
}


echo "<BR><BR>";
$gamemodePage = file_get_contents($gamemode);
$i = strpos($gamemodePage, '<h2 class="text--larger">Game Mode Commendations</h2>');
$i = strpos($gamemodePage, '</h2>',$i);

$i = strpos($gamemodePage, 'text--large',$i);

while($i !== false) {
    $i = strpos($gamemodePage, '>',$i) + 1;
    $j = strpos($gamemodePage, '</p>',$i);
    $title = substr($gamemodePage, $i,$j-$i);
    echo "<br>title: $title";

    $i=strpos($gamemodePage, 'text--smallest',$i);
    $i = strpos($gamemodePage, '>',$i) + 1;
    $j = strpos($gamemodePage, '</p>',$i);
    $level = substr($gamemodePage, $i,$j-$i);
    echo "<br>Level: $level";

    $i=strpos($gamemodePage, 'description text--smaller',$i);
    $i = strpos($gamemodePage, '>',$i) + 1;
    $j = strpos($gamemodePage, '</p>',$i);
    $desc = substr($gamemodePage, $i,$j-$i);
    echo "<br>Description: $desc";

    $i=strpos($gamemodePage, 'numeric--small xp',$i);
    $i = strpos($gamemodePage, '>',$i) + 1;
    $j = strpos($gamemodePage, '</p>',$i);
    $num = substr($gamemodePage, $i,$j-$i);
    echo "<br>Number: $num";

    $arrNum = explode('/', $num);
    
    $percentage = $arrNum[0]/$arrNum[1];

    $mysqli->query("INSERT INTO  company (
        id ,
        name ,
        level ,
        description ,
        count ,
        total ,
        percentage ,
        updateDate,
        type
        )
        VALUES (
        NULL ,  '$title',  '$level',  '$desc',  '".$arrNum[0]."',  '".$arrNum[1]."',  '$percentage',  '".date("Y-m-d H:i:s")."','gamemode'
        )");
    $i = strpos($gamemodePage, 'text--large',$i);
}

//$mysqli->query('TRUNCATE TABLE  company_board');


$result = $mysqli->query("select * from company_board");

while($row = $result->fetch_assoc()){
    $name = $row['name'];
    echo "<br><br>updating $name";
    

    $strSQL = "Select * from company where name = '".$mysqli->real_escape_string($name)."' ORDER BY  updateDate DESC LIMIT 0 , 1";
    $result2 = $mysqli->query($strSQL);
    $latsetRow = $result2->fetch_assoc();
    $currentCount = $latsetRow["count"];
    $amountNeeded = $latsetRow["total"];
    $level = substr($latsetRow["level"],strpos($latsetRow["level"],'/')-1,1);
    $amountLeft = $amountNeeded - $currentCount;
    $arrLvl = [];
    for($i=1;$i<=intval($level);$i++){
        $strSQL = "SELECT (MAX( count ) - MIN( count )) / (COUNT( DISTINCT (count) ) -1) avecount FROM company WHERE name =  '$name' and level = 'level $i/5'";
        $result2 = $mysqli->query($strSQL);
        $aveRow = $result2->fetch_assoc();
        $aveCount = $aveRow["avecount"];
        
        if($aveCount == 0 || $aveCount == NULL) {
            $arrLvl[] = 0;
        } else {
            $arrLvl[] = $aveCount;
        }
    }
    $arrLvl = array_filter($arrLvl);
    print_r($arrLvl);

   
    if(empty($arrLvl)) {
        $avecount = 0;
    } else {
        $avecount = array_sum($arrLvl) / count($arrLvl);
    }
    
    if($avecount == '')
        $avecount = 0;
    
    $level = $latsetRow["level"];
    $percentage = $latsetRow["percentage"];

    $strSQL = "update company_board set 
        level = '$level', 
        count = $currentCount,
        total = $amountNeeded,
        percentage = $percentage,
        updateDate =  '".date("Y-m-d H:i:s")."',
        projected = $avecount
        where name = '".$mysqli->real_escape_string($name)."'
        ";
    echo "<br>$strSQL";
    $resultInsert = $mysqli->query($strSQL);
    if(!$resultInsert){
        echo "<BR><b>QUERY FAILED</b>";
    }

}