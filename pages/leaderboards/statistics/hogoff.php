<?php

include("stat_globals.php");

$strSQL = "select * from hogoff ";

empty($_GET["sort"]) ? $sort = "wm" : $sort = $_GET["sort"];

if($sort == "gun")
{
	$sort = "(chain+gauss)";
}

if($sort == "player_gamertag")
{
	$strSQL .= " order by $sort asc";
}
else
{
	$strSQL .= " order by $sort desc";
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>h3Wheelmen.com - Hogoff Stats</title>
<link rel="stylesheet" type="text/css" href="../styles/style2p1.css" >
<style type="text/css"> 
 
.thrColLiqHdr #header {
	background-color: #000000;
	background-image: url(../images/top-20-banner.jpg);
	height: 260px;
} 
.thrColLiqHdr #header h1 {
	margin: 0; /* zeroing the margin of the last element in the #header div will avoid margin collapse - an unexplainable space between divs. If the div has a border around it, this is not necessary as that also avoids the margin collapse */
	padding: 10px 0; /* using padding instead of margin will allow you to keep the element away from the edges of the div */
	height: 260px;
} 

.darkRow {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color:#FFF;
	background-color: #202123;
	background-image: url(../images/cell_bg.gif);
	background-repeat:repeat-x;
}
.lightRow {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color:#FFFFFF;
	background-color: #424345;
}



</style>
</head>
<body class="thrColLiqHdr">
<div id="container">
 <?php include("../header.htm");?>

<div id="header">
    <h1>&nbsp;</h1>
  <!-- end #header --></div>
  
  <div id="mainContent">
  <p>
  <?php// include("../statMenus.htm");?>    
    
    
<table style="color:#FFFFFF;background-color: #2e2f31; width:100%" cellspacing="5" cellpadding="3" align="center">
	<tr>
		<td style="width: 21px" class="style.categories">&nbsp;</td>
		<td  class="style.categories" ><span class="style16"><a href="hogoff.php?sort=player_gamertag" class="style16">Gamertag</a></span></td>
       
	  <td style="width: 120px" class="style.categories"><a href="hogoff.php?sort=wm" class="style16">Wheelmen</a></td>
	  <td style="width: 120px" class="style.categories"><a href="hogoff.php?sort=gun" class="style16">Gunner Kills</a></td>
	  <td style="width: 120px" class="style.categories"><a href="hogoff.php?sort=chain" class="style16"><em>Chaingun</em></a></td>
	  <td style="width: 120px" class="style.categories"><a href="hogoff.php?sort=gauss" class="style16"><em>Gauss</em></a></td>
	</tr>
		
	<?
	$results = mysql_query($strSQL);
	
	for($i=1;$i<=mysql_num_rows($results);$i++)
	{
			
		if($i%2==0)
			$class = "darkRow";
		else
			$class = "lightRow";
			
		echo "<tr>";
		
		
			$row = mysql_fetch_array($results);
			echo "<!--Start of $gamertag-->";
			
			$gamertag = $row["player_gamertag"];
			$wm = $row["wm"];
			$chain = $row["chain"];
			$gauss = $row["gauss"];
			
				?>
				<tr><td  class="<?=$class?>"><?=$i?>.</td>
                <td  class="<?=$class?>">
                    <a target="_blank" href="http://www.bungie.net/Stats/Halo3/CareerStats.aspx?player=<?=$gamertag?>&social=False"><?=$gamertag?></a>	  </td>
                <td class="<?=$class?>"><?=$wm?></td>                
                <td class="<?=$class?>"><?=($chain+$gauss)?></td>
                <td class="<?=$class?>"><?=$chain?></td>
                <td class="<?=$class?>"><?=$gauss?></td>
	</tr>
		<?
		
		echo "<!--End of $gamertag-->";
		}


	

		?>
	</table>
      <!-- end #mainContent -->
    </p>
    <p class="style2"><br class="clearfloat" />
    </p>
  </div>
  <div id="footer">
    <p>&nbsp;</p>
  <!-- end #footer --></div>
<!-- end #container --></div>    
    

<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3489741-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<?
mysql_free_result($result);
mysql_close();
?>
</body>

</html>
