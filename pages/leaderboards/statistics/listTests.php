<?php

//wheelman medals
$strSQL = "SELECT SUM( pgm.amount ) , COUNT( pgm.game_id ) , p.player_gamertag
FROM  game_player_medal pgm
INNER JOIN game g ON pgm.game_id = g.game_id
AND (
g.variant_code >=1
AND g.variant_code <=3
)
INNER JOIN player p ON pgm.player_id = p.player_id
WHERE pgm.medal_code =74
GROUP BY p.player_gamertag
ORDER BY SUM( pgm.amount ) DESC ";


//driver kills
$strSQL = "SELECT SUM( gpw.kills ) , COUNT( pgm.game_id ) , p.player_gamertag
FROM  game_player_weapon gpw
INNER JOIN game g ON gpw.game_id = g.game_id
AND (
g.variant_code >=1
AND g.variant_code <=3
)
INNER JOIN player p ON gpw.player_id = p.player_id
WHERE gpw.weapon_id =33
GROUP BY p.player_gamertag
ORDER BY SUM( pgm.kills ) DESC ";

//all wm games
$strSQL =  "SELECT COUNT( DISTINCT gp.game_id ) , gp.player_id
FROM game_player gp
INNER JOIN game g ON gp.game_id = g.game_id
AND (
g.variant_code >=1
AND g.variant_code <=3
)
INNER JOIN game_player_medal gpm ON gp.game_id = gpm.game_id
INNER JOIN game_player_weapon gpw ON gp.game_id = gpw.game_id
WHERE (
gpm.medal_code =74
OR gpw.weapon_id =33
)
GROUP BY gp.player_id";




?>