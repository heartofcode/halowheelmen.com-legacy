<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled 2</title>

<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.5.1/build/reset-fonts-grids/reset-fonts-grids.css"> 
<style type="text/css" media="all">
html {list-style-image:none;}
html {
    background-color: #336633} 
a, a:link, a:visited, a:active {
	color: #0069AA}
a:visited {
	color: #7B98AA}
a, a:link, a:visited, a:active {
  text-decoration:none;
}
a:hover {
  text-decoration:underline;
}

body #doc4 {
    z-index: 1;
    position: relative;
}

#hd, #bd, #ft {
    position: relative;
}

#hd { z-index: 2; }
#bd { z-index: 1; }
#ft { z-index: 0; }

#yspbody {
  text-align:center;
  margin:0 auto;
  width:974px;
  padding:0 0 20px 0;
}
#yspheader{
  margin:0 auto;
  text-align:left;
  padding:0;
  zoom:1;
}
#yspfooter {
  padding:0 0 5px 0;
  margin:0 auto;
  background:#fff;
  text-align:left;
  zoom:1;
}
#yspfooter small {
  font:10px Verdana;
}
#yspfooter img {
 float:right;
 display:block;
}
#yspfooter center {
  display:block;
}
  /* IE Z-Index Bug: Do Not Apply Position Relative to These Containers */
  /* Relative positioning has been applied to id="hd" instead to avoid */
  /* multiple Z-index stacking contexts: (http://therealcrisp.xs4all.nl/meuk/IE-zindexbug.html) */
#yspcontentfooter, #yspcontentheader {
  margin:0 auto;
  clear:both;
  text-align:left;
}
#yspglobalform {
  margin:0;
}
#yspcontent {
  margin:0;
}
#yspsearch {
  border:1px solid #ccc;
  border-width:1px 0;
  background:#eee; text-align:center;
  clear:both;
  margin:0 10px;
  padding:10px 0;
  zoom:1;
}
#yspsearch form {
  margin:0 auto;
  text-align:center;
  line-height:100%;
}
#yspcopyright {
  margin: 0 10px 0 10px;
  padding-top:15px;
  border-top:#8d8d8d solid 1px;
  font:77% Verdana;
  text-align:left;
  clear: both;
}
#yspcopyright img {
  float:right;
  margin-left:10px;
}
#yspnocss {
  display:none;
}
#yspmh {
  margin-top:0px;
  text-align:left;
  width:100%;
  position:relative;
}
#yspteammh {
  color:#FFD9B4;
  zoom:1;
  font-size: 85%;
}
#yspteammh ul {
  float:right;
  position:relative;
  z-index:1000;
}
#yspteammh ul li a {
  font-weight: normal;
}
#yspteammh span {
  display: inline;
  position:relative;
  z-index:1000;  
}
#yspteammh span em {
  display:none;
}
#yspteammh ul {
  z-index:1000;
  position:relative;
  display: inline;
}
#yspteammh ul li {
  margin:0;
  padding:0;
  display: inline;
  float: none;
  font-weight: bold;
}
#yspteammh a {
  color:#fff;
  padding:0 5px;
  margin:0;
  text-decoration:none;
}
#yspteammh a:hover {
  text-decoration:underline;
}
#yspteammh li.last a {
  border-right:0px none;
  padding-right:0;
}
#yspmh h2 {
  margin:0;
}
a.anchor {
  *display:none;
}
#yspmain {
  zoom:1;
  background:#fff;
}
#yspcontentfooter {
  background:#fff;
}
#yspcontent {
  padding:0;
}
#yspmain h1 {
  font: 167% Arial;
  margin:0 0 6px 0;
  padding:0px 0 6px 0;
  text-align:left;
    color: #790000;
}
.yspcontent #yspmain h1 {
  margin-top:10px;
}
/* Start Layout Containers */
.yspindex #yspcontent {
  width:974px;
  border-top:10px #fff solid;
  margin-left:auto;
  margin-right:auto;
}
.yspspecial #yspcontent  {
  margin-bottom:0;
}
.yspspecial #yspmain  {
  text-align:center;
  padding-bottom:1px;
  padding: 0;
}
.yspspecial #yspmain .moduletabs {
  margin-left:10px;
  margin-right:10px;
}
.yspspecial #yspsub  {
  text-align:left;
  position: static;
}
.yspspecial #yspmain h1 {
  text-align:left;
  padding:10px 10px 0 10px;
}
.yspindex #yspsub {
  margin-left:0px;
  padding-bottom:20px;
}
.yspsubcontent #yspcontent, .yspcontent #yspcontent{
  width:974px;
  margin-left: auto;
  margin-right: auto;
  background:#fff;
  _height:200px;
  min-height:200px;
}
.yspsubcontent #yspmain, .yspcontent #yspmain {
  margin:0 10px;
  text-align:left;
}

.yspsubcontent #yspsub, .yspcontent #yspsub {
  background-color: white;
}

.yspcontent #yspsub {
  width:180px;
}
/* End Layout Containers */
/* Start Navigation */
.navlist {
  font-weight:bold;
}
.navlist ul {
  margin:0;
  padding:0;
  list-style-type:none;
}
.navlist li {
  float:left;
  margin:0;
  *min-height:10px;
}
.navlist a {
  margin:0;
  padding:0;
  float:left;
  white-space:nowrap;
}
.navlist strong {
  margin:0;
  padding:0;
  float:left;
  white-space:nowrap;
}
.navlist em {
  font-style:normal;
  float:left;
}
.navlist h4 {
  display:none;
}
#yspnav {
  border-bottom:0;
  font-weight: bold;
  font-size: 84%;
  zoom:1;
  position: relative;
  padding: 0;
  text-transform: uppercase;  
}
#yspnav a.anchor,#yspsubnav a.anchor {
  display:none;
}
#yspnav a, #yspnav strong {
  border: none;
  padding:7px 15px 6px;
  color:#fff;
  line-height:150%;
  text-decoration:none;
}
#yspnav strong {
  color:#999;
}
#yspnav a:hover {
  text-decoration:underline;
}
#yspnav li.selected, #yspnav.nav-league li.selected,
#yspnav li.selected:first-child, #yspnav.nav-league li.selected:first-child,
#yspnav ul li.selected-first, #yspnav.nav-league li.selected-first {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png) no-repeat -1px -701px #fff;
  margin-top: 1px;
  padding-top: 3px;
  border-right: 1px solid #999;
}

#yspnav li.last {
  float:right;
  padding-left:30px;
}
#yspnav li.selected a {
  color:#343434;
  _line-height:1;
}


#yspsubnav {
  background: white url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png)  left -609px repeat-x;
  border-top:0;
  font-size: 77%;
  zoom:1;
}

#yspsubnav .bd {
  background: transparent url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png)  left -402px no-repeat;
  overflow: hidden;
  zoom:1;
  margin-left: 1px;


}

#yspsubnav ul {
    background: transparent url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png) right -402px no-repeat;
    float: left;
    padding: 0 6px 5px 6px;
    padding-bottom: 5px;
    padding-right: 6px;
    min-width: 64em;
    _width: 650px;
    _width: auto !important;
    zoom:1;
}

#yspsubnav li {
    background: transparent url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png) -18px -402px no-repeat;
    white-space: nowrap;
}
#yspsubnav li.first {
    background-position: -22px -402px;
}

#yspsubnav a , #yspsubnav strong {
  padding:5px 10px 10px 10px;
  text-decoration:none;
  line-height:150%;
}

#yspsubnav a:visited {
	color: #0069AA}
.yspindex #yspsubnav a , .yspindex  #yspsubnav strong {
  padding:5px 10px 10px 10px;
}

#yspsubnav strong {
  color:#999;
  font-weight:bold;
}
#yspsubnav li.first a {
}
#yspsubnav li.selected a {
  font-weight:bold;
  color:#000;
}
#yspsubnav li.disabled a {
  color:#999;
}
#yspsubnav a:hover {
  text-decoration:underline;
}
#yspsubnav {
  margin-bottom:0;
  padding-bottom: 10px;
}
#yspnavempty div {
 padding:14px 0;
 zoom:1;
 font:1% Arial;
 text-indent:-9000px;
}
.yspcontent #yspnavempty {
  xpadding-bottom:15px;
  xbackground:#fff;
}

/* NEW 2008 */
#yspsubnav li.subsection-label strong {
    font-weight: bold;
    color: #7F7F7F;
}
#yspsubnav li.subsection {
    background-position: -28px -402px;
}
#yspnav li a {
    float: none;
}
#yspnav li.selected a {
    padding: 7px 15px 11px;
    *padding-top: 8px;
}

#yspnav li.last,
#yspnav.nav-league li.last {
    background-image: none;
}
.yuimenu .yuimenu,
.yuimenubar .yuimenu {
    visibility:hidden;
    position:absolute;
    top:-10000px;
    left:-10000px;
    background-position: -395px 100%;
    background-repeat: no-repeat;
    _border-right: 1px solid #999;
    _background-color: #999;    
}
html>body .yuimenu .yuimenu,
html>body .yuimenubar .yuimenu {

    background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/box-shadow.png); 
}
.ysfnavflyout h5 {
    display: none;
}
#yspnav .ysfnavflyout li,
#yspnav li.selected .ysfnavflyout li,
#yspnav.nav-league .ysfnavflyout li,
#yspnav.nav-league li.selected .ysfnavflyout li {
    background-image: none;
    background-color: #fff;
    border-top: 1px dotted #bbb;
    border-left: 1px solid #999;
    _border-top: 1px solid #bbb;
    display: block;
    float: none;
    margin: 0;
    padding: 0;
    zoom:1;
    width: 100%;
    list-style-type: none;
  margin: 0;

}

#yspnav .ysfnavflyout .bd > ul > li:first-child,
#yspnav li.selected .ysfnavflyout .bd > ul > li:first-child {
    border-top: none;
}

#yspnav .ysfnavflyout li.last,
#yspnav .ysfnavflyout li:last-child,
#yspnav li.selected .ysfnavflyout li.last,
#yspnav.nav-league .ysfnavflyout li.last,
#yspnav.nav-league li.selected .ysfnavflyout li.last {
  float:none;
  background:white;
  margin: 0;
  padding:0;
  border-bottom: 1px solid #999;
}
#yspnav .ysfnavflyout li.last a {
    padding: 5px 15px 6px;
}
.ysfnavflyout ul{
    border: none;
    margin: 0 0;
    position: relative;
    top: -1px;
    margin: 0 7px 0 -10px;
    _margin: 0;
    overflow: hidden;
    background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/box-shadow.png);
    background-position: 3px 100%;
    background-repeat: no-repeat;
    padding-bottom: 3px;
    _padding-bottom: 1px;
    zoom: 1;
    *width: 175px;
}
#yspnav .ysfnavflyout li a,
#yspnav .ysfnavflyout li strong {
    display: block;
    background: none;
    border: none;
    float: none;
    font-size: 92%;
    position: relative;
    width: 100%;
    zoom: 1;
    display: block;
}
#yspnav .ysfnavflyout li a,
#yspnav .ysfnavflyout li a:hover {
	color: #0069AA;
	text-transform: none;
}
.yui-skin-sam #yspnav .yuimenubaritemlabel-selected {
    border: none;
    margin-left: 0;
/*    *margin-left: 1px;
    *left: -1px;*/
}

#yspnav .ysfnavflyout li a:hover,
#yspnav .ysfnavflyout li.first a:hover,
#yspnav .ysfnavflyout li.last a:hover,
#yspnav li.selected .ysfnavflyout a:hover,
#yspnav .yuimenuitem-selected,
#yspnav li.selected .ysfnavflyout li a:hover {
    background-color: #ddd;
    background-image: none;
    text-decoration: none;
}
#yspnav .yuimenu .bd {
	position: relative;
	    top: 0;
	width: 175px;
	right: -10px;
	_right: auto;
	_left: -2px;
	background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/box-shadow.png);
	background-position: right bottom;
	background-repeat: no-repeat;
	zoom: 1;

}

#yspnav #ysf-fantasyflyout h5,
#yspnav #ysf-fantasyflyout h6 {
    background-color: white;
    text-align: left;
    background-image: none;
    color: #a1230d;
    position: relative;
    margin: 0pt 10px 0pt -10px;
    _margin:0;
    top: -1px;

    font-weight: bold;
    border-left: 1px solid #999;
    padding-left: 10px;
    display:block;
    float:none;
    width: 239px;
}

#yspnav #ysf-fantasyflyout ul.ysf-teamlist li.last,
#yspnav #ysf-fantasyflyout ul.ysf-teamlist li:last-child {
    border-bottom-style: dotted;
}

#yspnav #ysf-fantasyflyout h6 a,
#yspnav #ysf-fantasyflyout h6 a:visited,
#yspnav #ysf-fantasyflyout h6 a:link {
    color: #0069AA;
    padding-left: 0;
    background-image: none;
    text-transform: none;
}

#yspnav #ysf-fantasyflyout ul.ysf-teamlist {
    padding-bottom: 0;
}

#yspnav li.last #ysf-fantasyflyout li.ysf-team a,
#yspnav li.last #ysf-fantasyflyout ul.ysf-teamlist li.ysf-team a {
    display: inline;
    background-image: none;
    padding: 2px 0;
    margin: 0;
    font-weight: normal;
    text-transform: none;

    *width: auto;
    zoom:1;
}
#yspnav #ysf-fantasyflyout .bd,
#yspnav #ysf-fantasyflyout ul
{
    width: 250px;
}
#yspnav #ysf-fantasyflyout ul {
    *background-color: white;
}
#yspnav li.last #ysf-fantasyflyout li.last {
    border-bottom-style: dotted;
}
#yspnav li.last #ysf-fantasyflyout li.ysf-team {
    color: #999;
    display: block;
    padding-left: 10px;
    list-style-type:none;
    *zoom:1;
    _border-top: none;
}

#yspnav #ysf-fantasyflyout h5,
#yspnav #ysf-fantasyflyout h6 {
    display: block;
    padding-top: 3px;
    zoom:1;
}
.yui-skin-sam #yspnav.ysfnavflyout li.selected .yuimenubaritemlabel-selected {
    background: none;
}
#yspnav li.selected .ysfnavflyout a {
  color:#000;
  background: none;
  padding: 5px 15px 6px;
}
#yspnav li.last #ysf-fantasyflyout li a {
    padding: 2px 10px;
    padding-left: 10px;
    padding-right: 10px;
    background-image: none;
    background-color: white;
    margin: 0;
    zoom:1;
}

#yspnav li.last #ysf-fantasyflyout li a:hover {
    text-decoration: underline;

}
#yspnav li.last #ysf-fantasyflyout ul.other-games li a {
    font-weight: normal;
}
#yspnav li.last #ysf-fantasyflyout ul.other-games li  {
    width: 50%;
    *width: 45%;
    border-top: none;
    background-color: white;
    background-image: none;
    zoom: 1;
}
#yspnav li.last #ysf-fantasyflyout ul.other-games li.odd {
    float: left;
    *float: none;
    *display: inline;
    *zoom: 1;
    position: relative;
    overflow: visible;
    *overflow: hidden;
}

#yspnav li.last #ysf-fantasyflyout ul.other-games li.even {
    border-left: 1px solid white;
    position: relative;
    *float: none;
    *display: inline;
    *zoom: 1;
    overflow: hidden;
    *overflow:visible;
}
#yspnav li.last #ysf-fantasyflyout ul.other-games li:last-child,
#yspnav li.last #ysf-fantasyflyout ul.other-games li.odd:last-child {
    clear: both;
    float: none;
    width: 100%;
    border-left: 1px solid #999;
    border-bottom-style: solid;
    max-width: 100%;
}

/* END NEW 2008 */

.yspcontent #yspmain {
  min-height:120px;
}

.sub .mod {
  margin:5px 5px 10px 5px;
  text-align:left;
}
/* End Navigation */
div.yspmainmodule {
  margin-bottom:20px;
}
.yspindex div.yspsubmodule {
  margin:0 5px 0 3px;
}
div.yspmainmodule p.legend{
  font:77% Verdana;
  margin:0;
  padding:2px 4px;
}
h4.ysptitlebar {
  padding:3px 4px 5px 4px;
  *padding:4px 4px 5px 4px;
  font:bold 77% Verdana;
  color:#fff;
  margin:0;
}
#yspmain h4.ysptitlebar {
  border-bottom:1px solid #000;
  color:#760000;
  padding:0 0 1px 0;
  margin:25px 0 0 0;
  font:136% Arial;
}
#yspmain h4.yspsplit span {
  padding:0;
  margin:0;
  font-size:161.6%;
}
#yspmain h4.yspsplit {
  font:100% Arial;
}
#yspmain h4.yspsplit em {
  font-weight:normal;
  font-style:normal;
  float:right;
  font:77% Verdana;
  padding-top:5px;
  padding-right:2px;
}
.yspindex #yspmain div.hd h4 {
  font:136% Arial;
  padding:0 0 1px 0;
  margin:0;
}
h4.ysptitlebar a, h4.ysptitlebar a:visited {
  text-decoration:none;
}
h4.yspsplit a:hover {
  text-decoration:underline;
}
h4.yspsplit span {
  float:left;
}
h4.yspsplit em {
  font-weight:normal;
  font-style:normal;
  float:right;
}
/*\*//*/
h4.yspsplit {
  height:1.1em;
}
/**/

/* module tabs  */
.moduletabs {
  border:none;
  background:#e9e8e4;
  position:relative;
  font-weight:bold;
  font-size: 85%;
  color: white;
  padding:3px 3px 2px;
  zoom:1;
  margin-bottom: 2px;
  height: 4ex;
}

.moduletabs a  {
  color:#fff;
  text-decoration:none;
  padding-bottom:7px;
}

.moduletabs li {
    position: relative;
    height: 4ex;
}
.moduletabs a:visited {
  color:#fff;
}
.moduletabs em {
  padding:1px 20px 2px 20px;
  margin: 0;
  line-height:200%;
  cursor:hand;
  font-weight: bold;
  border-right: 1px solid white;  
}
html>body .moduletabs em {
  background:#424242 url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) center top no-repeat;
}

* html .moduletabs em {
  background:#424242 url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites-ie6.png) center top no-repeat;
}

.moduletabs ul {
    overflow: visible;
    height: 4ex;
}
.yspindex .moduletabs em {
  padding:1px 20px 2px 20px;
  margin: 0;
  line-height:200%;
  background:#424242 url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) center top no-repeat;
  cursor:hand;
  font-weight: bold;
}
.moduletabs li.selected em {
  cursor:default
}
.moduletabs a:hover  {
  text-decoration:underline;
}
.moduletabs a:hover em{
  background:#A8A8A8;
}
.moduletabs li.selected  a {
  background: #fff;
  color: black;
}

#bd .moduletabs li.selected a {
    color: black;
}
.moduletabs li.selected  a:hover {
  text-decoration:none;
  cursor:default;
}
.moduletabs li.first em {
}
.moduletabs h4 {
  display:block;
  position:absolute;
  right:0px;
  top:0px;
  width:1px;
  font:bold 100%;
  white-space:nowrap;
  overflow:hidden;
  margin:0 0 0 0;
  color:#000;
  padding:3px 0 4px 0;
}
.moduletabs h4 {
  height:auto;
  _height /**/:18px;
}
.moduletabs li.selected  a {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) no-repeat  bottom center  transparent ;
}
.moduletabs li.selected  a:hover {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) no-repeat  bottom center  transparent;
}
.moduletabs li.selected em {
  background:white;
}
.moduletabs li.selected a:hover em {
  background:white;
}


.tablelegend{
  font:77% Verdana;
  line-height:100%;
  padding-bottom:8px;
  margin:0 10px;
}
.tablelegend li {
  margin: 0 5px 0 0;
  border-right:1px solid #000;
  padding-right:5px;
}
.tablelegend li.last {
  border-right:0;
}
.tablelegend ul {
  float:left;
}
.tablelegend span {
  float:right;
}
.tablelegend a:link, .tablelegend a:visited{
  color:#00F;
}
div.detail span.status {
  color:#F00;
}
div.clr {
  clear: both;
  line-height: 0px;
}
html>body div.clr {
  height:.01em;
}
input.button {
  background:#D8D9D5 url('http://l.yimg.com/a/i/us/sp/fn/default/full/button_bg.gif') no-repeat;
  border:1px solid #a8a8a8;
  margin:0;
  cursor:hand;
  cursor:pointer; 
  font:85% Verdana;
  padding:2px 5px;
}
div.popupwrap {
  position:absolute;
  text-align:left;
  width:200px;
  zoom:1;
  opacity:.9;
  z-index:205;
}
div.popupwrap .shadow {
  background:#999;
  filter:alpha(opacity=50);
  opacity:.5;
  position:absolute;
  left:3px;
  top:3px;
  width:100%; height:100%; _height:1000px;
  z-index:99;
}
div.popupwrap div.content{
  border:1px solid #999;
  position:relative;
  padding:5px;
  background:#fff;
  z-index:200;
  font:77% Verdana;
}
div.popupwrap span {
  display:block;
  color:#666;
  margin-top:10px;
}
.pagingnav {
  padding: 0;
  margin-bottom:8px;
}
.yspspecial .pagingnav {
  margin-left:10px;
  margin-right:10px;
}
.pagingnav li {
  font:77% Verdana;
  margin:0;
}
.yspspecial .pagingnav li a {
  line-height:110%;
}
.pagingnavlist li {
  color:#000;
  padding: 0 8px;
  border-right:1px solid black;
}
.pagingnavlist li.first {
  padding-left:0px;
}
.pagingnavlist li.last {
  border-right:0px;
}
#yspadN {
  text-align:center;
  min-height:60px;
}
#yspadN table {
  text-align:center;
}
#yspadNT1 {
  text-align:center;
  padding:3px 0 3px 0;
  background:#fff;
  margin-bottom:6px;
}
#yspadNT1 img {
  display:block;
  margin:0 auto;
}
#dynamicloader {
  position:absolute;
  font:70% Verdana;
  color:#ccc;
  padding: 2px 0;
  left:10px;
  top:-12px;
}
.cta-w, .cta-p, .cta-s, .cta-h {
   background:transparent no-repeat top left;
   width:9em;
   font:bold 120%/1.2em arial;
   color:#000;
   text-decoration:none;
   text-align:center;
   margin: 0 auto 1em auto; /* defaults to center, with bottom margin */
   cursor:hand; cursor:pointer;
   display:block;
}
.cta-w:active, .cta-p:active, .cta-s:active, .cta-h:active{color:#000;}
.cta-w:visited, .cta-p:visited, .cta-s:visited, .cta-h:visited{color:#000;}
.cta-w:hover, .cta-p:hover, .cta-s:hover, .cta-h:hover {cursor:hand;cursor:pointer;    color:#000;}
.cta-w strong, .cta-p strong, .cta-s strong, .cta-h strong {
   background:transparent no-repeat top right;
   right:-4px;
   position:relative;
   display:block;
}
.cta-w strong strong, .cta-p strong strong, .cta-s strong strong, .cta-h strong strong {
   background:transparent no-repeat bottom left;
   top:5px;
   left:-4px;
   right:0;
}
.cta-w strong strong strong, .cta-p strong strong strong, .cta-s strong strong strong, .cta-h strong strong strong {
   background:transparent no-repeat bottom right;
   padding:.2em 0 .5em;
   top:auto;
   right:-4px;
   left:auto;
   color:#000;
   text-indent:-5px;
   font-weight: bold;
}
.cta-w em, .cta-p em, .cta-s em {display:block;font:70% arial;}
.cta-w {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/yel_tl.gif);}
.cta-w strong {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/yel_tr.gif);}
.cta-w strong strong {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/yel_bl.gif);}
.cta-w strong strong strong {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/yel_br.gif);}
.cta-p {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/bb_tl.gif);}
.cta-p strong {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/bb_tr.gif);}
.cta-p strong strong {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/bb_bl.gif);}
.cta-p strong strong strong {background-image: url(http://l.yimg.com/a/i/us/sp/fn/default/bb_br.gif);}
.cta-s {background-image:url(http://us.i1.yimg.com/us.yimg.com/i/us/nt/bn/cta/gry_tl.gif);}
.cta-s strong{background-image:url(http://us.i1.yimg.com/us.yimg.com/i/us/nt/bn/cta/gry_tr.gif);}
.cta-s strong strong{background-image:url(http://us.i1.yimg.com/us.yimg.com/i/us/nt/bn/cta/gry_bl.gif);}
.cta-s strong strong strong{background-image:url(http://us.i1.yimg.com/us.yimg.com/i/us/nt/bn/cta/gry_br.gif);}

/* CTA Migration */
/* We should migrate to using the following CTA method.  It allows us to use sprited CTAs and a more flexible skinning method */
a.ysf-cta {
    background:transparent no-repeat top left;
    width:9em;
    font:bold 122% arial;
    color:#000;
    text-decoration:none;
    text-align:center;
    margin:0;
    cursor:hand;
    cursor:pointer;
    display:block;
}
.ysf-cta:hover {
    cursor:hand;
    cursor:pointer;
}
.ysf-cta strong { font-weight: bold; background:transparent no-repeat top right;right:-4px;position:relative;display:block;}
.ysf-cta strong strong {background:transparent no-repeat bottom left;top:5px;left:-4px;right:0;}
.ysf-cta strong strong strong {background:transparent no-repeat bottom right;padding:.2em 0 .5em;top:auto;right:-4px;left:auto;text-indent:-5px;}
.ysf-cta em {display:block;font:70% arial;}

a.ysf-cta-main,
a.ysf-cta-main strong,
a.ysf-cta-main strong strong,
a.ysf-cta-main strong strong strong
{
    background-image:url('http://l.yimg.com/a/i/us/sp/fn/default/cta_yellow.png');
}
a.ysf-cta-reg,
a.ysf-cta-reg strong,
a.ysf-cta-reg strong strong,
a.ysf-cta-reg strong strong strong
{
    background-image:url('http://l.yimg.com/a/i/us/sp/fn/default/cta_blue.png');
}
a.ysf-cta-reset,
a.ysf-cta-reset strong,
a.ysf-cta-reset strong strong,
a.ysf-cta-reset strong strong strong
{
    background-image:url('http://l.yimg.com/a/i/us/sp/fn/default/cta_grey.png');
}
a.ysf-cta-main:hover,
a.ysf-cta-main:hover strong,
a.ysf-cta-main:hover strong strong,
a.ysf-cta-main:hover strong strong strong,
a.ysf-cta-reg:hover,
a.ysf-cta-reg:hover strong,
a.ysf-cta-reg:hover strong strong,
a.ysf-cta-reg:hover strong strong strong,
a.ysf-cta-reset:hover,
a.ysf-cta-reset:hover strong,
a.ysf-cta-reset:hover strong strong,
a.ysf-cta-reset:hover strong strong strong
{
    background-image:url('http://l.yimg.com/a/i/us/sp/fn/default/cta_green.png');
}

#yspadSKY {
  text-align:center;
  margin-bottom:20px;
}
#yspadSKY table {
  margin:0 auto;
}
.ysppopup #yspcontent {
  width:100%;
}
.ysppopup  #yspbody {
  width:100%;
}
.ysppopup #yspmain {
  padding:10px;
  margin:20px 20px 0 20px ;

  text-align:left;
}
.ysppopup #yspsub, .ysppopup #yspfooter  {
  display:none;
}

div.navlist:after, #yspcontent:after, .clear:after, h4.yspsplit:after, #noteContainer .hd:after, #noteContainer .ft:after, div.moduletabs ul:after{
  content: ".";
  display: block;
  height: 0;
  clear: both;
  visibility: hidden;
}
span.user-id {
  padding-left:21px;
  position:relative;
  zoom:1;
}
span.user-id img {
  position:absolute;
  top:-1px;
  left:0px;
}

#yspsubnav .subsection {
��margin-left: 7px;
��padding-left: 20px;
��border-left: 1px solid #999;
��font-weight: bold;
}

/* Player Status/Note Icons */

/* END Player Status/Note Icons */

div.navlist, .clear, #yspcontent, h4.yspsplit, #noteContainer .hd, #noteContainer .ft {display: inline-table;}
/* Hides from IE-mac \*/
div.navlist, .clear, #yspcontent, h4.yspsplit, #noteContainer .hd, #noteContainer .ft {display: block; zoom:1;}
/* End hide from IE-mac */


/* debug */
#bd .yui-b {
}

#ysp-network-nav {
/*    max-height: 50px;
    overflow: hidden; 
    height: 50px !important;
*/    
}
.yui-menu-shadow { border: 2px dotted red; 
position: absolute;
top: 0;
visibility: hidden;
display: none;
}/* LOC: game2006 */
#yspcontent {
  position:relative;
  z-index:0;
}
.yspcontent .plus #yspcontent {
  width:974px;
  margin-left:auto;
  margin-right:auto;
}
.yspcontent .plus #yspmain, .generic .plus #yspmain {
  width:954px;
  margin:0;
  padding:0 10px 1px 10px;
  text-align:left;
}
.yspindex .plus #yspcontent {
  margin-bottom:0;
}
body {
  background: #336633 url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/fade-bg.jpg) left top repeat-x;
}
#doc4 {
/*  background: transparent  url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-bg.jpg) center top no-repeat; */
}
#field {
  width:100%;
  height:355px;
  position:absolute;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-bg-3.jpg) center top no-repeat;
  top:0;
  left:0px;
  z-index:0;
  min-width:974px;
}

#yspheader {
  z-index:1;
  position:relative;
  padding-top:10px;
}
#yspmh {
  z-index:1;
}
#yspmh a.home{
  display:block;
  zoom:1;
  padding:10px;
  width:519px;
  margin:0 auto;
  overflow:hidden;
}
#yspmh h2 {
  width:519px;
  margin:0 auto;
  height:68px;
  text-indent: -2000px;
  cursor:pointer;
  cursor:hand;
  zoom:1;
  z-index:5000;
}

html > body #yspmh h2 {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) no-repeat left -300px;
}

* html #yspmh h2 {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites-ie6.png) no-repeat left -300px;
}
.plus #yspmh h2 {
    background-position: left -200px;
}
#yspmh a:hover {
  text-decoration:none;
}
#yspcontent {
  background:url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/nfl/indexbg.gif) repeat-y top right;
}
.yspsubcontent #yspcontent {
  background:url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/nfl/indexbg.gif) repeat-y 664px 0px #fff;
}
.yspindex #yspcontent {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/2007_right_bg.gif) repeat-y 664px 0px #fff;
}
.yspindex #yspfooter {
  padding-top:13px;
  background-color: white;
}
#yspnav, #yspnavempty div  {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png) repeat-x 0 0px;
}
#yspnav.nav-league {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png) repeat-x 0 -200px;
}
#yspnav li em {
  padding-bottom:3px;
  float:left;
}
#mh-shadow {
    display:none;
}
#yspnav a, #yspnav strong {
  position:relative;
  text-transform: uppercacse;
}


#yspnav li {
  background:transparent url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png) no-repeat 100% -100px ;
    line-height: 1.75em;
    margin: 0;
    padding: 4px 0 7px;
}

#yspnav li:first-child,
#yspnav li.first {
    background-position: 100% -100px;
}
#yspnav.nav-league li {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-nav-sprites.png) no-repeat 100% -300px transparent;
}

#yspnav.nav-league li:first-child {
    background-position: 100% -300px;
}
#yspnav a:hover {
  text-decoration:underline; 
}
#yspnav li.last a,
* html #yspnav li.last a,
* html #yspnav li.last a:hover {
  padding-right:15px;
  margin-right: 10px;
  background:transparent url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) no-repeat right -442px;
  _background-position: right -446px;
  _zoom:1;
}

#yspnav li.last,
* html #yspnav li.last,
* html #yspnav li.yuimenuitem-selected {
    _zoom:1;

}
* html #yspnav li a {
    zoom:1;
    margin-top: 0;
    margin-bottom: 0;
    line-height: 1;
    padding-top: 4px;
    padding-bottom: 4px;

}

* html #yspnav ul li a,
* html #yspnav ul li.selected a,
* html #yspnav ul li.last a {
    zoom:1;
    margin-top: 0;
    margin-bottom: 0;
    line-height: 1;
    padding-top: 4px;
    padding-bottom: 4px;
}

* html #yspnav ul li,
* html #yspnav ul li.selected {
    padding-top: 4px;
    padding-bottom: 5px;
}

* html #yspnav ul li.first a {
    background-image: none;
}

* html #yspnav li .yuimenu li,
* html #yspnav.nav-league li .yuimenu li {
    background-image: none;
}

/* boy, the things we do for IE */
* html #yspnav ul li .yuimenu li a,
* html #yspnav ul li.selected li .yuimenu a,
* html #yspnav ul li .yuimenu li a.first,
* html #yspnav ul li.selected li .yuimenu a.first, 
* html #yspnav ul li .yuimenu li a.last,
* html #yspnav ul li.selected li .yuimenu a.last {
    padding-bottom: 0;
    margin-bottom:0;
    line-height: 1.4;
    padding:7px 15px 6px;
    line-height:150%;
    background-image: none;
}

#yspnav li.stattracker a {
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) no-repeat right -394px;
    _background-position: right -398px;
    padding-right: 15px;
    margin-right: 10px;
    

}
#teammh {
  position:relative;
}
#yspadMH {
  position:absolute;
  right:0px;
  top:-10px;
  z-index:1000;
}

#yspadMH div {
  position:absolute;
  right:0px;
  top:0px;
  z-index: 100;
}
#yspadMH a {
  display: block;
  zoom: 1;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 1000;
}
#yspadMH div a { 
  position: relative;
}

#yspadM2 {
  position:absolute;
  right:0px;
  top:0px;
  z-index:1000;
}
#yspadHEAD {
  padding-top:5px;
}
#yspadHB img {
  display:block;
}
.mast {
  padding-top:3px;
  *background-image:none;
}
.mast a, .mast a:visited {
  color:#EAAF1E;;
}
.srch {
  float:right;
  margin:5px;
}
.srch form {
  margin:0;
  padding:0;
}
.srch img, .srch label, .srch input {
  float:left;
  margin-right:3px;
}
.lnks {
  float:left;
}
.mast ul {
  margin:0;
  padding:0;
}
.mast strong {
  display:block;
}
.lnks ul li{
  list-style-type:none;
  font:77% Verdana;
  float:left;
  display:inline;
  margin-left:10px;
}
.mast .info {
  clear:both;
  width:600px;
  float:left;
  display:inline;
  font:85% arial;
  color:#FFFFFF;
  margin:12px 5px 5px 10px;
}
.mast h3 a {
  margin:0;
  padding:0 10px 12px 0;
  float:left;
  text-indent:-9000px;
  font:0px;
  width:219px;
  height:27px;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/sports_logo.png) no-repeat;
  _background-image:none;
  cursor:hand;
  _filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='http://l.yimg.com/a/i/us/sp/fn/default/sports_logo.png', sizingMethod='crop');
  zoom:1;
  display: block;

}
.mast h3 a:hover {
  text-decoration:none;
}
.mast h3 {
  margin:0;
  padding:0;
  zoom: 1;
  float: left;
  overflow: hidden;
}
.mast p {
  margin:0;
  padding:0;
}
.mast p strong {
  display:inline;
}
.help {
  float:right;
  margin:10px 10px 10px 0;
  clear:right;
  width:200px;
  display:none;
}
.help ul li {
  float:right;
  display:inline;
  list-style-type:none;
  font:77% Verdana;
  text-align:right;
  margin-left:10px;
}
#yspleaderboard_messages.yspsubcontent #yspnavempty, #yspleaderboard_message.yspsubcontent #yspnavempty {
  padding-bottom:15px;
  background:#fff;
}
#yspteammh {
  color:#FFFFFF;
  position:relative;
  padding-bottom:5px;
  zoom:1;
}
#yspteammh a, #yspteammh a:visited {
  color: white; 
}
#keystosuccess {
  border:0;
}
#yspleaguehome #leaguehome-image h1 {
  display:none;
}
#yspleaderboard_home .free h1 {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/free/viagra_leaderboard.jpg) no-repeat;
  color:#fff;
  min-height:46px;
  _height:46px;
  font-size:136%;
  line-height:42px;
  padding-left:10px;
}
#yspleaderboard_home .plus #viagraad {
  display:none;
}
#yspleaderboard_home .free #viagraad {
  display:none;
  position:absolute;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/1x1clear.gif);
  width:200px;
  height:42px;
  top:10px;
  right:190px;
}

#ysphome #hd #yspnav li.last {
   display:block; 
}

#ysphome div#fantasy-message-boards {
  float:left;
  clear: left;
}

#bd { 
    background-color: white;
}
#yui-main {
    zoom:1;
}

#yspmain {
    padding: 0 10px; 
}


#yspsub {
    margin-right: 10px;
    background-color: #353535;
}

.yspindex #ft {
    background-color: white;
}

#yspsub .mod {
    padding: 5px 5px 10px;
}

/* Chex Draft Central Overview Page Title sponsorship */
#page_title_sponsor {
    background: url(http://l.yimg.com/a/i/us/sp/fn/draft_app/chex_250x56.jpg) right top no-repeat;
    border-bottom: 1px solid #0860A8;
    color: #0860A8;
    height: 26px;
    padding: 30px 0 0 0;
    position: relative;
    display: block;
}
#page_title_sponsor a {
    position: absolute;
    right: 0;
    top: 0;   
    height: 55px;
    width: 220px;
    text-indent: -2000px;
} 
#page_title_sponsor a:hover {
    text-decoration: none;
} 

h3.table-title {
    background-color:#790000;
    color:white;
    display:block;
    font-size:100%;
    font-weight:bold;
    margin-bottom:0;
    padding:3px 5px;
}



ul.formitems #marketing-item {
    margin-top: 10px;
    border-top: 1px solid #D6D6D6;
    border-bottom: 1px solid #D6D6D6;    
}

#reg-createteam ul.formitems #marketing-item {
    border: none;
}

ul.formitems #marketing-item span {
    font-size: 93%;
}

ul.formitems #marketing-item label {
    width: auto;
}

#reg-createteam ul.formitems #marketing-item li {
    padding-bottom: 0;
    height: 6ex;
    
}

.reg-module ul.formitems #marketing-item img {
    margin-bottom: 20px;
}

#ysphome .yui-u #home-promo {
    border: 5px solid #ddd;
    padding: 10px;
    margin-top: 15px;
    margin-right: 0;
    width: 285px;
    zoom:1;
}



#home-promo img {
    float: right;
    margin: -25px -10px 0 0;
    position: relative;
    z-index: 100;
}

#home-promo h3 {
    color: #7b0001;
    font-size: 93%;
    text-transform: uppercase;
    font-weight: bold;
    width: 14em;
    *width: 12em;
}

#home-promo p {
    font-size: 85%;
    margin: 2ex 0;
    width: 14em;
    *width: 13em;
    border: none;
}


#home-promo .ysf-cta {
    font-size: 85%;
    margin-top: 5px;
    color:  black;
}
/* color treatment */

h1 {

}

#leaguehome-title h1 {
  float:left;
}
#leaguehome-image {
  margin-bottom:10px;
}
#leaguehome-image h1 {
  margin-top:15px;
}
#leaguehome-image img {
  display:block;
}
#persistent-league-nav {
  float:right;
  margin:3px 0;
}
#persistent-league-nav form {
  margin:0;
  padding:0;
}
#persistent-league-nav label, #persistent-league-nav select {
  font:92% Arial;
}
.specialmessage {
  padding-top:10px;
  padding-bottom:15px;
  background:#fff;
}
.specialmessage p {
  margin:0;
  padding:0;
}
.specialmessage .content {
  border:1px solid #000;
  margin:0;
  padding:5px;
  font:92% Arial;
  text-align:left;
  background:url( http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/special_gradient.gif ) repeat-x #fff;
}
.specialmessage ul {
  margin:0;
  padding:0;
}
.specialmessage ul li {
  margin:5px 0 5px 25px;
}
#yspmatchup .specialmessage {
  padding-left:10px;
  padding-right:10px;
}
#yspteam .specialmessage {
  padding-left:10px;
  padding-right:10px;
}
.featuredmessage .content {
  background:url( http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/featured_gradient.gif ) repeat-x #fff;
}
.featuredmessage {
  padding-top:0;
}
.gamead .content {
  padding:0;
  border:0;
}
.gamead .content img {
  display:block;
}

#yspcontent .strongfeature .content {
  background-image: url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/nfl/free/alert-background-red.gif);
  color: white;
  background-color: #342c2e;
  font-size: 86%;
}

#yspcontent .strongfeature .content li {
    list-style-type: none;
    margin-left: 0;
    padding-left: 0;
    margin: 0;
    padding: 3px 0;

    zoom: 1;
}

#yspcontent .strongfeature .content li p {
    min-height: 27px;
    font-size: 94%;
    padding: 5px auto;
    margin: 0;
}
#yspcontent .strongfeature .content li a {
    color: #EAB01E;
}

#yspcontent .strongfeature .content li h5 {
    float: left;
    line-height: 3;
    margin: 0;
    text-transform: uppercase;
    font-size: 94%;
    margin-right: 10px;
}

#rev-promo {
    border: 1px solid #730000;
    background-color: white;
    padding: 2px;
}

#rev-promo .content {
    background-color: #730000;
    padding: 5px;
    color: white;
    overflow: hidden;
}

#rev-promo .content h3 {
    font-size: 108%;
    font-weight: bold;
    width: 150px;
    float: left;
    overflow: hidden;
    display: block;
    margin-top: 1ex;
    padding-right: 5px;
}

#rev-promo .content p {
    font-weight:bold;
    line-height: 1.5;
    overflow: hidden;
    float: left;
    display: block;
    width: 260px;
    font-size: 85%;
    text-align: left;
}

#rev-promo .content p a {
    color: #e8ad15;
}


#rev-promo .content img {
    float: right;
}

#promocontent #rev-promo {
    clear: both;
    width: 521px;
    float: right;
    margin-right: 3px;
    padding:
    margin-bottom: 20px;
}

#promocontent #rev-promo .content {
    padding: 10px;
}
#promocontent #rev-promo p {
    float: right;
    margin: 0;
    text-align: right;
    font-weight: normal;
    padding-bottom: 0;
    border: none;
}
#rev-promo .content h3 {
    margin-top: 0;
    padding-top: 0;
    
}

#promocontent #rev-promo em {
    font-style: italic;
}
#promocontent #rev-promo strong {
    font-weight: bold;
}

#promocontent #rev-promo p.promo-description,
#promocontent #rev-promo h3 {
    float: none;
    width: 100%;
    text-align: left;
}

#promocontent #rev-promo p.promo-description {
border: none;
width: 95%;    
}
#promocontent #rev-promo h3 {
    font-size: 135%;
}
#leaguenotes {
  zoom:1;
  border-top:1px solid #D6D6D6;
}
#leaguenotes h4 {
  color:#000;
  font:bold 85% Verdana;
  margin:15px 0 5px 0;
}
#leaguenotes ul {
  margin:0;
  padding:0 0 0 20px;
  list-style-type:square;
  color:#000;
}
#leaguenotes ul li {
  margin: 0 0 5px 0;
  font:92% Arial;
  list-style-type: disc;
}
#leaguenotes ul li.last {
  margin: 0 0 15px 0;
}
#leaguenotes ul li span {
  color:#000;
}/* leaguehome_standings */
#leaguestandingstabs li.orphan  {
  float:right;
  border:0;
  background:transparent;
}
#leaguestandingstabs li.orphan em {
  border:0;
  background:#A8A8A8;
}
#leaguestandingstabs li.selected  a {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) no-repeat  bottom center;
}
#leaguestandingstabs li.selected  a:hover {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/football/nfl-2008-sprites.png) no-repeat  bottom center;
}

#leaguehomestandings .yspmainmodule{
}
#leaguehomestandings div.inactive {
  display:none;
}
#standingstab {
  zoom:1;
}
#standingslegend {
  padding:0;
  margin:0;
  margin-top: 2px;
  list-style-type:none;
  border: none;
}
#standingslegend li {
  font:77% Verdana;
  float:right;
  vertical-align:middle;
  margin:2px 4px;
  line-height:2em;
}
#standingslegend li img {
  display:inline;
  position:relative;
  top:4px;
  left:2px;
}
#standingslegend li.invite {
  float:left;
  margin-left:4px;
  font-weight:bold;
}
#standingslegend li a {
 line-height:2.5em;
}
#standingstable td.rank {
  padding-right:16px;
  *padding-right:19px;
}
#standingstable td.im {
  padding-left:0px;
}
#standingstable td.im img {
  float:left;
}
#standingstable td.blast {
  position:relative;
  padding-right:0px;
}
#standingstable td.rank a.blast {
  float:right;
  display:block;
  width:16px;
  text-indent:-8000px;
  height:16px;
  overflow:hidden;
  margin-left:0px;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/new_blast.gif)  1px 1px no-repeat;
}

#standingstable th {
    background-color: #e9e8e4;

}

#standingstable {
    border: 1px solid #E3E3E1;
}
#scheduletypenav{
  font:77% Verdana;
  padding:5px 0 5px 25px;
}
#scheduletypenav a {
  padding-right:25px;
}
#scheduletypenav li.selected a, #scheduletypenav li.selected a:visited ,#scheduletypenav li.selected a:active ,#scheduletypenav li.selected a:hover {
  font-weight:bold;
  color:#000;
}
#scheduletypenav a, #scheduletypenav a:visited, #scheduletypenav a:active, #scheduletypenav a:hover {
  color:blue;
}
#scheduletable {
  border-top:1px solid #ABAB9E;
  background:#F1F2ED;
}
#scheduletable th {
  padding-top:10px;
}
#scheduletable th.first {
  padding-left:25px;
}
#scheduletable td.first {
  background:url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/1x1_E7E7E5.gif) repeat-y  right top #fff;
  padding:0;
  text-align:left;
}
#scheduletable td.first li.selected a, #scheduletable td.first li.selected a:visited, #scheduletable td.first li.selected a:active, #scheduletable td.first li.selected a:hover{
  color:#000;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/plus/li_square.gif) no-repeat left center ;
  border-top:1px solid #E7E7E5;
  border-bottom:1px solid #E7E7E5;
  border-right:1px solid #F1F2ED;
}
#scheduletable td.first li {
  padding-left:10px;
  zoom:1;
}
#scheduletable td.first li.selected {
  background:#F1F2ED;
}
#scheduletable td.first ul {
  list-style-type:none;
  padding:0;
  margin:0;
}
#scheduletable td.first li a, #scheduletable td.first li a:visited, #scheduletable td.first li a:active {
  color:#999;
  border-right:1px solid #E7E7E5;
}
#scheduletable td.first li a {
  display:block;
  border-bottom:1px solid #FFF;
  border-top:1px solid #FFF;
  padding:2px 0px 2px 15px;
  background:url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/li_square_off.gif) no-repeat left center ;
}
#scheduletable td.first li a:hover {
   background:url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/li_square_on.gif) no-repeat left center ;
}
#scheduletable td.result, #scheduletable th.result {
  text-align:left;
}
#scheduletable td.week, #scheduletable th.week {
  width:1%;
}
#scheduletable td.scorethin td {
  text-align:left;
  font:100% Arial;
}
#scheduletable td.scorethin table {
  margin-bottom:10px;
}
#scheduletable td.scorethin h4 {
  margin:5px 0 3px 4px;
  font:bold 85% Verdana;
}
#scheduletable td.scorethin h4 span {
  float:left;
}
#scheduletable td.scorethin h4 a {
  float:right;
  font-weight:normal;
}
#scheduletable td.scorethin td.team {
  width:170px;
}
#scheduletable td.scorethin td.score {
  width:50px;
  text-align:right;
}
#scheduletable td.scorethin td.matchup {
  text-align:center;
  vertical-align:middle;
}
#scheduletable td.last, #scheduletable th.last {
  text-align:left;
  white-space:nowrap;
}
#scheduletable td.points, #scheduletable th.points{
  width:200px;
  padding-right:50px;
  text-align:right;
}
#playofftypenav{
  font:77% Verdana;
  padding:5px 0 5px 20px;
}
#playofftypenav a {
  padding-right:25px;
}
#playofftypenav li.selected a, #playofftypenav li.selected a:visited ,#playofftypenav li.selected a:active ,#playofftypenav li.selected a:hover {
  font-weight:bold;
  color:#000;
}
#playofftypenav a, #playofftypenav a:visited, #playofftypenav a:active, #playofftypenav a:hover {
  color:blue;
}
#playofftable th {
  text-align:left;
  border-right:1px solid #ABAB9E;
  border-top:1px solid #ABAB9E;
}
#playofftable.eight th, #playofftable.eight td{
  width:25%;
}
#playofftable td {
  text-align:left;
  font:77% Verdana;
}
#playofftable div {
  border:1px solid #E7E7E5;
  background: #F1F2ED;
  margin:3px 0;
  padding:2px
}
#playofftable div span,#playofftable div strong, #playofftable div a {
  display:block;
  line-height:150%;
}
#playofftable div a.matchup {
  text-align:center;
}
#playofftable div strong {
  font:bold 90% Verdana;
  line-height:167%;
}
#playofftable div span a {
  display:inline;
}
#playofftable div span {
  white-space:nowrap;
  overflow:hidden;
  display:block;
  line-height:150%;
}
#playofftable.eight div span{
  width:150px;
}
#playofftable.eight td.quarter div, #playofftable td.semi div {
  margin:5px 0;
  padding:2px;
}
#playofftable.eight td.semi div, #playofftable td.final div {
  padding:15px 2px;
}
#playofftable.eight td.final div {
  padding:40px 2px;
}
#playofftable td div#g5 {
  margin-bottom:40px;
}
#playofftable td div#g6 {
  margin-top:40px;
}
#playofftable td div#g3 {
  margin-top:13px;
}
#playofftable td {
  padding:3px 1px;
}
#finalresults {
  border-top:1px solid #ABAB9E;
  text-align:center;
}
#finalresults h5{
   margin:0;
   font:152% Arial;
}
#finalresults ul {
  margin:0 auto;
  padding:0;
  width:360px;
}
 #finalresults li a {
  font:bold 85% Verdana;
  padding:5px 0;
  display:block;

}
#finalresults li {
  width:120px;
  margin:20px 0;
  float:left;
  padding:0;
  list-style-type:none;
}
#finalresults span {
  display:block;
}
#finalresults img.logo {
  margin: 3px auto;
}
#lhstplayofftab {
  position:relative;
}
#pps {
  color:#000;
  font:77% Verdana;
  margin:0;
  padding:0 0 7px 4px;
}
#ysparchive_leaguehome #playofftable.eight div span {
   width:115px;
}
#alltime {
  text-align:center;
}
#alltime h5{
  margin:0;
  font:152% Arial;
}
#alltime ul {
  margin:0;
  padding:0;
  list-style-type:none;
}
#alltime ul ul {
  float:left;
  padding:0;
  width:400px;
  zoom:1;
}
#alltime ul ul.no-winner {
  width: 530px;
}
#alltime ul ul.no-winner li.last h5 {
  font:122% Arial;
  margin-top:5px;
}
#alltime ul ul li a {
  font:bold 85% Verdana;
  padding:5px 0;
  display:block;
}
#alltime ul ul li {
  width:120px;
  padding:20px 0;
  float:left;
  margin-left:10px;
  overflow:hidden;
}
#alltime span {
  display:block;
}
#alltime li span.empty {
  height:10px;
}
#alltime img.logo {
  margin:3px auto;
}
#alltime a.season {
  float:left;
  margin:57px 0 0 30px;
  font:bold 122% Arial;
}
#alltime a.no-logos  {
  margin:30px 0 0 30px;
}
#alltime li {
  border-bottom:1px solid #ccc;
}
#alltime li li, #alltime li.last {
  border-width:0;
}
#alltime p {
  margin:0 0 10px 0;
  text-align:left;
  padding:5px 10px 0 10px;
  font:92% Arial;
}
.simpletable {
  width:100%;
}
.simpletable td {
  font:77% Verdana;
  padding: 2px 2px;
  text-align:left;
  line-height:100%;
}
.simpletable tr.first td {
  padding-top:4px;
}
.simpletable th {
  font:77% Verdana;
  text-align:left;
  background:#EDEDED;
  padding:4px 2px;
}
.simpletable th.first, .simpletable td.first {
  padding-left:4px;
}
.simpletable th.last, .simpletable td.last {
  padding-right:4px;
}
.simpletable th.actiongroup {
  text-align:center;
}
.simpletable span.status {
  color:#F00;
}
.simpletable td.action {
  width:16px;
  text-align:center;
}
.simpletable td.watch {
  width:16px;
  text-align:center;
}
.simpletable td.buzzgraph {
  font-weight:bold;
  width:100px;
}
.simpletable td.buzzgraph img {
  float:left;
  margin-right:2px;
  background:url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/buzz_graph.jpg) no-repeat  0px 2px;
}
p.simpletablemessage {
  margin:0;
  padding: 4px 4px 10px 4px;
}
.simpletable .pdf a {
  display:block;
  padding:5px 0 5px 24px;
  background: url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/pdf.gif) 3px 1px no-repeat;
}
.simpletable .pdf span {
  display:block;
  padding:5px 0 5px 24px;
  background: url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/fn/default/full/pdf.gif) 3px 1px no-repeat;
  color:#999;
}
.simpletable .pdf span.comingsoon {
  padding:2px 0;
  background-image:none;
}
.simpletable td.pdf, .simpletable td.date , .simpletable td.size, .simpletable td.prov {
  width:110px;
  padding-top:1px;
  padding-bottom:1px;
}
.simpletable td.size,  .simpletable th.size {
  text-align:right;
}.gametable {
  width:100%;
}
.gametable th {
  padding:5px 4px 3px 4px;
  font:77% Verdana;
  border-bottom:1px solid #ABAB9E;
  color:#000;
  text-align:right;
  background:#D5D6C6;
  vertical-align:bottom;
}
.gametable td {
  padding:3px 4px;
  font:92% Arial;
  color:#000;
  text-align:right;
  line-height:130%;
}
.gametable td.first, .gametable th.first {
  width:auto;
}
.gametable th.first {
  text-align:left;
}
.gametable td.team, .gametable th.team {
  text-align:left;
  width:150px;
}
.gametable td.wlt {
  font-weight:bold;
}
.gametable tr.selected td{
  border-top:1px solid #E7E7E5;
  border-bottom:1px solid #E7E7E5;
  background:#F1F2ED;
}
.gametable  td.legend {
  text-align:left;
  font:77% Verdana;
}
.gametable tr.division td {
  font:bold 85% Verdana;
  color:#5C0A0C;
  padding:4px 4px;
  background:#fff;
  text-align:left;
}
.gametable td.negative {
  color: red;
}
.gametable td.positive {
  color: green;
}
#recenttransactions {
  *overflow:hidden;
  *width:100%;
}
#recenttransactions div.navlist h5 {
  float:left;
  margin:0 5px 0 4px;
  font:100% Verdana;
  display:inline;
}
#recenttransactions div.navlist a:visited {
  color:#00f;
}
#recenttransactions div.navlist {
  font:77% Verdana;
  line-height:100%;
  margin-top:10px;
}
#recenttransactions div.navlist li {
  margin: 0 5px 0 0;
  border-right:1px solid #000;
  padding-right:5px;
}
#recenttransactions div.navlist li.last {
  border-right:0;
}
#recenttransactions td.first, #recenttransactions th.first, #recenttransactions td.last, #recenttransactions th.last {
  width:1px;
  padding:0;
}
#recenttransactions td.message {
  padding-left:3px;
}
#recenttransactions td div.spacer , #recenttransactions td spacer {
  width:1px; overflow:hidden;
}
#recenttransactions td.player  {
  white-space:nowrap ;
}#recentmessages td.avatar {
  width:16px;
  padding:1px 4px;
}
#recentmessages tr.first td {
  padding-top:6px;
}
#recentmessages td.date {
  width:40px;
}#commishnote {
  border:1px solid #FFE53E;
  background: #FFFBBE;
  padding:3px 3px 10px 3px;
  margin-bottom:20px;
}
#commishnote h4 {
  font:bold 77% Verdana;
  margin:0;
}
#commishnote em{
  font:77% Verdana;
  font-style:normal;
  float:left;
  width:40px;
  padding-top:3px;
}
#commishnote p{
  font:77% Verdana;
  float:left;
  margin:0 0 5px 0;
  padding-top:3px;
  width:445px;
  margin-left:3px;
}
#commishnoteedit{
  margin-top:5px;
  padding-left:43px;
  font:77% Verdana;
}
#commishnote.default p {
  margin-left:0px;
}
#commishnote.default #commishnoteedit {
  padding-left:0px;
}
.yspindex #yspadLREC, .yspsubcontent #yspadLREC  {
  text-align:center;
  padding:0 0 15px 0;
  background:#fff;
  zoom:1;
}
.yspindex #yspadLREC a, .yspsubcontent #yspadLREC a {
  text-align:right;
  margin:0 auto;
}
.yspindex #yspadLREC img, .yspsubcontent #yspadLREC img {
  margin:0 auto;
  display:block;
}
#yspadLREC table{
  margin:0 auto;
}
#yspteam #yspadLREC {
  float:right;
  text-align:center;
  display:block;
  width:300px;
  display:inline;
  margin:0 10px 10px 0;
}#scoreboardtabs  {
  position:relative;
  z-index:2;
  zoom:1;  
}
#scoreboardtabs ul {
  margin:0;
  padding:0;
}
#scoreboardtabs li {
  float:left;
  list-style-type:none;
  font:bold 77% Verdana;
}
#scoreboardtabs li.selected {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/2007_sub_nav_bg.gif) #fff no-repeat center bottom;
}
#scoreboardtabs  li a {
  color:#EAB01E;
  padding:5px 15px 13px 15px;
  float:left;
}
#scoreboardtabs  li.selected a {
  color:#000;
}
#scoreboard {
  margin:0;
  xwidth:288px;
  _width /**/:291px;
  *overflow:hidden;
  padding:5px 5px 0 5px;
  margin-bottom:-3px;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/sprint_gradient.gif) repeat-y;
}
#scoreboardweeknav {
  position:relative;
  margin-bottom:4px;
  width:100%;
  zoom:1;
  text-align:center;
}
#scoreboardweeknav ul {
  list-style-type:none;
  margin:0;
  padding:0;
  font:bold 85% Verdana;
  line-height:100%;
  color:#fff;
}
#scoreboardweeknav .last li.current{
  text-align:right;
}
#scoreboardweeknav .first li.current{
  text-align:left;
}
#scoreboardweeknav a{
  color:#EAB01E;
  font-weight:normal;
}
#scoreboardweeknav li.next   {
   position:absolute;
   right:0;
   top:0;
}
#scoreboardweeknav li.previous   {
   position:absolute;
   left:0px;
   top:0px;
}
/* scoreboard stuff */
.scoreboard a:visited {
  color:blue;
}
.scoreboard {
  top:-7px;
  padding-top:10px; 
  background:#fff;
  position:relative;
  zoom:1;
  text-align:left;
  padding-bottom:10px; 
}
.scoreboard h4 {
  display:none;
}
.scoreboard ul {
  list-style-type:none;
  padding:0;
  margin:0;
}
.scoreboard p {
  font:77% Verdana;
  padding:1px 3px;
  margin:0;
  float:left;
}
.scoreboard p a.player {
  line-height:100%;
}
.scoreboard p a.app {
  font-size:1px;
  margin:3px 0 0 1px;
  padding: 8px 0 0 0;
  overflow: hidden;
  height: 0px !important;
  _height /**/:8px;
  width:183px;
  text-indent:-200px;
  float:left;
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/watch_live_stat.gif) no-repeat transparent top left;
}
.scoreboard p a.app:hover {
  background-color:blue;
}
.scoreboard li {
  border-bottom:1px dotted #8C8C8C;
  background:#fff;
  margin:0;
  padding:0;
  float:left;
  _width /**/:288px;
  zoom:1;
}
.scoreboard  table {
  width:100%;
}
.scoreboard  table td {
  padding:1px 3px;
}
.scoreboard  table td.last {
  text-align:right;
  width:80px;
  padding-right:10px;
}
.scoreboard  table td.first {
  width:150px;
  padding-left:10px;
}
.scoreboard ul.points table td.first {
  width:auto;
}
.scoreboard  table td.first div {
  white-space:nowrap;
  text-overflow:ellipsis;
  overflow:hidden;
  width:150px;
}
.scoreboard  table td.pts {
  text-align:right;
}
.scoreboard  table td.rem {
  text-align:center;
}
.scoreboard div.dynamic {
  display:none;
}
#realtab table td.last {
  text-align:right;
}
#realtab table td.first div {
  padding-left:10px;
  width:140px;
  _width /**/:150px;
}
#realtab p a.app {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/watch_live_game.gif) no-repeat transparent top left;
  width:181px;
}
#realtab p a.app:hover {
  background-color:blue;
}
#realtab td.pos div {
  background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/possession.gif) no-repeat transparent center left ;
}
#realtab td.win div {
  font-weight:bold;
}
#realtab  table td.last {
  text-align:right;
}
.scoreboardrefresh {
  background:#D5D6C6;
  font:77% Verdana;
  color:#5C0A0C;
  padding:3px;
  margin:0;
  border:1px solid #4D0002;
}
.scoreboardrefresh strong {
  float:left;
  padding:2px 0px;
}
.scoreboardrefresh a {
  float:right;
  border:1px solid #A8A8A8;
  border-color: #FFF #A8A8A8 #A8A8A8 #FFF;
  padding:1px 3px 1px 14px;
  background:url( http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/plus/icon_refresh.gif ) no-repeat 2px 2px #F1F2ED;
}
.scoreboardrefresh a:link, .scoreboardrefresh a:visited, .scoreboardrefresh a:hover, .scoreboardrefresh a:active {
  font-weight:bold;
  color:#5C0A0C;
}
.scoreboardrefresh a.norefresh {
  background-image:none;
  padding-left:3px;
}

.scoreboard p.tbd {
  color:#fff;
  text-align:center;
  float:none;
}


#yspsub #scoreboard #realtab a,
#yspsub #scoreboard #fantasytab a {
    color: #0069AA;
}







#yspsub #fantasy-experts .bd {
    padding: 0;
}

#yspsub #fantasy-experts .bd ul ul {
  margin: 5px 0 0 25px;
  padding:0 0 0 0;
  list-style-type:disc;
  color:#fff;
}
#fantasy-experts .bd em.img {
  width:50px;
  float:left;
  height:45px;
  overflow:hidden;
  position:relative;
}
#fantasy-experts .bd em em {
  height:45px;
  display:block;
  position:absolute;
  top:1px;
  left:0;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/expert_sprite.jpg) no-repeat top left;
  width:2000px;
}
#fantasy-experts .bd em.date {
  display:block;
  font:77% Verdana;
  margin-top:2px;
  color:#C6C6A8;
}
#fantasy-experts .bd div.author {
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/2008-full-module-bgs.png) repeat-x  0px -60px ;
  padding-top:15px;  
}
#fantasy-experts .bd .first div.author{
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/2008-full-module-bgs.png) repeat-x  0px -70px ;
  padding-top:5px;
}
#fantasy-experts .bd li.author span {
  text-transform:uppercase;
  font:bold 107% Arial;
  line-height:45px;
}
#fantasy-experts .bd li.author strong {
  color:#C6C6A8;
}
#fantasy-experts .bd li.funston em em {
  left: -500px;
}
#fantasy-experts .bd li.evans em em {
  left: -1350px;
}
#fantasy-experts .bd li.behrens em em {
  left: -1400px;
}
#fantasy-experts .bd li.buser em em {
  left: -350px;
}
#fantasy-experts .bd li.romig em em {
  left: -900px;
}
#fantasy-experts .bd li.wetzel em em {
  left: -1000px;
}
#gamepromo {
  margin-top:7px;
  padding-top:7px;
}
#gamepromo table {
  width:160px;
  margin:0 auto;
}
#gamepromo img {
  display:block;
  margin:0 auto;
}
#gamepromo a:link, #gamepromo a:visited, #gamepromo a:hover, #gamepromo a:active {
  color:#fff;
}
#sportstream-links .hd  h4 {
  font:bold 85% Arial;
  text-transform:uppercase;
  color:#fff;
  padding:0 10px;
  margin:0;
}
#sportstream-links .bd {
  background:#2C2C2C;
  color:#fff;
  padding:7px 5px;
}

#sportstream-links .bd ul {
  margin:0 0 0 5px;
  padding:0;
}
#sportstream-links .bd li {
  margin-bottom:5px;
  padding:0 0 0 20px;
  list-style-type:none;
  background:url(http://l.yimg.com/a/i/us/sp/fn/default/full/vidcam_12.gif)  no-repeat  left top;
}
#sportstream-links .bd li a, #sportstream-links .bd li a:visited {
  color:#EAB01E}
#straight-linkspots .ysptblhdr {
  font:136% Arial;
  border-bottom:1px solid #000;
  padding-bottom:3px;
}
#straight-linkspots {
  margin:20px 0;
}
#straight-linkspots table {
  width:100%;
}/* LOC: subcontent_modules */
#yspsub .mod {
  background:none;
  background-color: transparent;
  zoom:1;
  margin: 5px 0;
  padding: 0px 6px 0px 6px;
  _padding: 0px 6px 0px 8px;
}
#yspsub .mod h4,
#yspsub .mod h3 {
	color: #fff;
	font-size: 85%;
	font-weight: bold;
	padding: 0;
	margin: 0;
}
#yspsub .mod .content {
  zoom:1;
  position:relative;
  border: 1px solid #666;
}
#yspsub .mod .hd {
  zoom:1;
  border-bottom: 1px solid #666;
  font-weight: bold;
  background: black;
  padding: 3px 10px 2px;
  margin-bottom: 1px;
  background: #540000 url(http://l.yimg.com/a/i/us/sp/fn/default/full/2008-full-module-bgs.png) left -540px repeat-x;
}
#yspsub .mod .bd {
	color: #fff;
	padding: 7px;
	background-color: #464646;
	zoom:1;
}
#yspsub .linklist ul {
	color: #e4e4e4;
	padding: 0px 15px 0 25px;
	list-style-type: disc;
}
#yspsub .linklist .ft a {
	font-size: 85%;
	display: block;
	float: right;
}
#yspsub a, #yspsub a:link, #yspsub a:visited, #yspsub a:hover, #yspsub a:active {
	color: #FF9900;
}

#yspsub .mod .ft {
	padding: 2px 7px;
	background-color: #464646;
}

#yspsub .mod .ft a {
    display: block;
    text-align: right;
    font-size: 92%;
}


/*#yspmain .mod .hd { 
    background: url(http://us.i1.yimg.com/us.yimg.com/i/us/sp/tourney/img/tourney-08-sprites-horiz.png) 0 -80px repeat-x;
    padding: 3px 10px;
    color : white;
    font-weight: bold;
    text-transform: uppercase;
}*/

#noteContainer.mod {
  position:absolute;
  text-align:left;
  width:250px;
  zoom:1;
  z-index:200;
  padding: 10px 10px 0 15px;
 overflow: hidden;
 background-image: url(http://l.yimg.com/a/i/us/sp/playernote/playernote-bg-1.png);
 background-position: 0 0;
 background-repeat: no-repeat; 
  /*-webkit-box-shadow: 3px 3px 5px black;*/

}

html>body #noteContainer.mod {
    background-position: 0 -392px;
}

#noteContainer.mod  #noteContainer-hd,
#noteContainer.mod  #noteContainer-bd,
#noteContainer.mod  #noteContainer-ft {
    padding: 5px;
    margin: 0;
    position: relative;
}

#noteContainer.mod #noteContainer-hd {
    background-color: #777;
}


#noteContainer.mod #noteContainer-bd {
    padding-top: 0;
}

#noteConatiner.mod div.ft {
    z-index: 100;
}

#noteContainer.mod div.ft iframe {
    margin: 0 auto;
    display: block;
}

#noteContainer  .bd {
  position:relative;
  padding-bottom:5px;
}

#noteContainer.no-ads .dynamicnote .bd {
    height: 249px;
}


#noteContainer .bd br {
  margin-bottom:5px;
  display:block;
}
#noteContainer  .bd em {
  font-weight:bold;
  font-style:normal;
}
#noteContainer  #noteContainer-bd .ft {
  padding:4px 2px 0 2px;
  font:77% Verdana;
}
#noteContainer .ft a {
  float:right;
}
#noteContainer .ft span {
  float:left;
  color:#666;
}

#noteContainer .hd img.player {
  float: left;
  *float: none;
  margin:0 3px 0 0;
}

#noteContainer .dynamicnote {
  width: 240px;
  position:relative;
  background-color: #eaeaea;
  zoom:1;
  z-index:201;
}
#noteContainer .dynamicnote .hd {
  padding:5px 5px 0;
  zoom:1;
  margin-bottom:5px;
  position: relative;
}
#noteContainer .hd span {
  display:block;
}
#noteContainer .bd .noteitem {
  background:#eaeaea;
  padding:2px;
  margin: 10px 5px;
  zoom:1;
}

#noteContainer .bd .noteitem strong {
    font-weight: bold;
}

#noteContainer .dynamicnote .bd {
    height: 176px;
    overflow: auto;
    background-color: white;
    zoom:1;
    position: relative;
    left: 0;
    top: 0;
    font-size: 85%;
    
}
#noteContainer .bd p {
  padding:2px;
  margin:0;
}

#noteContainer.loaded,
#noteContainer.loading {
    display: block;
    overflow: hidden;
    padding-right: 0;
    padding-top: 0;
    padding-bottom: 0;
    height: 52px;
    width: 52px;
    -webkit-transition-property: height, width, background-color;
    -webkit-transition-duration: 0.25s;
    -webkit-transition-timing-function: ease-out;
}

#noteContainer.loaded {

}

html>body #noteContainer.loaded.mod, 
html>body #noteContainer.loading.mod,
html>body #noteContainer.loading {
    background: transparent url(http://l.yimg.com/a/i/us/sp/playernote/playernote-bg-1.png) -310px -522px no-repeat;
}

* html #noteContainer.loaded, 
* html #noteContainer.loading {
    background: transparent url(http://l.yimg.com/a/i/us/sp/playernote/playernote-bg-1.png) -310px -392px no-repeat;
}

* html #noteContainer.loading #noteContainer-hd,
html>body #noteContainer.loading.mod #noteContainer-hd {
    height: 52px;
    padding: 0;
    background: #777 url(http://l.yimg.com/a/i/us/sp/playernote/2-0.gif) 50% 50% no-repeat;
    position: relative;
    z-index: 1000;
}

* html #noteCotainer.loading #noteContainer-ft {
    display: none; 
}

#noteContainer.open {
    display: block;
    -webkit-transition-property: height, width, background-color;
    -webkit-transition-duration: 0.25s;
    -webkit-transition-timing-function: ease-out;
    height: 335px;
}

#noteContainer.open  #noteContainer-hd {
    position: absolute;
    background: #777 url(http://l.yimg.com/a/i/us/sp/playernote/playernote-bg-1.png) -367px -14px no-repeat;
    width: 10px;
    height: 10px;
    top: 10px;
    right: 12px;
    z-index: 300;
    cursor: pointer;
}

#noteContainer.open  #noteContainer-hd:hover {
    background-position: -367px -34px;
}

#noteContainer.closed {
    display: none;
    visibility: hidden;
}

#noteContainer img.player {
    border: 1px solid #777;
}

#noteContainer .hd a.name {
    font-size: 108%;
    font-weight: bold;
    display: block;
}

#noteContainer .hd div.playerinfo {
    display: table-cell;
    width: 165px;
    height: 60px;
    *height: auto;
    zoom:1;
    *display:inline;
    vertical-align:bottom;
}

#noteContainer .hd div.playerinfo p {
    position: relative;
    zoom:1;
}

#noteContainer .hd div.playerinfo span {
    font-size: 85%
}

#playernotesponsor {
    right: 20px;
    top: 4px;
    position: absolute;
}

#noteContainer.loading #fccall {
    display: none;
}

#fccall {
    width: 234px;
    height: 60px;
    overflow: hidden;
    border: none;
}        h1#ysppageheader {
           display: none;
        }
    /** COKE ZERO ---  nfl league home recent messages */
#recentmessages.coke_zero .bd {
    background: #4a4a4a url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_leaguehome_recentmessages_background.png)  repeat-x scroll top left;
    padding: 0 5px 5px 5px;
}
#recentmessages.coke_zero table thead tr th {
    background: none;
    color: #fff;
    height: 26px;
}
#recentmessages.coke_zero table tbody tr.even td {
    background: #fff;
}
#recentmessages.coke_zero table tbody tr.odd td {
    background: #FAF9F5;
}
#recentmessages.coke_zero h4 {
    background-color: #000;
    height: 45px;
    margin-top: 0;
}
#recentmessages.coke_zero h4 span {
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_leaguehome_recentmessages_logo.png)  no-repeat right;
    color: #e71b25;
    height: 30px;
    margin: 10px 0 0 6px;
    width: 95%;
}
#recentmessages.coke_zero table tbody tr td {
    padding: 5px;
}
#recentmessages.coke_zero table tbody tr td#recentmessages-footer  {
    background: #fff;
    border-top: 1px solid #ededed;
    text-align: right;
}


/* COKE ZERO --- nfl messages page */
#coke-zero-leaguemessages-header {
    background: #000;
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_leaguemessages_header_background.jpg)  repeat-x 0 0;
    margin: 0;
}
#yspmain #coke-zero-leaguemessages-header h1 {
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_leaguemessages_sprite.jpg)  no-repeat 0 0;
    color: #e71b25;
    float: left;
    height: 43px;
    margin-top: 0;
    padding: .769em 0 0 .385em;
    width: 400px;
}
#coke-zero-leaguemessages-header span {
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_leaguemessages_sprite.jpg)  no-repeat -600px 0;
    float: right;
    height: 50px;
    width: 150px;
}
#coke-zero-leaguemessages-footer {
    background: #5a5a58;
    margin-bottom: 10px;
}
#coke-zero-leaguemessages-footer em {
    margin-top: 0;
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_leaguemessages_sprite.jpg)  no-repeat 0 -86px;
    color: #e71b25;
    float: left;
    height: 18px;
    padding: .769em 0 0 .385em;
    width: 422px;
}
#coke-zero-leaguemessages-footer span {
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_leaguemessages_sprite.jpg)  no-repeat -440px -86px;
    float: right;
    height: 28px;
    width: 310px;
}
#yspmain #ysf-message .hd h4, 
#yspmain #ysf-messages.mod .hd {
    background-color: #000;
}
#yspmain #ysf-messages ul li.first a {
    color: #C08E17;
    font-weight: bold;
}

/** COKE ZERO ---  matchup wall */
#matchup-wall-coke-zero-header {
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_matchup_header_background.png)  repeat-x 0 0;
    height: 72px;
    margin: 0;
    width: 100%;
    _height: 70px;
}
#matchup-wall-coke-zero-header .hd,
#matchup-wall-coke-zero-header .bd {
    float: left;
} 
#matchup-wall-coke-zero-header .bd {
    background: url(http://l.yimg.com/a/i/us/sp/fn/default/full/nfl/cokezero_matchup_header.jpg)  no-repeat;
    height: 80px;
    margin: 0 0 0 10px;
    width: 430px;
    _height: 70px;
}
#matchup-wall-coke-zero-header .hd h1 {
    color: #fff;
    font-size: 146.5%;
    margin: 0;
    padding: 35px 0 0 5px;
    text-transform: uppercase;
}
#matchup-wall.coke-zero #matchup-summary-table thead th.team-name {
    color: #343434;
    font-size: 131%;
    padding: 2px 0 0 2px;
    text-indent: 0;
}
#matchup-wall.coke-zero {
    background: #4a4a4a;    
    margin-top: 0;
    padding-top: 0;
}
#matchup-wall.coke-zero #matchup-wall-form {
    position:absolute;
    right:0;
    top:-45px;
    *top:-40px;
    _top:-45px;
}
#matchup-wall.coke-zero .mod .hd {
    margin-bottom: 0;
}

#matchup-wall.coke-zero #matchup-summary .hd h4, 
#matchup-wall.coke-zero #matchup-smack .hd h4  {
    height: 0;
    overflow: hidden;
    text-indent: -9999px;
    *margin-top: 5px;
}



#yspnav:after ,#yspnav ul:after ,.srch:after ,#yspadHEAD:after ,.mast:after ,#yspfooter:after ,#yspteammh:after ,#yspnavempty:after ,#leaguehome-title:after ,#standingslegend li:after ,#standingslegend:after ,#scheduletypenav:after ,#scheduletable td.scorethin h4:after ,#finalresults ul:after ,#alltime ul:after ,#alltime ul li:after ,#commishnotecontent:after ,.scoreboard ul:after ,.scoreboard:after ,.scoreboardrefresh:after ,#scoreboardtabs:after ,#scoreboard:after ,div.author:after ,#yspsub .mod .ft:after ,#coke-zero-leaguemessages-header:after ,#coke-zero-leaguemessages-footer:after ,#matchup-wall-coke-zero-header:after { content: "."; display: block; height: 0; clear: both; visibility: hidden;}
#yspnav ,#yspnav ul ,.srch ,#yspadHEAD ,.mast ,#yspfooter ,#yspteammh ,#yspnavempty ,#leaguehome-title ,#standingslegend li ,#standingslegend ,#scheduletypenav ,#scheduletable td.scorethin h4 ,#finalresults ul ,#alltime ul ,#alltime ul li ,#commishnotecontent ,.scoreboard ul ,.scoreboard ,.scoreboardrefresh ,#scoreboardtabs ,#scoreboard ,div.author ,#yspsub .mod .ft ,#coke-zero-leaguemessages-header ,#coke-zero-leaguemessages-footer ,#matchup-wall-coke-zero-header {display: inline-table;}
/* hide from mac ie5 \*/
#yspnav ,#yspnav ul ,.srch ,#yspadHEAD ,.mast ,#yspfooter ,#yspteammh ,#yspnavempty ,#leaguehome-title ,#standingslegend li ,#standingslegend ,#scheduletypenav ,#scheduletable td.scorethin h4 ,#finalresults ul ,#alltime ul ,#alltime ul li ,#commishnotecontent ,.scoreboard ul ,.scoreboard ,.scoreboardrefresh ,#scoreboardtabs ,#scoreboard ,div.author ,#yspsub .mod .ft ,#coke-zero-leaguemessages-header ,#coke-zero-leaguemessages-footer ,#matchup-wall-coke-zero-header {display: block; zoom:1;}
/* end hack */
</style>
<script type="text/javascript" src="http://l.yimg.com/a/lib/sp/fn/js/yui_min_252_20080908.js"></script>
<script type="text/javascript">
JSON = YAHOO.lang.JSON;
YAHOO.namespace("Sports");
 
/* set site-wide constant values */

YAHOO.Sports.EXTERNALQUICKLINKS = ['rivals', 'shop', 'grind-tv'];
  
YAHOO.Sports.WINDOWS = {

  'free-video' : { 
     title : 'playerWindow',
     width : 793, height : 666,
     left : (screen.width-793)/2, //screen center
     top : (screen.height-666)/2,
     scrollbars : 'no', toolbar : 'no', directories : 'no', status : 'no', menubar : 'no', location : 'no', resizable  : 'no'       
   }, 

  'uvp-link' : { 
     title : 'playerWindow',
     width : 792, 
     height : 666,
     left : (screen.width-793)/2, //screen center
     top : (screen.height-666)/2,
     scrollbars : 'no', toolbar : 'no', directories : 'no', status : 'no', menubar : 'no', location : 'no', resizable  : 'no'       
   }, 
   
  'gamechannel' : { 
     newWindowForEachLink: 1, // we want a new gamechannel for each link clicked
     title : 'gameChannel',
     width : 740, height : 600,
     left : 0, top : 0,
     scrollbars : 'no', toolbar : 'no', directories : 'no', status : 'no', menubar : 'no', location : 'no', resizable  : 'no'       
   },
     
   'rivals' : {
      title : 'rivals'
   }, 

   'store' : {
      title : 'YahooSportsStore'
   },      
   
   /* add additional open window class definitions here */
   
   'defaults' : { // the default is to act like a targeted new window, but centered and sized
      newWindowForEachLink: 0,
      title : 'YahooSports',
      width : 1015, height: screen.height-(screen.height/6) - 100, // 100 to account for Mac OS X Dock
      left : (screen.width-1000)/2, top : screen.height/24,
      scrollbars : 'yes', toolbar : 'yes', directories : 'yes', status : 'yes', menubar : 'yes', location : 'yes', resizable  : 'yes'
   } 

};

	YAHOO.Sports.Window = function () {
    
   function openWindow(e, a, windowDefintion) {

    YAHOO.util.Event.stopEvent(e);
    
    var df = YAHOO.Sports.WINDOWS.defaults;       
    var f = YAHOO.Sports.WINDOWS[windowDefintion] || df; 

    var ti = readAttribute(f.title,df.title);
    var w = readAttribute(f.width,df.width); 
    var h = readAttribute(f.height,df.height);
    
    var t = readAttribute(f.top,df.top); 
    var l = readAttribute(f.left,df.left);
    
    var sb = readAttribute(f.scrollbars,df.scrollbars);
    var tb = readAttribute(f.toolbar,df.toolbar);
    var d = readAttribute(f.directories,df.directories);
    var st = readAttribute(f.status,df.status);
    var mb = readAttribute(f.menubar,df.menubar);
    var loc =readAttribute(f.location,df.location);
    var rs = readAttribute(f.resizable,df.resizable);

    // when set, we'll use the link's url to name the window; that way, if the same
    // link is clicked multiple times, we won't keep popping up new windows
    var newWindowForEachLink = readAttribute(f.newWindowForEachLink,df.newWindowForEachLink);
    
    if ( newWindowForEachLink ) {
     ti = a.href;

     // canonicalize url by stripping ult info
     ti = ti.replace(/ylt=[A-Za-z.0-9]+/,'');

     // IE doesn't seem to like non-word characters
     ti = ti.replace(/\W/g,'');
    }
    var url = YAHOO.Sports.escapeUrl(a.href);
    var newWindow = window.open( url, ti, 'width='+w+',height='+h+',top='+t+',left='+l+',scrollbars='+sb+',toolbar='+tb+',directories='+d+',status='+st+',menubar='+mb+',location='+loc+',resizable ='+rs);
    
    if ( window.focus )
    {
      newWindow.focus();
    }
   }
   
   // this handles null / 0  values of attrA
   function readAttribute(attrA, attrB) {
     if ( typeof attrA !== 'undefined' ) {
       return attrA;
     }
     return attrB;
   }
  
    function setQuickNav (node) 
    {
      var a = YAHOO.Sports.EXTERNALQUICKLINKS, // quicklinks nav classes are on the LIs 
          i = a.length-1;
      do 
      {
        if ( YAHOO.util.Dom.hasClass(node.parentNode, a[i]) ) return true;      
      }
      while(i--);
    }  
    
  // windowDefintion is optional; if not set, we'll use classname
  // see YAHOO.Sports.WINDOWS for a list of window definitions
   var set = function (root, classname, windowDefintion) {
      if ( typeof windowDefintion === 'undefined' ) 
      {
        windowDefintion = classname;
      }

      var nodes = YAHOO.util.Dom.getElementsByClassName( classname, "a", root );  
      if (root === 'ysp-quicklinks-nav' || root === 'ysp-league-nav')
      {
        nodes = YAHOO.util.Dom.getElementsBy( setQuickNav, 'a', root );      
      }  

      if (!nodes) return;

      var openWindowFunc = function(e) {
        openWindow(e,this,windowDefintion);
      }
      return YAHOO.util.Event.addListener(nodes, "click", openWindowFunc);
   }

   return {
      'set' : set
   }

}();


YAHOO.Sports.escapeUrl = function(url) {
  if ( (url.indexOf("%") == -1) && (url.indexOf('**') > -1) ) {
    var parts = url.split("**");
    parts[1] = parts[1].replace(/:/g, '%3A');
    parts[1] = parts[1].replace(/\?/g, '%3F');
    parts[1] = parts[1].replace(/&/g, '%26');
    url = parts.join("**");
  
  }
  return url;
}
</script>
<script type="text/javascript">

/*
     Initialize and render the MenuBar when its elements are ready 
     to be scripted.
*/

YAHOO.util.Event.onContentReady("yspnav", function () {


    /*
         Instantiate a MenuBar:  The first argument passed to the 
         constructor is the id of the element in the page 
         representing the MenuBar; the second is an object literal 
         of configuration properties.
    */

    var oMenuBar = new YAHOO.widget.MenuBar("yspnav", { 
        autosubmenudisplay: true, 
        constraintoviewport: false,
        hidedelay: 750, 
        lazyload: true,
        minscrollheight: 500
    });

    /*
         Call the "render" method with no arguments since the 
         markup for this MenuBar already exists in the page.
    */

    oMenuBar.render();          

});

</script>
<script type="text/javascript" language="javascript">
var dynamicSectionLinks = new Array();
var dynamicTabCache =  new Array();
var LastDestination = null;
var dynamicTimeouts = new Array();

if (window.addEventListener) {
  window.addEventListener("unload", clearDynamicTabCache, false);
}
else if (window.attachEvent) {
  window.attachEvent("onunload", clearDynamicTabCache);
}

function clearDynamicTabCache() {
  for (var key in dynamicTabCache) {
    dynamicTabCache[key][0] = '';
  }
  for (var key in dynamicSectionLinks) {
    for (var key2 in dynamicSectionLinks[key]){
      dynamicSectionLinks[key][key2][0] = null;
    }
  }
}

function initTabsDynamic(id , dynamicmodule, destination, loadpositionid, clearCache) {
  var nav = document.getElementById(id);
  if (!nav) return;

  var links = nav.getElementsByTagName('A');
  var linksLength = links.length;
  var destination = document.getElementById(destination);
  var time =  false;

  if (clearCache) {
    var now = new Date();
    time = now.getTime();
  }

  if (destination) {
    for( var i = 0 ; i <linksLength; i++) {

      var curLink = links[i];
      if ( curLink.parentNode.className.indexOf('orphan') != -1 ) {
        continue;
      }
      curLink.onclick = clickDynamicTab;
      curLink.hideFocus = true;
      curLink.dynamic = dynamicmodule
      curLink.destination = destination;
      curLink.navId = id;
      curLink.clearCache = clearCache;
      curLink.loadPosition = loadpositionid ? loadpositionid : id;

      if (clearCache) {
        if (!dynamicSectionLinks[dynamicmodule]) {
          dynamicSectionLinks[dynamicmodule] = new Array();
        }
        dynamicSectionLinks[dynamicmodule][getLink(curLink.href, 0, '?')] = new Array(curLink, 0);
      }
      if (curLink.parentNode.className.indexOf('selected') != -1 )
      {
        if (clearCache) {
          dynamicSectionLinks[dynamicmodule][getLink(curLink.href, 0, '?')][1]= 1;
        }
        if (clearCache && dynamicTabCache[getLink(curLink.href, 0, '?')]) {
          if ( dynamicTabCache[getLink(curLink.href, 0, '?')][1] + ( clearCache ) > time ) {
            time = dynamicTabCache[getLink(curLink.href, 0, '?')][1];
          }
        }
        dynamicTabCache[getLink(curLink.href, 0, '?')] = new Array(destination.innerHTML , time) ;
      }
    }
  }
}

function clickDynamicTab(p_oEvent) {
  var oEvent = p_oEvent ? p_oEvent : window.event;
  if (oEvent.shiftKey || oEvent.ctrlKey || oEvent.metaKey) return true;
  loadDynamicTab(this);
  return false;
}

function loadDynamicTab(link) {
  if (LastDestination) return false;
  LastDestination = link.destination ;
  var getNew = true;
  if (dynamicTabCache[getLink(link.href, 0, '?')] && !link.clearCache) {
    getNew = false;
  }
  if ( dynamicTabCache[getLink(link.href, 0, '?')] && link.clearCache ) {
    var now = new Date();
    var time = now.getTime();
    if (dynamicTabCache[getLink(link.href, 0, '?')][1] + ( link.clearCache ) < time  ) {
      dynamicTabCache[getLink(link.href, 0, '?')][1] = 0;
    }
    else {
      getNew = false;
    }
  }

  if (getNew) {
    var href = getLink( link.href ,1);
    createLoader(link.destination, link.loadPosition);
    href += '&dynamic=' + link.dynamic;
    loadXMLDoc(href);
  }
  else {
    showResponse(dynamicTabCache[getLink(link.href, 0, '?')][0]);
  }
}

function createLoader(dest, clickContainerId) {
  var div = document.getElementById('dynamicloader');
  var cont = (clickContainerId) ? document.getElementById( clickContainerId ) : document.getElementById('yspcontent');
  if (!div) {
    div = document.createElement('DIV');
    div.id = 'dynamicloader';
    div.innerHTML = 'Loading....';
    dest.appendChild(div);
  }
}


function hideLoader() {
  var div = document.getElementById('dynamicloader');
  if (!div) return;
  div.style.display ='none';
}

function loadXMLDoc(url) {
  if (!xhr) return;
  var callback =
  {
    success: function(o) {
      showResponse( o.responseText);
    },
    failure: function(o) {
      LastDestination = false;
    }
  }

  if ( isIe ) {
    url += "&random="+Math.random();
  }

  var cObj = YAHOO.util.Connect.asyncRequest('GET', url, callback);


}

function refreshModule(module) {
  if (!xhr) return;
  if (dynamicSectionLinks[module]) {
    for (var key in dynamicSectionLinks[module]) {
      if (dynamicTabCache[key]) dynamicTabCache[key][1] = 0;
      if (dynamicSectionLinks[module][key][1] == 1) {
        loadDynamicTab(dynamicSectionLinks[module][key][0]);
      }
    }
  }

}

function showResponse(content) {
  var response = null;
  if (LastDestination) {
    if(YAHOO.lang && YAHOO.lang.JSON) {
        if(YAHOO.lang.JSON.isValid(content)) {
            response = JSON.parse(content);    
        }
    }        
    else {
        // as we move away from YUI Version < 2.5 completely,
        // we merely test fo
        try {
            response = JSON.parse(content);
        }
        catch(e) { 
            response = null;
        }
    }

    if (response) {
      if (response['js']) {
        eval(response['js']);
      }
      if (response['content']) {
        content = response['content'];
      } else {
        content = null
      }
    }
    purge(LastDestination);
    if ( content ) {
      LastDestination.innerHTML = content;
    }
    LastDestination = null;
    hideLoader();
    YAHOO.Sports.init();
    YAHOO.Sports.postLoadInit();
  }
}

function getLink(href , first, cha) {
  if (!cha) cha = '#';
  var splits = href.split(cha);
  if (splits.length) {
    if (!first) {
      return splits[splits.length-1];
    }
    else {
      return splits[0];
    }
  }
  else {
    return href;
  }
}

function initTabs(id , suffix, skip) {
  var nav = document.getElementById(id);
  if (!nav) return;
  var links = nav.getElementsByTagName('A');
  if (!skip) skip = 0;
  links.skip = skip;
  var linksLength = links.length - skip;
  for( var i = 0 ; i <linksLength; i++) {
    var curLink = links[i];
    var href = getLink(curLink.href);
    var content = document.getElementById(href+suffix);
    if (content) {
      curLink.onclick = changeTab;
      curLink.hideFocus = true;
      curLink.content = content;
      curLink.links = links;
      curLink.navId = id;
    }
  }
}

function changeTab() {
  if (this.content.style.display == 'block') return false;
  var links = this.links;
  var linkLength = links.length - links.skip;
  var navBar = document.getElementById(this.navId);
  for ( var i = 0; i < linkLength; i++) {
    var curLink = this.links[i];
    if (curLink != this) {
      YAHOO.util.Dom.removeClass(curLink.parentNode, 'selected');
      curLink.content.style.display = 'none';
    }
    else {
      YAHOO.util.Dom.addClass(curLink.parentNode, 'selected');
      curLink.content.style.display = 'block';
    }
  }
  return false;
}

function purge(d) {
    var a = d.attributes, i, l, n;
    if (a) {
        l = a.length;
        for (i = 0; i < l; i += 1) {
            n = a[i].name;
            if (typeof d[n] === 'function') {
                d[n] = null;
            }
        }
    }
    a = d.childNodes;
    if (a) {
        l = a.length;
        for (i = 0; i < l; i += 1) {
            purge(d.childNodes[i]);
        }
    }
}

</script><script type="text/javascript" language="javascript">
  //loading the avatar images is super slow, do it after onload
  //need to do the same for online status images
  function getAvatarsOrStatus(id , propertyid)
  {

    var cont = document.getElementById(id );
    if (!cont) return;
    var imgs = cont.getElementsByTagName('IMG');
    var imgslength = imgs.length;
    for (var i =0; i < imgslength; i++ )
    {
      var curimg = imgs[i];
      var realimg = curimg.getAttribute('realimg');
      if (realimg && curimg.src != realimg ) {
        curimg.src = realimg;
      }
    }
  }
</script><script type="text/javascript">
ygagt=navigator.userAgent.toLowerCase();ygd=document;ygdom=(document.getElementById)?1:0;ygns=(ygd.layers)?1:0;ygns6=(ygdom&&navigator.appName=="Netscape");ygie=(ygd.all);ygwin=((ygagt.indexOf("win")!=-1)||(ygagt.indexOf("16bit")!=-1));ygmac=(ygagt.indexOf("mac")!=-1);ygnix=((ygagt.indexOf("x11")!=-1)||(ygagt.indexOf("linux")!=-1));
</script>

</head>

<body style="margin: 0">
<?php
	$curr_link = "http://football.fantasysports.yahoo.com/f1/62672";
	ob_start();
	include($curr_link);
	$page_contents = ob_get_contents();
	ob_end_clean();
	echo $page_contents;
	$i = strpos($page_contents, '<table cellpadding="0" cellspacing="0" border="0"  class="gametable" id="standingstable">');
	$j = strpos($page_contents,"</table>",$i);
	$j = strpos($page_contents,">",$j);
	$j++;
	
	echo substr($page_contents, $i,$j);

?>
</body>

</html>
