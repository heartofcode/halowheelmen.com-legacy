
<!----------------------------------------
	START OF PAGE
----------------------------------------->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<!----------------------------------------
		HEADER
	----------------------------------------->
	
	<title>Halo Wheelmen - Community, Statistics & Ponies</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<link rel="stylesheet" type="text/css" href="<?php echo getEnv('ROOT_PATH') ?>/styles/metro/style-metro.css">

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo getEnv('ROOT_PATH') ?>/scripts/smoothscroll.js"></script>

	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">

	<link rel="manifest" href="<?php echo getEnv('ROOT_PATH') ?>/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">

	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">

	<meta name="theme-color" content="#ffffff">

</head>

<!----------------------------------------
	BODY
----------------------------------------->

<body>

	<!---------------------------------------- 
		NAVIGATION 
	----------------------------------------->

	<input type="checkbox" id="mobile-menu-trigger" />
	<label id="hamburger-icon" for="mobile-menu-trigger"></label>

	<div class="menu-hamburger">

		<!-- LINKS -->

		<a href="/index.php">Home</a>
		<a href="<?php echo getEnv('ROOT_PATH') ?>/guides/halo-5/h5-guide.php">Guides</a>
		<a href="<?php echo getEnv('ROOT_PATH') ?>/forum">Forum</a>
		<a href="<?php echo getEnv('ROOT_PATH') ?>/pages/blog/blog.php">Blog</a>

	</div>
	
	<!-- Site Wrapper -->
	
	<div class="site-wrapper">

		<!-- Standard Navigation -->
		
		<div id="nav-primary" class="navigation">

			<div class="nav-title">
				<a href="/index.php">HaloWheelmen</a>
			</div>
			
			<div class="menu">

				<!-- LINKS -->

				<a href="/index.php">Home</a>
				<a href="<?php echo getEnv('ROOT_PATH') ?>/pages/guides/halo-5/h5-guide.php">Guides</a>
				<a href="<?php echo getEnv('ROOT_PATH') ?>/forum">Forum</a>
				<a href="<?php echo getEnv('ROOT_PATH') ?>/pages/blog/blog.php">Blog</a>

			</div>

		</div>
	
		<!----------------------------------------
			BANNER
			
			Preferably use absolute paths.
			The final parameter should be either
			"fullscreen" or ""
		----------------------------------------->	
		
		<!----------------------------------------
			FEATURED MEDIA
		----------------------------------------->
			
		<div id='hero-video' class='img-container hero-video fullscreen'>

			<a href='#content' class='arrow-down smoothScroll'></a>
			<div class='overlay black'>
				<h1>Home of the Warthog</h1>
			</div>
		
			<video width='auto' height='auto'autoplay loop muted>
				<source id='hero-video-source' src='<?php echo getEnv('ROOT_PATH') ?>/pages/index/images/featured.mp4' type='video/mp4'>
			</video>
		
		</div>
	
	<!----------------------------------------
		FEATURED MEDIA
	----------------------------------------->

	<div id='hero-image' class='img-container hero-image fullscreen'>
		<a href='#content' class='arrow-down smoothScroll'></a>
		<div class='overlay black'>
			<h1>Home of the Warthog</h1>
		</div>
		<img src='<?php echo getEnv('ROOT_PATH') ?>/images/featured2.jpg' />
	</div>	
	
	<!----------------------------------------
		VIDEO LOADER
	----------------------------------------->
	
	<script>

		var heroImg = document.getElementById("hero-image");
		var vidMP4 = document.getElementById("hero-video");
		var vidSRC = vidMP4.src;
	
		heroImg.setAttribute("style", "display: block;");
		vidMP4.setAttribute("style", "display: none;");
		
		vidMP4.setAttribute("src", "");
		
		document.onreadystatechange = function () 
		{
			//console.log("readyState = " + document.readyState);
			if (document.readyState == "complete") 
			{
				init();
			}
		}
	
		function init()
		{
			vidMP4 = document.getElementById("hero-video");
			vidMP4.setAttribute("src", vidSRC);
			
			heroImg.setAttribute("style", "");
			vidMP4.setAttribute("style", "");
		}

	</script>
	
	
	<!----------------------------------------
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none">
	
		<div class="section-none flex-container bg-blue">
		
			<div class="img-container flex-col-third-2 hide-on-mobile">
				<img src="<?php echo getEnv('ROOT_PATH') ?>/images/index/homepage1.jpg" />
			</div>
			
			<div class="section-medium flex-col-third-1 flex-right">
				<h1>Who are we?</h1>
				<p>
					Founded in 2008, the Wheelmen are a Halo clan for teamwork-oriented players and fans of the Warthog. Read more about us <a href="<?php echo getEnv('ROOT_PATH') ?>/forum/viewtopic.php?f=88&t=11974">here.</a>
				</p>
			</div>
		</div>

		<div class="section-none flex-container bg-blue">
		
			<div class="img-container flex-col-third-2 flex-right">
				<img src="<?php echo getEnv('ROOT_PATH') ?>/images/index/homepage2.jpg" />
			</div>
			
			<div class="section-medium flex-col-third-1">
				<h1>Why us?</h1>
				<p>
					You want to play Halo with polite teammates. The Wheelmen focus heavily on developing talent and teamwork, especially through our <a href="<?php echo getEnv('ROOT_PATH') ?>/pages/guides/">guides.</a>
				</p>
			</div>

		</div>
		
		<div class="section-none flex-container bg-blue">
			<div class="img-container flex-col-third-2">
				<img src="<?php echo getEnv('ROOT_PATH') ?>/images/index/homepage3.jpg" />
			</div>
			
			<div class="section-medium flex-col-third-1 flex-right">
				<h1>Join Now!</h1>
				<p>
					Whether you're interested in joining the clan or playing with some quality teammates, post an introduction <a href="<?php echo getEnv('ROOT_PATH') ?>/forum/viewforum.php?f=22">here</a> and get into our lobbies.	
				</p>
			</div>
		</div>
	
	</div>

	<!---------------------------------------- 
		FOOTER
	----------------------------------------->
	
	<div id="footer" class="navigation section-small">
		<a href="http://heartofcode.net.com" target="blank" class="float-left">Site by Addison Short</a>
		<a href="<?php echo getEnv('ROOT_PATH') ?>/pages/roster/roster.php">Roster</a>
	</div>

	<script src="<?php echo getEnv('ROOT_PATH') ?>/scripts/navigation-transition.js"></script>	
	
	<!-- End Site Wrapper -->
	</div>
	
</body>

</html>
