<?php
/**
*
* @package hjw calendar Extension
* @copyright (c) 2016 calendar
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hjw\calendar\acp;

class main_info
{
	function module()
	{
		return array(
			'filename'	=> '\hjw\calendar\acp\main_module',
			'title'		=> 'ACP_CALENDAR_TITLE',
			'version'	=> '0.8.3',
			'modes'		=> array(
				'instructions'		=> array('title' => 'ACP_CALENDAR_INSTRUCTIONS',	'auth' => 'ext_hjw/calendar && acl_a_board', 'cat' => array('ACP_CALENDAR_TITLE')),
				'displayoptions'	=> array('title' => 'ACP_CALENDAR_DISPLAYOPTIONS',	'auth' => 'ext_hjw/calendar && acl_a_board', 'cat' => array('ACP_CALENDAR_TITLE')),
				'settings'			=> array('title' => 'ACP_CALENDAR_EVENT_CONFIG',	'auth' => 'ext_hjw/calendar && acl_a_board', 'cat' => array('ACP_CALENDAR_TITLE')),
				'forums_settings'	=> array('title' => 'ACP_CALENDAR_FORUMS_CONFIG',	'auth' => 'ext_hjw/calendar && acl_a_board', 'cat' => array('ACP_CALENDAR_TITLE')),
				'event_list'		=> array('title' => 'ACP_CALENDAR_EVENT_LIST',		'auth' => 'ext_hjw/calendar && acl_a_board', 'cat' => array('ACP_CALENDAR_TITLE')),
				'special_days'		=> array('title' => 'ACP_CALENDAR_SPECIAL_DAY',		'auth' => 'ext_hjw/calendar && acl_a_board', 'cat' => array('ACP_CALENDAR_TITLE')),
			),
		);
	}
}
