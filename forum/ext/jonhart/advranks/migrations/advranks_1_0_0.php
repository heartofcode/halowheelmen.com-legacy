<?php

/**
 *
 * @package phpBB Extension - Advanced Ranks
 * @copyright (c) 2016 Jon Hart
 *
 */

namespace jonhart\advranks\migrations;

class advranks_1_0_0 extends \phpbb\db\migration\migration
{
	static public function depends_on()
	{
		return array('\phpbb\db\migration\data\v31x\v317pl1');
	}

	public function update_data()
	{
		return array(
			
			// Add permissions
			array('permission.add', array('u_chng_role', true)),

			// Set permissions
			array('permission.permission_set', array('ADMINISTRATORS', 'u_chng_role', 'group')),
			
			
		);
	}

	public function update_schema()
	{
		return array(
			'add_tables' => array(
				$this->table_prefix . 'rank_groups' => array(
					'COLUMNS'		=> array(
						'rank_id'			=> array('UINT:8', 0),
						'group_id'          => array('UINT:8', 0),	
					),
				),
	
			),

			'add_columns' => array(
				$this->table_prefix . 'ranks' => array(
					'selectable_rank' => array('TINT:1', 0),
					
				),
			),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_tables' => array(
				$this->table_prefix . 'rank_groups',
			),


			'drop_columns' => array(
				$this->table_prefix . 'ranks' => array(
					'selectable_rank',
					
				),
			),
		);
	}
}
