<?php

/**
 *
 * @package phpBB Extension - Advanced Ranks
 * @copyright (c) 2016 Jon Hart
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace jonhart\advranks\event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class acp_listener implements EventSubscriberInterface
{
	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\request\request */
	protected $request;

	/** @var \phpbb\user */
	protected $user;

	/** @var string */
	protected $root_path;

	/** @var string */
	protected $php_ext;

	/**
	* Constructor
	*
	* @param \phpbb\template\template	$template
	* @param \phpbb\request\request		$request
	* @param \phpbb\user				$user
	* @param string						$root_path
	* @param string						$php_ext
	*/
	public function __construct(\phpbb\template\template $template, \phpbb\request\request $request, \phpbb\user $user, $root_path, $php_ext)
	{
		$this->template		= $template;
		$this->request		= $request;
		$this->user			= $user;
		$this->root_path	= $root_path;
		$this->php_ext		= $php_ext;
	}

	/**
	 * @return array
	 */
	static public function getSubscribedEvents()
	{
		return array(
			'core.permissions'							=> 'permissions',
		);
	}

	/**
	 * @param Event $event
	 */
	public function permissions($event)
	{
		

		$permission_categories = array(
			'ranks' => array(
				'u_chng_role',
				
			),
			
		);

		$ranks_permissions = array();

		foreach ($permission_categories as $cat => $permissions)
		{
			foreach ($permissions as $permission)
			{
				$ranks_permissions[$permission] = array(
					'lang'	=> 'ACL_' . strtoupper($permission),
					'cat'	=> $cat,
				);
			}
		}

		$event['permissions'] = array_merge($event['permissions'], $ranks_permissions);

		$event['categories'] = array_merge($event['categories'], array(
			'ranks'				=> 'ACP_CAT_ADVRANKS',
		));
	}

}
