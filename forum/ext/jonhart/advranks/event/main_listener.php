<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 * For full copyright and license information, please see
 * the docs/CREDITS.txt file.
 *
 */

namespace jonhart\advranks\event;

/**
* @ignore
*/
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
* Event listener
*/
class main_listener implements EventSubscriberInterface
{
	static public function getSubscribedEvents()
	{
		return array(
			'core.acp_ranks_edit_modify_tpl_ary'	=> 'acp_ranks_edit_modify_tpl_ary',
			'core.acp_ranks_save_modify_sql_ary'	=> 'acp_ranks_save_modify_sql_ary',
			'core.acp_ranks_list_modify_rank_row'   => 'acp_ranks_list_modify_rank_row',
			'core.ucp_profile_modify_profile_info'  => 'ucp_profile_modify_profile_info',
			'core.ucp_profile_info_modify_sql_ary'  => 'ucp_profile_info_modify_sql_ary',
		);
	}

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\request\request */
	protected $request;
	
	protected $rank_groups_table;


	/**
	 * Constructor
	 *
	 * @param \phpbb\template\template			$template
	 * @param \phpbb\user						$user
	 * @param \phpbb\auth\auth					$auth
	 * @param \phpbb\db\driver\driver_interface	$db
	 * @param \phpbb\request\request			$request
	 */
	public function __construct(\phpbb\template\template $template, \phpbb\user $user, \phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\request\request $request, $rank_groups_table)
	{
		$this->template		= $template;
		$this->user			= $user;
		$this->auth			= $auth;
		$this->db			= $db;
		$this->request		= $request;
		$this->rank_groups_table = $rank_groups_table;
	}
	
	public function ucp_profile_modify_profile_info($event)
	{
		$chng_role = $this->auth->acl_get('u_chng_role');
		
	
		$sql = 'SELECT *
			FROM ' . RANKS_TABLE . ' r
			WHERE r.selectable_rank = 1
			AND r.rank_id IN(SELECT DISTINCT rank_id
							FROM '.$this->rank_groups_table.' g
							WHERE g.group_id IN (SELECT group_id
													FROM '.USER_GROUP_TABLE.' gt
													WHERE gt.user_id = '.$this->user->data['user_id'].'))
			ORDER BY r.rank_title';
			
			
			
		$result = $this->db->sql_query($sql);

		
		$s_rank_options = '<option value="0"' . ((!$this->user->data['user_rank']) ? ' selected="selected"' : ' ') . '>' . 'None' . '</option>';

		while ($row = $this->db->sql_fetchrow($result))
		{
			$selected = ($this->user->data['user_rank'] && $row['rank_id'] == $this->user->data['user_rank']) ? ' selected="selected"' : '';
			$s_rank_options .= '<option value="' . $row['rank_id'] . '"' . $selected . '>' . $row['rank_title'] . '</option>';
		}
		$this->db->sql_freeresult($result);

		$this->template->assign_vars(array(
			'S_RANK'			=> true,
			'S_RANK_OPTIONS'	=> $s_rank_options,
			'CAN_CHANGE_ROLE'   => $chng_role,
			)
		);
		
	}
	
	public function ucp_profile_info_modify_sql_ary($event)
	{
		if(!$this->auth->acl_get('u_chng_role'))
		{
			return;
		}
		
		$sql_ary = $event['sql_ary'];
		
		$rank_id = request_var('user_rank', 0);
		
		$sql_ary['user_rank'] = $rank_id; 
		$event['sql_ary'] = $sql_ary;
		
		
	}
	
	
	
	/**
	* Updates selectable_rank and group template data.
	*
	* @param \phpbb\event\data	$event	Event object
	*/
	public function acp_ranks_edit_modify_tpl_ary($event)
	{
		$ranks = $event['ranks'];
		$tpl_ary = $event['tpl_ary'];
		$tpl_new = array('S_SELECTABLE_RANK' => (isset($ranks['selectable_rank']) && $ranks['rank_special'] && $ranks['selectable_rank']) ? true : false,);
		$event['tpl_ary'] = array_merge($tpl_ary, $tpl_new);
		
		
		$sql = 'SELECT group_id, group_name
			FROM '.GROUPS_TABLE.' 
			ORDER BY group_name ASC';	
		$result = $this->db->sql_query($sql);
		
		$sql = 'SELECT group_id
			FROM '.$this->rank_groups_table.'
			WHERE rank_id = '.$ranks['rank_id'].'';	
			$select = $this->db->sql_query($sql);
		
		while($row = $this->db->sql_fetchrow($result))
		{
			$id1 = $row['group_id'];
			
			//Reset internal pointer of select query on loop
			$this->db->sql_rowseek(0, $select);
		
			//Compare at least once in case rank_groups table is empty
			$sel = $this->db->sql_fetchrow($select);
			do
			{
				$id2 = $sel['group_id'];
				
				//Compare to see if group is associated with the current rank in the rank_groups table. Break on true to prevent overwrite.
				if(($id1 == $id2))
				{
					$selected = 1;
					break;
				}
				else
				{
					$selected = 0;
				}
			}while($sel = $this->db->sql_fetchrow($select));
			
			//Assign template variables
			$group_row = array(
				'S_GROUP_NAME'		=> $row['group_name'],
				'S_GROUP_ID'	=> $row['group_id'],
				'GROUP_SELECTED' => $selected,
			);
			
			
			$this->template->assign_block_vars('groups', $group_row);
			
		}
		//Clear the sql results from memory
		$this->db->sql_freeresult($select);
		$this->db->sql_freeresult($result);
		
		
	}
	
	/**
	* Add column to ranks list to display whether users may select the rank themselves.
	*
	* @param \phpbb\event\data	$event	Event object
	*/
	public function acp_ranks_list_modify_rank_row($event)
	{
		$row = $event['row'];
		$rank_row = $event['rank_row'];
		$new = array('S_SELECTABLE_RANK' => ($row['selectable_rank']) ? true : false, );
		$event['rank_row'] = array_merge($rank_row, $new);
		
	}
	
	/**
	* Adds selectable_rank to sql array and adds the groups to the rank_groups table.
	*
	* @param \phpbb\event\data	$event	Event object
	*/
	public function acp_ranks_save_modify_sql_ary($event)
	{
		$sql_ary = $event['sql_ary'];
		$rank = $event['rank_id'];
	
		if(!$sql_ary['rank_special'])
		{
			$selectable_rank = 0;
		}
		else
		{
			$selectable_rank = request_var('selectable_rank', 0);
		}
		
		$sql_ary['selectable_rank'] = $selectable_rank; 
		$event['sql_ary'] = $sql_ary;
		
		//Delete all the rank/group associations for this rank before readding currently selected.
		$sql = 'DELETE FROM '.$this->rank_groups_table.' 
			WHERE rank_id = '.$rank.'';	
		$this->db->sql_query($sql);
		
		
		if($sql_ary['selectable_rank'])
		{
			//Get the selected groups
			$groups = request_var('groups', array(''));
			
			foreach($groups as $group)
			{
				$sql = 'INSERT INTO '.$this->rank_groups_table.'
				VALUES ('.$rank.','.$group.')';	
				$this->db->sql_query($sql);
			}
		
		}
		
		
		
		
		
	}
	
	
	
}
