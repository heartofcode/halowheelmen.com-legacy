<?php
/**
 *
 * Discord Integration. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2017, Jon Hart
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jonhart\discordintegration\ucp;

/**
 * Discord Integration UCP module info.
 */
class main_info
{
	function module()
	{
		return array(
			'filename'	=> '\jonhart\discordintegration\ucp\main_module',
			'title'		=> 'DISCORD_TITLE',
			'modes'		=> array(
				'settings'	=> array(
					'title'	=> 'UCP_DISCORD',
					'auth'	=> 'ext_jonhart/discordintegration',
					'cat'	=> array('DISCORD_TITLE')
				),
			),
		);
	}
}
