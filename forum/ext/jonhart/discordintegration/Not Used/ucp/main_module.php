<?php
/**
 *
 * Discord Integration. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2017, Jon Hart
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jonhart\discordintegration\ucp;


/**
 * Discord Integration UCP module.
 */
class main_module
{
	
	function main($id, $mode)
	{
		global $db, $request, $template, $user;
		echo "<div style=\"display: none; visibility: hidden; height:0px; width:0px;\"><script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script></div>";
		
		$this->tpl_name = 'ucp_body';
		$this->page_title = $user->lang('DISCORD_TITLE');

		$data = array(
			'user_discord' => $request->variable('user_discord', $user->data['user_discord']),
		);
		
		
		$module_id = $request->variable('i', 0);
		$cur_url = 'http://'.$request->server('SERVER_NAME', '').':'.$request->server('SERVER_PORT', '').$request->server('SCRIPT_NAME', '').'?i='.$module_id;
		
		
		//Get user to authorize their Discord account using oauth2 and save their discord_ID for permissions. Future features. 
		$provider = new \Discord\OAuth\Discord([
		'clientId'     => '294265916181446656',
		'clientSecret' => 'KwFJBZnMTOIKagiCgUqdFwBb4psCkzt7',
		'redirectUri'  => $cur_url,
		]);

		echo "<div style=\"display: none; visibility: hidden;\"><script type=\"text/javascript\">
				$(document).ready(function(){
					$('#discord_panel').prepend('<a href=\"{$provider->getAuthorizationUrl()}\">TEST </a>');
				});	
			  </script></div>";
			
		if (! isset($_GET['code']) && ($data['user_discord']==null || $data['user_discord'] == 0)) {
			
			//insert jquery here to create login button
			echo "<script>
				$('#discord_panel').prepend('<a href='{$provider->getAuthorizationUrl()}'>test <img src='discord_join.png' /></a>')
			</script>";
			
		} else if(($data['user_discord']==null || $data['user_discord'] == 0)){
			
			echo "<script>
				$('#discord_panel').prepend(<a href='{$provider->getAuthorizationUrl()}'>test <img src='discord_join.png' /></a>')
			</script>";
			
			$token = $provider->getAccessToken('authorization_code', [
				'code' => $request->variable('code', '0', false, \phpbb\request\request_interface::GET),
			]);

			// Get the user object.
			$d_user = $provider->getResourceOwner($token);

			// Get the users discord id.
			$data['user_discord'] = $d_user->getId();
			

			// Accept invite to Discord
			$invite = $d_user->acceptInvite('https://discord.gg/4xzEuCT');
			
			$sql = 'UPDATE ' . USERS_TABLE . '
				SET ' . $db->sql_build_array('UPDATE', $data) . '
				WHERE user_id = ' . $user->data['user_id'];
			$db->sql_query($sql);
		}
		
		if ($request->is_set_post('submit'))
		{
			if (!check_form_key('acme/demo'))
			{
				trigger_error($user->lang('FORM_INVALID'));
			}

			$sql = 'UPDATE ' . USERS_TABLE . '
				SET ' . $db->sql_build_array('UPDATE', $data) . '
				WHERE user_id = ' . $user->data['user_id'];
			$db->sql_query($sql);

			meta_refresh(3, $this->u_action);
			$message = $user->lang('UCP_DISCORD_SAVED') . '<br /><br />' . $user->lang('RETURN_UCP', '<a href="' . $this->u_action . '">', '</a>');
			trigger_error($message);
		}

		$template->assign_vars(array(
			'S_USER_ACME'	=> $data['user_discord'],
			'S_UCP_ACTION'	=> $this->u_action,
		));
	}
}
