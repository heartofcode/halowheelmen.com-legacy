<?php
/**
 *
 * Discord Integration. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2017, Jon Hart
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace jonhart\discordintegration\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Discord Integration Event listener.
 */
class main_listener implements EventSubscriberInterface
{
	static public function getSubscribedEvents()
	{
		return array(
			'core.submit_post_end'	=> 'submit_post_end',
		);
	}

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	
	/**
	 * Constructor
	 *
	 * @param \phpbb\db\driver\driver_interface	$db
	 */
	public function __construct(\phpbb\db\driver\driver_interface $db)
	{
		$this->db			= $db;
	}
	
	/**
	 * Event triggered when a new post or topic is created.
	 *
	 * @param \phpbb\event\data	$event	Event object
	 */
	public function submit_post_end($event)
	{
		$mode = $event['mode'];
		$forum = $event['data']['forum_id'];
		$topic = $event['data']['topic_id'];
		$post = $event['data']['post_id'];
				
		if($mode == 'reply' || $mode == 'post' || $mode == 'quote')
		{
			$post_url = 'http://www.halowheelmen.com/forum/viewtopic.php?f='.$forum.'&t='.$topic.'&p='.$post.'#p'.$post;
			$forum_url = 'http://www.halowheelmen.com/forum/viewforum.php?f='.$forum;
			$user_url = '';
			
			if($mode == 'reply' || $mode == 'quote')
			{
				$type = ' posted a reply: ';
			}
			else
			{
				$type = ' posted a new topic: ';
			}
						
			$message = $event['username']. $type.'['.$event['subject'].']('.$post_url.')'.' in '. '['.$event['data']['forum_name'].']('.$forum_url.')';
			
			//Get the forum permission from the acl_groups table and compare to user groups. 
			//Check registered users, then Wheelmen, then just post the update for Admins only. 
			$guests = 72;
			$reg_users = 73;
			$wheelmen = 109;
			$admin = true;
			
			$sql = 'SELECT *
			FROM ' . ACL_GROUPS_TABLE . ' r
			WHERE r.forum_id = '.$forum.'
			AND r.auth_option_id = 0
			AND (r.group_id = '.$reg_users.' OR r.group_id = '.$wheelmen.' OR r.group_id = '.$guests.')';
			
			$result = $this->db->sql_query($sql);
			
			while ($row = $this->db->sql_fetchrow($result))
			{
				if(($row['group_id'] == $reg_users && $row['auth_role_id'] != 16) || ($row['group_id'] == $guests && $row['auth_role_id'] != 16))
				{
					$admin = false;
					//$webhook = 'https://discordapp.com/api/webhooks/316980948300464129/r-OCaP5ivOwgk4lTT1WL-DFQ6ul6boj3-X84oP6glVDWkYioOB6arYvYfIFEEs-gjqxp';
					$webhook = 'https://discordapp.com/api/webhooks/316354995849003010/rxXHkp9BRYxnMGE4kd-crUVfear_umae3ISrWMqMMoU4iRvTE96KJLTT6-OSLGKV9dLf';
					break;
				}
				else if($row['group_id'] == $wheelmen && $row['auth_role_id'] != 16)
				{
					$admin = false;
					//$webhook = 'https://discordapp.com/api/webhooks/316981291033559041/K8GKNhryuSU5AYXOY9pngFRChc-EMcB2JXEfsOrYydfqlHhjXiaMjAojM5iRfyVQRmlJ';
					$webhook = 'https://discordapp.com/api/webhooks/316423997807919105/S6iXED_QHQ6_tK5m3Wo6eVmkXsLRPb-ulo1B-nGvd4F7olWrlKH9-voIUzjXbmbFMLgo';
					break;
				}
				
			}

			//Post in Admin only chat
			if($admin)
			{
				//$webhook = 'https://discordapp.com/api/webhooks/316981362106302465/NwcCnFCTDS_Oo7ImMQSHZ4v7p5M-phldOBQP_umcCSk9-X5h9G70M5ArwhpqNjz6BaXu';
				$webhook = 'https://discordapp.com/api/webhooks/316424111234744332/kngXCtyBtSw1il9Ht-ddB9rnjPiX3Oy803EFKjROkv3AFnT5Sc_uZJIywyS01UBOPW-R';
			}
			
			$data = array("content" => $message, "username" => "Wheeltron 6000");
			$curl = curl_init($webhook);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			return curl_exec($curl);
		}
	}
}
