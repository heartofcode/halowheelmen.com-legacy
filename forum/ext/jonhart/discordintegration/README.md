# Discord Integration

## Installation

Copy the extension to phpBB/ext/jonhart/discordintegration

Go to "ACP" > "Customise" > "Extensions" and enable the "Discord Integration" extension.

## License

[GPLv2](license.txt)
