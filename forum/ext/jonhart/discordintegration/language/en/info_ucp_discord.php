<?php
/**
 *
 * Discord Integration. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2017, Jon Hart
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'UCP_DISCORD'			=> 'Discord',
	'DISCORD_TITLE'		    => 'Discord',
	'UCP_DISCORD_EXPLAIN'	=> 'Link your Discord account here to join the HaloWheelmen Discord!',
	'UCP_DISCORD_SAVED'		=> 'Discord has been linked successfully!',
	'UCP_DISCORD_REMOVED'	=> 'Discord has been removed successfully!',

	'NOTIFICATION_TYPE_DEMO'	=> 'Use Acme demo notifications',
));
