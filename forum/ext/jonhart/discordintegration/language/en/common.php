<?php
/**
 *
 * Discord Integration. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2017, Jon Hart
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'DISCORD_I'			=> 'Discord PHPbb Integration',
	'D_HELLO'		=> 'Hello %s!',
	'D_GOODBYE'		=> 'Goodbye %s!',

	'ACP_DISCORD'				=> 'Settings',
	'ACP_DISCORD_SETTING_SAVED'	=> 'Settings have been saved successfully!',

	'ACME_DEMO_NOTIFICATION'	=> 'Acme demo notification',

	'VIEWING_ACME_DEMO'			=> 'Viewing Acme Demo',
));
