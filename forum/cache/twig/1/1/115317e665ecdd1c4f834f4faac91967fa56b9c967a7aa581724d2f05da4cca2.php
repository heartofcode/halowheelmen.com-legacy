<?php

/* components/analytics.html */
class __TwigTemplate_115317e665ecdd1c4f834f4faac91967fa56b9c967a7aa581724d2f05da4cca2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["ST_g_analytics_ID"]) ? $context["ST_g_analytics_ID"] : null)) {
            // line 2
            echo "\t<!-- GOOGLE ANALYTICS -->
\t<script>
\t\t(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
\t\t(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
\t\tm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
\t\t})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

\t\tga('create', '";
            // line 9
            echo (isset($context["ST_analytics_ID"]) ? $context["ST_analytics_ID"] : null);
            echo "', 'auto');
\t\tga('send', 'pageview');
\t</script>
";
        }
        // line 13
        echo "
";
        // line 14
        if ((isset($context["ST_c_analytics_ID"]) ? $context["ST_c_analytics_ID"] : null)) {
            // line 15
            echo "\t<script type=\"text/javascript\">
\tvar clicky_site_ids = clicky_site_ids || [];
\tclicky_site_ids.push(";
            // line 17
            echo (isset($context["ST_c_analytics_ID"]) ? $context["ST_c_analytics_ID"] : null);
            echo ");
\t(function() {
\t  var s = document.createElement('script');
\t  s.type = 'text/javascript';
\t  s.async = true;
\t  s.src = '//static.getclicky.com/js';
\t  ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
\t})();
\t</script>
\t<noscript><p><img alt=\"Clicky\" width=\"1\" height=\"1\" src=\"//in.getclicky.com/";
            // line 26
            echo (isset($context["ST_c_analytics_ID"]) ? $context["ST_c_analytics_ID"] : null);
            echo "ns.gif\" /></p></noscript>
";
        }
    }

    public function getTemplateName()
    {
        return "components/analytics.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 26,  46 => 17,  42 => 15,  40 => 14,  37 => 13,  30 => 9,  21 => 2,  19 => 1,);
    }
}
