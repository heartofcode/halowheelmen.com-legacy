<?php

/* posting_pm_header.html */
class __TwigTemplate_1b4c6eaa05a3e7dd0e05edea78c86f1f9143b4a29c60cb0946728e8d25e0a004 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<fieldset>
\t";
        // line 2
        if ( !(isset($context["S_SHOW_DRAFTS"]) ? $context["S_SHOW_DRAFTS"] : null)) {
            // line 3
            echo "
\t\t";
            // line 4
            if ((isset($context["S_GROUP_OPTIONS"]) ? $context["S_GROUP_OPTIONS"] : null)) {
                // line 5
                echo "\t\t\t<div class=\"column2\">
\t\t\t\t<label for=\"group_list\"><strong>";
                // line 6
                echo $this->env->getExtension('phpbb')->lang("TO_ADD_GROUPS");
                echo $this->env->getExtension('phpbb')->lang("COLON");
                echo "</strong></label><br />
\t\t\t\t<select name=\"group_list[]\" id=\"group_list\" multiple=\"multiple\" size=\"3\" class=\"inputbox\">";
                // line 7
                echo (isset($context["S_GROUP_OPTIONS"]) ? $context["S_GROUP_OPTIONS"] : null);
                echo "</select><br />
\t\t\t</div>
\t\t";
            }
            // line 10
            echo "\t\t
\t\t";
            // line 11
            if ((isset($context["S_ALLOW_MASS_PM"]) ? $context["S_ALLOW_MASS_PM"] : null)) {
                // line 12
                echo "\t\t<div class=\"ucp-pm-compose__addrecipients\">
\t\t\t";
                // line 13
                if ( !(isset($context["S_EDIT_POST"]) ? $context["S_EDIT_POST"] : null)) {
                    // line 14
                    echo "\t\t\t<label for=\"username_list\">";
                    echo $this->env->getExtension('phpbb')->lang("TO_ADD_MASS");
                    echo $this->env->getExtension('phpbb')->lang("COLON");
                    echo "</label>
\t\t\t<textarea id=\"username_list\" name=\"username_list\" class=\"inputbox\" cols=\"50\" rows=\"2\" tabindex=\"1\"></textarea>
\t\t\t\t<button name=\"add_to\" value=\"";
                    // line 16
                    echo $this->env->getExtension('phpbb')->lang("ADD");
                    echo "\" class=\"btn btn-default\" tabindex=\"1\">";
                    echo $this->env->getExtension('phpbb')->lang("ADD");
                    echo "</button>
\t\t\t\t<button name=\"add_bcc\" value=\"";
                    // line 17
                    echo $this->env->getExtension('phpbb')->lang("ADD_BCC");
                    echo "\" class=\"btn btn-default\" tabindex=\"1\">";
                    echo $this->env->getExtension('phpbb')->lang("ADD_BCC");
                    echo "</button>
\t\t\t\t";
                    // line 18
                    // line 19
                    echo "\t\t\t\t<a class=\"btn btn-default\" href=\"";
                    echo (isset($context["U_FIND_USERNAME"]) ? $context["U_FIND_USERNAME"] : null);
                    echo "\" onclick=\"find_username(this.href); return false;\">";
                    echo $this->env->getExtension('phpbb')->lang("FIND_USERNAME");
                    echo "</a>
\t\t\t\t";
                    // line 20
                    // line 21
                    echo "\t\t\t";
                }
                // line 22
                echo "\t\t</div>
\t\t";
                // line 23
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "to_recipient", array())) || twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "bcc_recipient", array())))) {
                    echo "<hr />";
                }
                // line 24
                echo "\t\t<div class=\"column1\">
\t\t\t";
                // line 25
                if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "to_recipient", array()))) {
                    // line 26
                    echo "\t\t\t\t<label>";
                    echo $this->env->getExtension('phpbb')->lang("TO_MASS");
                    echo $this->env->getExtension('phpbb')->lang("COLON");
                    echo "</label>
\t\t\t\t\t<dd class=\"recipients\">
\t\t\t\t\t<ul class=\"recipients\">
\t\t\t\t\t\t";
                    // line 29
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "to_recipient", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["to_recipient"]) {
                        // line 30
                        echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t";
                        // line 31
                        if ( !(isset($context["S_EDIT_POST"]) ? $context["S_EDIT_POST"] : null)) {
                            echo "<input type=\"submit\" name=\"remove_";
                            echo $this->getAttribute($context["to_recipient"], "TYPE", array());
                            echo "[";
                            echo $this->getAttribute($context["to_recipient"], "UG_ID", array());
                            echo "]\" value=\"x\" class=\"button2\" />";
                        }
                        // line 32
                        echo "\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["to_recipient"], "IS_GROUP", array())) {
                            echo "<a href=\"";
                            echo $this->getAttribute($context["to_recipient"], "U_VIEW", array());
                            echo "\"><strong>";
                            echo $this->getAttribute($context["to_recipient"], "NAME", array());
                            echo "</strong></a>";
                        } else {
                            echo $this->getAttribute($context["to_recipient"], "NAME_FULL", array());
                        }
                        // line 33
                        echo "\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['to_recipient'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 35
                    echo "\t\t\t\t\t</ul>
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t";
                }
                // line 39
                echo "\t\t</div>
\t\t\t";
                // line 40
                if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "bcc_recipient", array()))) {
                    // line 41
                    echo "\t\t\t<div class=\"column2\">
\t\t\t\t<dl>
\t\t\t\t\t<dt><label>";
                    // line 43
                    echo $this->env->getExtension('phpbb')->lang("BCC");
                    echo $this->env->getExtension('phpbb')->lang("COLON");
                    echo "</label></dt>
\t\t\t\t\t<dd class=\"recipients\">
\t\t\t\t\t<ul class=\"recipients\">
\t\t\t\t\t\t";
                    // line 46
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "bcc_recipient", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["bcc_recipient"]) {
                        // line 47
                        echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t";
                        // line 48
                        if ( !(isset($context["S_EDIT_POST"]) ? $context["S_EDIT_POST"] : null)) {
                            echo "<input type=\"submit\" name=\"remove_";
                            echo $this->getAttribute($context["bcc_recipient"], "TYPE", array());
                            echo "[";
                            echo $this->getAttribute($context["bcc_recipient"], "UG_ID", array());
                            echo "]\" value=\"x\" class=\"button2\" />";
                        }
                        // line 49
                        echo "\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["bcc_recipient"], "IS_GROUP", array())) {
                            echo "<a href=\"";
                            echo $this->getAttribute($context["bcc_recipient"], "U_VIEW", array());
                            echo "\"><strong>";
                            echo $this->getAttribute($context["bcc_recipient"], "NAME", array());
                            echo "</strong></a>";
                        } else {
                            echo $this->getAttribute($context["bcc_recipient"], "NAME_FULL", array());
                        }
                        // line 50
                        echo "\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bcc_recipient'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 52
                    echo "\t\t\t\t\t</ul>
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t</div>
\t\t\t";
                }
                // line 57
                echo "\t\t";
            } else {
                // line 58
                echo "\t\t<div class=\"column1\">
\t\t\t<dl>
\t\t\t\t<dt><label for=\"username_list\">";
                // line 60
                echo $this->env->getExtension('phpbb')->lang("TO_ADD");
                echo $this->env->getExtension('phpbb')->lang("COLON");
                echo "</label>";
                if ( !(isset($context["S_EDIT_POST"]) ? $context["S_EDIT_POST"] : null)) {
                    echo "<br /><span><a href=\"";
                    echo (isset($context["U_FIND_USERNAME"]) ? $context["U_FIND_USERNAME"] : null);
                    echo "\" onclick=\"find_username(this.href); return false\">";
                    echo $this->env->getExtension('phpbb')->lang("FIND_USERNAME");
                    echo "</a></span>";
                }
                echo "</dt>
\t\t\t\t";
                // line 61
                if ( !(isset($context["S_EDIT_POST"]) ? $context["S_EDIT_POST"] : null)) {
                    // line 62
                    echo "\t\t\t\t<dd><input class=\"inputbox\" type=\"text\" name=\"username_list\" id=\"username_list\" size=\"20\" value=\"\" /> <input type=\"submit\" name=\"add_to\" value=\"";
                    echo $this->env->getExtension('phpbb')->lang("ADD");
                    echo "\" class=\"button2\" /></dd>
\t\t\t\t";
                }
                // line 64
                echo "\t\t\t\t";
                if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "to_recipient", array()))) {
                    // line 65
                    echo "\t\t\t\t\t<dd class=\"recipients\">
\t\t\t\t\t<ul class=\"recipients\">
\t\t\t\t\t\t";
                    // line 67
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "to_recipient", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["to_recipient"]) {
                        // line 68
                        echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t";
                        // line 69
                        if ($this->getAttribute($context["to_recipient"], "IS_GROUP", array())) {
                            echo "<a href=\"";
                            echo $this->getAttribute($context["to_recipient"], "U_VIEW", array());
                            echo "\"><strong>";
                            echo $this->getAttribute($context["to_recipient"], "NAME", array());
                            echo "</strong></a>";
                        } else {
                            echo $this->getAttribute($context["to_recipient"], "NAME_FULL", array());
                        }
                        echo "&nbsp;
\t\t\t\t\t\t\t";
                        // line 70
                        if ( !(isset($context["S_EDIT_POST"]) ? $context["S_EDIT_POST"] : null)) {
                            echo "<input type=\"submit\" name=\"remove_";
                            echo $this->getAttribute($context["to_recipient"], "TYPE", array());
                            echo "[";
                            echo $this->getAttribute($context["to_recipient"], "UG_ID", array());
                            echo "]\" value=\"x\" class=\"button2\" />";
                        }
                        // line 71
                        echo "\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['to_recipient'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 73
                    echo "\t\t\t\t\t</dd>
\t\t\t\t";
                }
                // line 75
                echo "\t\t\t</dl>
\t\t</div>
\t\t";
            }
            // line 78
            echo "
\t";
        }
        // line 80
        echo "\t</fieldset>
";
    }

    public function getTemplateName()
    {
        return "posting_pm_header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 80,  277 => 78,  272 => 75,  268 => 73,  261 => 71,  253 => 70,  241 => 69,  238 => 68,  234 => 67,  230 => 65,  227 => 64,  221 => 62,  219 => 61,  206 => 60,  202 => 58,  199 => 57,  192 => 52,  185 => 50,  174 => 49,  166 => 48,  163 => 47,  159 => 46,  152 => 43,  148 => 41,  146 => 40,  143 => 39,  137 => 35,  130 => 33,  119 => 32,  111 => 31,  108 => 30,  104 => 29,  96 => 26,  94 => 25,  91 => 24,  87 => 23,  84 => 22,  81 => 21,  80 => 20,  73 => 19,  72 => 18,  66 => 17,  60 => 16,  53 => 14,  51 => 13,  48 => 12,  46 => 11,  43 => 10,  37 => 7,  32 => 6,  29 => 5,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
