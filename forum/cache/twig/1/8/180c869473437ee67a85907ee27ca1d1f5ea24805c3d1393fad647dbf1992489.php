<?php

/* @dmzx_mchat/mchat_body.html */
class __TwigTemplate_180c869473437ee67a85907ee27ca1d1f5ea24805c3d1393fad647dbf1992489 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["MCHAT_IS_ARCHIVE_PAGE"]) ? $context["MCHAT_IS_ARCHIVE_PAGE"] : null) || (isset($context["MCHAT_IS_CUSTOM_PAGE"]) ? $context["MCHAT_IS_CUSTOM_PAGE"] : null))) {
            // line 2
            echo "\t";
            $location = "overall_header.html";
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate("overall_header.html", "@dmzx_mchat/mchat_body.html", 2)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
        }
        // line 4
        echo "
";
        // line 5
        $location = "mchat_script_data.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("mchat_script_data.html", "@dmzx_mchat/mchat_body.html", 5)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 6
        echo "
";
        // line 7
        if (((isset($context["MCHAT_IS_ARCHIVE_PAGE"]) ? $context["MCHAT_IS_ARCHIVE_PAGE"] : null) && (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "pagination", array())) || (isset($context["MCHAT_TOTAL_MESSAGES"]) ? $context["MCHAT_TOTAL_MESSAGES"] : null)))) {
            // line 8
            echo "<div class=\"action-bar bar-top\">
\t";
            // line 9
            // line 10
            echo "\t<div class=\"pagination\">
\t\t";
            // line 11
            echo (isset($context["MCHAT_TOTAL_MESSAGES"]) ? $context["MCHAT_TOTAL_MESSAGES"] : null);
            echo "
\t\t";
            // line 12
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "pagination", array()))) {
                // line 13
                echo "\t\t\t";
                $location = "pagination.html";
                $namespace = false;
                if (strpos($location, '@') === 0) {
                    $namespace = substr($location, 1, strpos($location, '/') - 1);
                    $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                    $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
                }
                $this->loadTemplate("pagination.html", "@dmzx_mchat/mchat_body.html", 13)->display($context);
                if ($namespace) {
                    $this->env->setNamespaceLookUpOrder($previous_look_up_order);
                }
                // line 14
                echo "\t\t";
            } else {
                // line 15
                echo "\t\t\t&bull; ";
                echo (isset($context["PAGE_NUMBER"]) ? $context["PAGE_NUMBER"] : null);
                echo "
\t\t";
            }
            // line 17
            echo "\t</div>
\t";
            // line 18
            // line 19
            echo "</div>
";
        }
        // line 21
        echo "
";
        // line 22
        // line 23
        echo "
";
        // line 24
        if ((isset($context["MCHAT_IS_COLLAPSIBLE"]) ? $context["MCHAT_IS_COLLAPSIBLE"] : null)) {
            // line 25
            echo "\t<a class=\"category";
            if ((isset($context["S_MCHAT_HIDDEN"]) ? $context["S_MCHAT_HIDDEN"] : null)) {
                echo " hidden-category";
            }
            echo " mchat-category\"></a>
";
        }
        // line 27
        echo "
<div class=\"forabg mchat-wrapper\">
\t<a id=\"mChat\"></a>
\t<div class=\"inner\">
\t\t<ul class=\"topiclist\">
\t\t\t<li class=\"header\">
\t\t\t\t";
        // line 33
        $location = "mchat_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("mchat_header.html", "@dmzx_mchat/mchat_body.html", 33)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 34
        echo "\t\t\t\t";
        if ((isset($context["MCHAT_IS_COLLAPSIBLE"]) ? $context["MCHAT_IS_COLLAPSIBLE"] : null)) {
            // line 35
            echo "\t\t\t\t\t<a href=\"";
            echo (isset($context["U_MCHAT_COLLAPSE_URL"]) ? $context["U_MCHAT_COLLAPSE_URL"] : null);
            echo "\"
\t\t\t\t\t\tclass=\"collapse-btn collapse-";
            // line 36
            if ((isset($context["S_MCHAT_HIDDEN"]) ? $context["S_MCHAT_HIDDEN"] : null)) {
                echo "show";
            } else {
                echo "hide";
            }
            echo " mchat-collapse\"
\t\t\t\t\t\tdata-hidden=\"";
            // line 37
            echo (isset($context["S_MCHAT_HIDDEN"]) ? $context["S_MCHAT_HIDDEN"] : null);
            echo "\"
\t\t\t\t\t\tdata-ajax=\"phpbb_collapse\"
\t\t\t\t\t\tdata-overlay=\"true\"
\t\t\t\t\t\ttitle=\"";
            // line 40
            echo $this->env->getExtension('phpbb')->lang("MCHAT_COLLAPSE_TITLE");
            echo "\"></a>
\t\t\t\t";
        }
        // line 42
        echo "\t\t\t</li>
\t\t</ul>

\t\t";
        // line 45
        // line 46
        echo "
\t\t<div id=\"mchat-body\"";
        // line 47
        if ((isset($context["MCHAT_IS_COLLAPSIBLE"]) ? $context["MCHAT_IS_COLLAPSIBLE"] : null)) {
            echo " class=\"collapsible\"";
        }
        echo ">
\t\t\t";
        // line 48
        if ( !(isset($context["MCHAT_SOUND_DISABLED"]) ? $context["MCHAT_SOUND_DISABLED"] : null)) {
            // line 49
            echo "\t\t\t\t<audio id=\"mchat-sound-add\" class=\"hidden\" src=\"";
            echo (isset($context["EXT_URL"]) ? $context["EXT_URL"] : null);
            echo "sounds/add.mp3\" preload=\"auto\"></audio>
\t\t\t\t<audio id=\"mchat-sound-edit\" class=\"hidden\" src=\"";
            // line 50
            echo (isset($context["EXT_URL"]) ? $context["EXT_URL"] : null);
            echo "sounds/edit.mp3\" preload=\"auto\"></audio>
\t\t\t\t<audio id=\"mchat-sound-del\" class=\"hidden\" src=\"";
            // line 51
            echo (isset($context["EXT_URL"]) ? $context["EXT_URL"] : null);
            echo "sounds/del.mp3\" preload=\"auto\"></audio>
\t\t\t\t<audio id=\"mchat-sound-error\" class=\"hidden\" src=\"";
            // line 52
            echo (isset($context["EXT_URL"]) ? $context["EXT_URL"] : null);
            echo "sounds/error.mp3\" preload=\"auto\"></audio>
\t\t\t";
        }
        // line 54
        echo "
\t\t\t<div id=\"mchat-confirm\" class=\"hidden\">
\t\t\t\t<h3>";
        // line 56
        echo $this->env->getExtension('phpbb')->lang("CONFIRM");
        echo "</h3>
\t\t\t\t<div class=\"mchat-confirm-fields\">
\t\t\t\t\t<p></p>
\t\t\t\t\t<textarea></textarea>
\t\t\t\t</div>
\t\t\t\t<fieldset class=\"submit-buttons\">
\t\t\t\t\t<input type=\"button\" name=\"confirm\" value=\"";
        // line 62
        echo $this->env->getExtension('phpbb')->lang("MCHAT_OK");
        echo "\" class=\"button2\" />&nbsp;
\t\t\t\t\t<input type=\"button\" name=\"cancel\" value=\"";
        // line 63
        echo $this->env->getExtension('phpbb')->lang("CANCEL");
        echo "\" class=\"button2\" />
\t\t\t\t</fieldset>
\t\t\t</div>

\t\t\t<div id=\"mchat-main\">
\t\t\t\t";
        // line 68
        if (((isset($context["MCHAT_STATIC_MESS"]) ? $context["MCHAT_STATIC_MESS"] : null) &&  !(isset($context["MCHAT_IS_ARCHIVE_PAGE"]) ? $context["MCHAT_IS_ARCHIVE_PAGE"] : null))) {
            // line 69
            echo "\t\t\t\t\t<ul class=\"topiclist forums\">
\t\t\t\t\t\t<li class=\"row mchat-static\">";
            // line 70
            echo (isset($context["MCHAT_STATIC_MESS"]) ? $context["MCHAT_STATIC_MESS"] : null);
            echo "</li>
\t\t\t\t\t</ul>
\t\t\t\t";
        }
        // line 73
        echo "
\t\t\t\t";
        // line 74
        // line 75
        echo "
\t\t\t\t<ul id=\"mchat-messages\" class=\"topiclist forums\"";
        // line 76
        if ( !(isset($context["MCHAT_IS_ARCHIVE_PAGE"]) ? $context["MCHAT_IS_ARCHIVE_PAGE"] : null)) {
            echo " style=\"height:";
            if ((isset($context["MCHAT_IS_CUSTOM_PAGE"]) ? $context["MCHAT_IS_CUSTOM_PAGE"] : null)) {
                echo (isset($context["MCHAT_CUSTOM_HEIGHT"]) ? $context["MCHAT_CUSTOM_HEIGHT"] : null);
            } else {
                echo (isset($context["MCHAT_INDEX_HEIGHT"]) ? $context["MCHAT_INDEX_HEIGHT"] : null);
            }
            echo "px\"";
        }
        echo ">
\t\t\t\t\t";
        // line 77
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "mchatrow", array()))) {
            // line 78
            echo "\t\t\t\t\t\t";
            $location = "mchat_messages.html";
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate("mchat_messages.html", "@dmzx_mchat/mchat_body.html", 78)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
            // line 79
            echo "\t\t\t\t\t";
        } else {
            // line 80
            echo "\t\t\t\t\t\t<li class=\"row mchat-static mchat-no-messages\">";
            echo $this->env->getExtension('phpbb')->lang("MCHAT_NOMESSAGE");
            echo "</li>
\t\t\t\t\t";
        }
        // line 82
        echo "\t\t\t\t</ul>

\t\t\t\t";
        // line 84
        // line 85
        echo "\t\t\t</div>

\t\t\t";
        // line 87
        $location = "mchat_panel.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("mchat_panel.html", "@dmzx_mchat/mchat_body.html", 87)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 88
        echo "\t\t</div>

\t\t";
        // line 90
        // line 91
        echo "\t</div>
</div>

";
        // line 94
        // line 95
        echo "
";
        // line 96
        if ((isset($context["MCHAT_IS_ARCHIVE_PAGE"]) ? $context["MCHAT_IS_ARCHIVE_PAGE"] : null)) {
            // line 97
            echo "<div class=\"action-bar bottom\">
\t";
            // line 98
            // line 99
            echo "\t<div class=\"pagination\">
\t\t";
            // line 100
            echo (isset($context["MCHAT_TOTAL_MESSAGES"]) ? $context["MCHAT_TOTAL_MESSAGES"] : null);
            echo "
\t\t";
            // line 101
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "pagination", array()))) {
                // line 102
                echo "\t\t\t";
                $location = "pagination.html";
                $namespace = false;
                if (strpos($location, '@') === 0) {
                    $namespace = substr($location, 1, strpos($location, '/') - 1);
                    $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                    $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
                }
                $this->loadTemplate("pagination.html", "@dmzx_mchat/mchat_body.html", 102)->display($context);
                if ($namespace) {
                    $this->env->setNamespaceLookUpOrder($previous_look_up_order);
                }
                // line 103
                echo "\t\t";
            } else {
                // line 104
                echo "\t\t\t&bull; ";
                echo (isset($context["PAGE_NUMBER"]) ? $context["PAGE_NUMBER"] : null);
                echo "
\t\t";
            }
            // line 106
            echo "\t</div>
\t";
            // line 107
            // line 108
            echo "</div>
";
        }
        // line 110
        echo "
";
        // line 111
        if (((isset($context["MCHAT_IS_CUSTOM_PAGE"]) ? $context["MCHAT_IS_CUSTOM_PAGE"] : null) && (isset($context["MCHAT_WHOIS_REFRESH"]) ? $context["MCHAT_WHOIS_REFRESH"] : null))) {
            // line 112
            echo "\t<h3>";
            echo $this->env->getExtension('phpbb')->lang("MCHAT_WHO_IS_CHATTING");
            echo "</h3>
\t";
            // line 113
            $location = "mchat_whois.html";
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate("mchat_whois.html", "@dmzx_mchat/mchat_body.html", 113)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
            // line 114
            echo "\t<div id=\"mchat-refresh\">
\t\t<span id=\"mchat-refresh-explain\">";
            // line 115
            echo (isset($context["MCHAT_WHOIS_REFRESH_EXPLAIN"]) ? $context["MCHAT_WHOIS_REFRESH_EXPLAIN"] : null);
            echo "</span>
\t\t<span id=\"mchat-refresh-pending\" class=\"hidden\">";
            // line 116
            echo $this->env->getExtension('phpbb')->lang("MCHAT_REFRESHING");
            echo "</span>
\t</div>
";
        }
        // line 119
        echo "
";
        // line 120
        if ((((isset($context["MCHAT_IS_CUSTOM_PAGE"]) ? $context["MCHAT_IS_CUSTOM_PAGE"] : null) || (isset($context["MCHAT_IS_ARCHIVE_PAGE"]) ? $context["MCHAT_IS_ARCHIVE_PAGE"] : null)) && (isset($context["LEGEND"]) ? $context["LEGEND"] : null))) {
            // line 121
            echo "\t<div id=\"mchat-legend\">
\t\t<em>";
            // line 122
            echo $this->env->getExtension('phpbb')->lang("LEGEND");
            echo $this->env->getExtension('phpbb')->lang("COLON");
            echo " ";
            echo (isset($context["LEGEND"]) ? $context["LEGEND"] : null);
            echo "</em>
\t</div>
";
        }
        // line 125
        echo "
";
        // line 126
        if (((isset($context["MCHAT_IS_ARCHIVE_PAGE"]) ? $context["MCHAT_IS_ARCHIVE_PAGE"] : null) || (isset($context["MCHAT_IS_CUSTOM_PAGE"]) ? $context["MCHAT_IS_CUSTOM_PAGE"] : null))) {
            // line 127
            echo "\t";
            $location = "overall_footer.html";
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate("overall_footer.html", "@dmzx_mchat/mchat_body.html", 127)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
        }
    }

    public function getTemplateName()
    {
        return "@dmzx_mchat/mchat_body.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  413 => 127,  411 => 126,  408 => 125,  399 => 122,  396 => 121,  394 => 120,  391 => 119,  385 => 116,  381 => 115,  378 => 114,  366 => 113,  361 => 112,  359 => 111,  356 => 110,  352 => 108,  351 => 107,  348 => 106,  342 => 104,  339 => 103,  326 => 102,  324 => 101,  320 => 100,  317 => 99,  316 => 98,  313 => 97,  311 => 96,  308 => 95,  307 => 94,  302 => 91,  301 => 90,  297 => 88,  285 => 87,  281 => 85,  280 => 84,  276 => 82,  270 => 80,  267 => 79,  254 => 78,  252 => 77,  240 => 76,  237 => 75,  236 => 74,  233 => 73,  227 => 70,  224 => 69,  222 => 68,  214 => 63,  210 => 62,  201 => 56,  197 => 54,  192 => 52,  188 => 51,  184 => 50,  179 => 49,  177 => 48,  171 => 47,  168 => 46,  167 => 45,  162 => 42,  157 => 40,  151 => 37,  143 => 36,  138 => 35,  135 => 34,  123 => 33,  115 => 27,  107 => 25,  105 => 24,  102 => 23,  101 => 22,  98 => 21,  94 => 19,  93 => 18,  90 => 17,  84 => 15,  81 => 14,  68 => 13,  66 => 12,  62 => 11,  59 => 10,  58 => 9,  55 => 8,  53 => 7,  50 => 6,  38 => 5,  35 => 4,  21 => 2,  19 => 1,);
    }
}
